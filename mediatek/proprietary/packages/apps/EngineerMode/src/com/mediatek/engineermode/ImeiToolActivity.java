package com.mediatek.engineermode;

import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
//import com.mediatek.common.featureoption.FeatureOption;
import android.os.AsyncResult;
import android.os.Message;
import android.util.Log;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
public class ImeiToolActivity extends Activity {
	private final static String XLOGTAG = "ImeiTool";
	private Button mOkBtn;
	private EditText mImei1, mImei2;
	protected  Phone mPhone = null;
	private final int WRITEIMEI1 = 1;
	private final int WRITEIMEI2 = 2;
	private int count = 0;
	private boolean mFailFlag = false;
        private TextView mImei2Text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.imei_tool);
		mImei1 = (EditText) findViewById(R.id.imei1);
		mImei2 = (EditText) findViewById(R.id.imei2);
		mOkBtn = (Button) findViewById(R.id.ok);
		mImei2Text = (TextView)findViewById(R.id.imei2Text);
		if (SystemProperties.get("ro.mtk_gemini_support").equals("1") == false)
		{
  			mImei2.setVisibility(View.GONE);
			mImei2Text.setVisibility(View.GONE);
		}
		mOkBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Log.i(XLOGTAG, "imei1:" + mImei1.getText().toString()
						+ " imei2:" + mImei2.getText().toString());
				if (mImei1.getText().toString().length() != 15) {
					new AlertDialog.Builder(ImeiToolActivity.this).setMessage(
							"imei1 length must is 15").show();
					return;
				}
				if (SystemProperties.get("ro.mtk_gemini_support").equals("1") == true)
				{
					if (mImei2.getText().toString().length() != 15) {
					new AlertDialog.Builder(ImeiToolActivity.this).setMessage(
							"imei2 length must is 15").show();
					return;
					}
				
					writeImei(mImei1.getText().toString(), 1);
					writeImei(mImei2.getText().toString(), 2);
				}
				else
				{
					 writeImei(mImei1.getText().toString(), 1);
				}
				count = 0;
				mFailFlag = false;

			}

		});

	}

	public void writeImei(String imei, int slot) {
		String cmd[] = new String[2];
		if (slot == 1)
			cmd[0] = "AT+EGMR=1,7," + "\"" + imei + "\"";
		else
			cmd[0] = "AT+EGMR=1,10," + "\"" + imei + "\"";
		cmd[1] = "";
        	mPhone = PhoneFactory.getDefaultPhone();
        	if (TelephonyManager.getDefault().getPhoneCount() > 1) {
            		mPhone = PhoneFactory.getPhone(PhoneConstants.SIM_ID_1);
		 	for (int i = 0 ; i < TelephonyManager.getDefault().getPhoneCount(); i++) {
				Log.i(XLOGTAG, "cmd[0]=" + cmd[0]);
                        	mPhone.invokeOemRilRequestStrings(cmd, mHandler.obtainMessage(slot == 1 ? WRITEIMEI1
									: WRITEIMEI2));
			}
		} else {
			mPhone = PhoneFactory.getDefaultPhone();
			mPhone.invokeOemRilRequestStrings(cmd,
					mHandler.obtainMessage(WRITEIMEI1));
		}
	}

	protected final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			String text = "";
			AsyncResult ar = (AsyncResult) msg.obj;
			switch (msg.what) {
			case WRITEIMEI1:
				if (ar.exception == null) {
					text = "write imei1 success";
					count++;
				} else {
					text = "write imei1 failed.";
					mFailFlag = true;
				}

				break;
			case WRITEIMEI2:

				if (ar.exception == null) {
					text = "write imei2 success.";
					count++;
				} else {
					text = "write imei2 failed.";
					mFailFlag = true;
				}

				break;

			default:
				break;
			}
			if (mFailFlag)
				new AlertDialog.Builder(ImeiToolActivity.this)
						.setMessage(text)
						.setPositiveButton(R.string.imeiOK,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {

										/* User clicked OK so do some stuff */
									}
								}).create().show();

			if (count == 2||(!mFailFlag&&SystemProperties.get("ro.mtk_gemini_support").equals("1") == false))
				new AlertDialog.Builder(ImeiToolActivity.this)
						.setMessage(R.string.imeiTip)
						.setPositiveButton(R.string.imeiOK,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {

									//	PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
									//	pm.reboot("");
										Intent i = new Intent(Intent.ACTION_REBOOT);
										i.putExtra("nowait", 1);
										i.putExtra("interval", 1);
										i.putExtra("window", 0);
										sendBroadcast(i);
									}
								})
						.setNegativeButton(R.string.imeiCancel,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {

										/* User clicked Cancel so do some stuff */
									}
								}).create().show();
		}
	};

}
