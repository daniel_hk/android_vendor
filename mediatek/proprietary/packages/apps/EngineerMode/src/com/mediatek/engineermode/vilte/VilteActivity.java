/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.vilte;

import android.app.Activity;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.os.SystemProperties;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.EditText;

import com.mediatek.engineermode.R;

/**
 * ViLTE Configuration.
 */
public class VilteActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "EM/ViLTE";
    private static final String PROP_VILTE_UT_SUPPORT = "persist.radio.vilte_ut_support";
    private static final String PROP_VILTE_IMS_SIMULATE = "persist.ims.simulate";
    private static final String PROP_VILTE_VIDEO_FPS = "persist.radio.vilte_vfps";
    private static final String PROP_VILTE_SOURCE_BITSTREAM = "persist.radio.vilte_dump_source";
    private static final String PROP_VILTE_SINK_BITSTREAM = "persist.radio.vilte_dump_sink";
    private static final String PROP_VILTE_VIDEO_LEVEL = "persist.radio.vilte_vlevel";
    private static final String PROP_VILTE_VIDEO_PROFILE = "persist.radio.vilte_vprofile";
    private static final String PROP_VILTE_VIDEO_LEVEL_BIT_RATE =
        "persist.radio.vilte_vbitrate";
    private static final String PROP_VILTE_VIDEO_VENC_BITRATE_RATIO =
        "persist.radio.bitrate_ratio";
    private static final String PROP_VILTE_VIDEO_IDR_PERIOD = "persist.radio.vilte_viperiod";
    private static final String BROADCUST_REGISTER = "registry";

    private TextView mVilteUtSupport;
    private TextView mVilteVideoFps;
    private TextView mVilteSourceBitstream;
    private TextView mVilteSinkBitstream;
    private TextView mVilteImsSimulate;
    private TextView mTextviewLevel;
    private TextView mTextviewProfile;
    private TextView mTextviewBitrate;
    private TextView mTextviewBitrateRatio;
    private TextView mTextviewIPeriod;
    private Button mButtonEnable;
    private Button mButtonDisable;
    private Button mButtonSetFps;
    private Button mButtonEnableSourceBitstream;
    private Button mButtonDisableSourceBitstream;
    private Button mButtonEnableSinkBitstream;
    private Button mButtonDisableSinkBitstream;
    private Button mButtonSetLevel;
    private Button mButtonSetProfile;
    private Button mButtonSetBitRate;
    private Button mButtonSetBitrateRatio;
    private Button mButtonSetIperiod;
    private Spinner mSpinner;
    private Spinner mSpinnerProfile;
    private EditText mEdittextLevel;
    private EditText mEdittextBitRate;
    private EditText mEdittextBitrateRatio;
    private EditText mEdittextIperiod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("@M_" + TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vilte_config);
        mVilteUtSupport = (TextView) findViewById(R.id.vilte_ut_support_status);
        mVilteVideoFps = (TextView) findViewById(R.id.vilte_video_fps_status);
        mVilteSourceBitstream = (TextView) findViewById(R.id.vilte_source_bitstream_status);
        mVilteSinkBitstream = (TextView) findViewById(R.id.vilte_sink_bitstream_status);
        mVilteImsSimulate = (TextView) findViewById(R.id.vilte_ims_simulate_status);
        mTextviewLevel = (TextView) findViewById(R.id.vilte_video_level_status);
        mTextviewProfile = (TextView) findViewById(R.id.vilte_video_profile_status);
        mTextviewBitrate = (TextView) findViewById(R.id.vilte_video_bitrate_status);
        mTextviewBitrateRatio = (TextView) findViewById(R.id.vilte_video_bitrate_ratio_status);
        mTextviewIPeriod = (TextView) findViewById(R.id.vilte_video_iperiod_status);
        mSpinner = (Spinner) findViewById(R.id.vilte_video_fps_values);
        mEdittextLevel = (EditText) findViewById(R.id.vilte_video_level_values);
        mSpinnerProfile = (Spinner) findViewById(R.id.vilte_video_profile_values);
        mEdittextBitRate = (EditText) findViewById(R.id.vilte_video_level_bit_rate_values);
        mEdittextBitrateRatio = (EditText) findViewById(R.id.vilte_video_bitrate_ratio_values);
        mEdittextIperiod = (EditText) findViewById(R.id.vilte_video_idr_period_values);
        mButtonEnable = (Button) findViewById(R.id.vilte_ut_support_enable);
        mButtonDisable = (Button) findViewById(R.id.vilte_ut_support_disable);
        mButtonSetFps = (Button) findViewById(R.id.vilte_video_fps_set);
        mButtonSetLevel = (Button) findViewById(R.id.vilte_video_level_set);
        mButtonSetProfile = (Button) findViewById(R.id.vilte_video_profile_set);
        mButtonSetBitRate = (Button) findViewById(R.id.vilte_video_level_bit_rate_set);
        mButtonSetBitrateRatio = (Button) findViewById(R.id.vilte_video_venc_bitrate_ratio_set);
        mButtonSetIperiod = (Button) findViewById(R.id.vilte_video_idr_period_set);
        mButtonEnableSourceBitstream = (Button) findViewById(R.id.vilte_source_bitstream_enable);
        mButtonDisableSourceBitstream = (Button) findViewById(R.id.vilte_source_bitstream_disable);
        mButtonEnableSinkBitstream = (Button) findViewById(R.id.vilte_sink_bitstream_enable);
        mButtonDisableSinkBitstream = (Button) findViewById(R.id.vilte_sink_bitstream_disable);
        mButtonEnable.setOnClickListener(this);
        mButtonDisable.setOnClickListener(this);
        mButtonSetFps.setOnClickListener(this);
        mButtonEnableSourceBitstream.setOnClickListener(this);
        mButtonDisableSourceBitstream.setOnClickListener(this);
        mButtonEnableSinkBitstream.setOnClickListener(this);
        mButtonDisableSinkBitstream.setOnClickListener(this);
        mButtonSetLevel.setOnClickListener(this);
        mButtonSetProfile.setOnClickListener(this);
        mButtonSetBitRate.setOnClickListener(this);
        mButtonSetBitrateRatio.setOnClickListener(this);
        mButtonSetIperiod.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        Log.d("@M_" + TAG, "onResume()");
        super.onResume();
        queryCurrentValue();
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonEnable) {
            Log.d("@M_" + TAG, "Set " + PROP_VILTE_UT_SUPPORT + " = 1");
            SystemProperties.set(PROP_VILTE_UT_SUPPORT, "1");
            SystemProperties.set(PROP_VILTE_IMS_SIMULATE, "1");
            Intent intent = new Intent("ACTION_IMS_SIMULATE");
            intent.putExtra(BROADCUST_REGISTER, true);
            sendBroadcast(intent);
        } else if (v == mButtonDisable) {
            Log.d("@M_" + TAG, "Set " + PROP_VILTE_UT_SUPPORT + " = 0");
            SystemProperties.set(PROP_VILTE_UT_SUPPORT, "0");
            SystemProperties.set(PROP_VILTE_IMS_SIMULATE, "0");
        } else if (v == mButtonSetFps) {
            String fps = mSpinner.getSelectedItem().toString();
            Log.d("@M_" + TAG, "Set " + PROP_VILTE_VIDEO_FPS + " = " + fps);
            SystemProperties.set(PROP_VILTE_VIDEO_FPS, fps);
        } else if (v == mButtonEnableSourceBitstream) {
            Log.d("@M_" + TAG, "Set " + PROP_VILTE_SOURCE_BITSTREAM + " = 1");
            SystemProperties.set(PROP_VILTE_SOURCE_BITSTREAM, "1");
        } else if (v == mButtonDisableSourceBitstream) {
            Log.d("@M_" + TAG, "Set " + PROP_VILTE_SOURCE_BITSTREAM + " = 0");
            SystemProperties.set(PROP_VILTE_SOURCE_BITSTREAM, "0");
        } else if (v == mButtonEnableSinkBitstream) {
            Log.d("@M_" + TAG, "Set " + PROP_VILTE_SINK_BITSTREAM + " = 1");
            SystemProperties.set(PROP_VILTE_SINK_BITSTREAM, "1");
        } else if (v == mButtonDisableSinkBitstream) {
            Log.d("@M_" + TAG, "Set " + PROP_VILTE_SINK_BITSTREAM + " = 0");
            SystemProperties.set(PROP_VILTE_SINK_BITSTREAM, "0");
        } else if (v == mButtonSetLevel) {
            Log.d("@M_" + TAG, "Set " + PROP_VILTE_VIDEO_LEVEL +
                " = " + mEdittextLevel.getText().toString());
            SystemProperties.set(PROP_VILTE_VIDEO_LEVEL, mEdittextLevel.getText().toString());
        } else if (v == mButtonSetBitRate) {
            Log.d("@M_" + TAG, "Set " + PROP_VILTE_VIDEO_LEVEL_BIT_RATE +
                " = " + mEdittextBitRate.getText().toString());
            SystemProperties.set(PROP_VILTE_VIDEO_LEVEL_BIT_RATE,
                mEdittextBitRate.getText().toString());
        } else if (v == mButtonSetBitrateRatio) {
            Log.d("@M_" + TAG, "Set " + PROP_VILTE_VIDEO_VENC_BITRATE_RATIO +
                " = " + mEdittextBitrateRatio.getText().toString());
            SystemProperties.set(PROP_VILTE_VIDEO_VENC_BITRATE_RATIO,
                mEdittextBitrateRatio.getText().toString());
        } else if (v == mButtonSetIperiod) {
            Log.d("@M_" + TAG, "Set " + PROP_VILTE_VIDEO_IDR_PERIOD +
                " = " + mEdittextIperiod.getText().toString());
            SystemProperties.set(PROP_VILTE_VIDEO_IDR_PERIOD,
                mEdittextIperiod.getText().toString());
        } else if (v == mButtonSetProfile) {
            Log.d("@M_" + TAG, "Set " + PROP_VILTE_VIDEO_PROFILE +
                " = " + mSpinnerProfile.getSelectedItem().toString());
            SystemProperties.set(PROP_VILTE_VIDEO_PROFILE,
                mSpinnerProfile.getSelectedItem().toString());
        }
        queryCurrentValue();
    }

    private void queryCurrentValue() {
        String ut = SystemProperties.get(PROP_VILTE_UT_SUPPORT, "");
        String fps = SystemProperties.get(PROP_VILTE_VIDEO_FPS, "");
        String level = SystemProperties.get(PROP_VILTE_VIDEO_LEVEL, "");
        String profile = SystemProperties.get(PROP_VILTE_VIDEO_PROFILE, "");
        String bitrate = SystemProperties.get(PROP_VILTE_VIDEO_LEVEL_BIT_RATE, "");
        String bitrateRatio = SystemProperties.get(PROP_VILTE_VIDEO_VENC_BITRATE_RATIO, "");
        String iPeriod = SystemProperties.get(PROP_VILTE_VIDEO_IDR_PERIOD, "");
        String source = SystemProperties.get(PROP_VILTE_SOURCE_BITSTREAM, "");
        String sink = SystemProperties.get(PROP_VILTE_SINK_BITSTREAM, "");
        String simulate = SystemProperties.get(PROP_VILTE_IMS_SIMULATE, "");
        mVilteUtSupport.setText(PROP_VILTE_UT_SUPPORT + " = " + ut);
        mVilteVideoFps.setText(PROP_VILTE_VIDEO_FPS + " = " + fps);
        mVilteSourceBitstream.setText(PROP_VILTE_SOURCE_BITSTREAM + " = " + source);
        mVilteSinkBitstream.setText(PROP_VILTE_SINK_BITSTREAM + " = " + sink);
        mVilteImsSimulate.setText(PROP_VILTE_IMS_SIMULATE + " = " + simulate);
        mTextviewLevel.setText(PROP_VILTE_VIDEO_LEVEL + " = " + level);
        mTextviewProfile.setText(PROP_VILTE_VIDEO_PROFILE + " = " + profile);
        mTextviewBitrate.setText(PROP_VILTE_VIDEO_LEVEL_BIT_RATE + " = " + bitrate);
        mTextviewBitrateRatio.setText(PROP_VILTE_VIDEO_VENC_BITRATE_RATIO + " = " + bitrateRatio);
        mTextviewIPeriod.setText(PROP_VILTE_VIDEO_IDR_PERIOD + " = " + iPeriod);
        for (int i = 0; i < mSpinner.getCount(); i++) {
            if (mSpinner.getItemAtPosition(i).toString().equals(fps)) {
                mSpinner.setSelection(i);
                break;
            }
        }

        for (int i = 0; i < mSpinnerProfile.getCount(); i++) {
            if (mSpinnerProfile.getItemAtPosition(i).toString().equals(profile)) {
                mSpinnerProfile.setSelection(i);
                break;
            }
        }
        mEdittextLevel.setText(level);
        mEdittextBitRate.setText(bitrate);
        mEdittextBitrateRatio.setText(bitrateRatio);
        mEdittextIperiod.setText(iPeriod);
    }
}
