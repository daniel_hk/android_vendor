package com.mediatek.connectivity;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemProperties;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.PhoneConstants;

import java.util.ArrayList;

/**
  * Service class for eMBMS enginner function.
  *
  */
public class CdsEmbmsService extends Service
                implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = "CdsEmbms";

    private static final int MAX_AT_CMD_RESPONSE = 2048;
    private static final int MAX_SESSION_NUM = 8;

    private PhoneStateListener mPhoneStateListener;
    private SharedPreferences mDataStore;
    private SharedPreferences.Editor  mDataEditor;
    private final IBinder mBinder = new EmbmsBinder();
    private boolean mIsEmbmsEnabled;
    private boolean mIsEmbmsAuto;

    /**
     *
     * @hiden
     */
    public class EmbmsBinder extends Binder {
        CdsEmbmsService getService() {
            // Return this instance of LocalService so clients can call public methods
            return CdsEmbmsService.this;
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
                boolean isAirplaneModeOn = intent.getBooleanExtra("state", false);
                if (!isAirplaneModeOn) {
                    checkEmbmsStatus();
                }
            }
        }
    };

    private void checkEmbmsStatus() {
        updateSessionVariables();
        Log.i(TAG, "Check embms status:" + mIsEmbmsEnabled + ":" + mIsEmbmsAuto);
        if (mIsEmbmsAuto) {
            enableEmbmsSrv(mIsEmbmsEnabled);
        }
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate() {
        mDataStore = this.getSharedPreferences(CdsEmbmsConsts.EMBMS_FILE, 0);
        mDataStore.registerOnSharedPreferenceChangeListener(this);

        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        registerReceiver(mReceiver, intentFilter);
        checkEmbmsStatus();

        TelephonyManager telephonyManager =
            (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int subId = getDefaultPhoneId();
        mPhoneStateListener = getPhoneStateListener(subId);
        telephonyManager.listen(mPhoneStateListener,
                            PhoneStateListener.LISTEN_SERVICE_STATE);

        updateSessionVariables();
        Log.i(TAG, "onCreate in CdsEmbmsService");
    }

    private void updateSessionVariables() {
        mIsEmbmsEnabled = mDataStore.getBoolean(CdsEmbmsConsts.EMBMS_STATUS, false);
        mIsEmbmsAuto = mDataStore.getBoolean(CdsEmbmsConsts.EMBMS_AUTO_ENABLED, false);
        Log.i(TAG, "updateSessionVariables:" + mIsEmbmsEnabled + ":" + mIsEmbmsAuto);
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy in CdsEmbmsService");
        mDataStore.unregisterOnSharedPreferenceChangeListener(this);
        unregisterReceiver(mReceiver);

        TelephonyManager telephonyManager =
            (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(mPhoneStateListener,
                        PhoneStateListener.LISTEN_NONE);
    }


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     *  Enable modem eMBMS capability.
     *
     */
    public void enableEmbmsFeature() {
        sendAtcmd("AT+ESBP=5,\"SBP_LTE_MBMS\",1");
        sendAtcmd("AT+ESBP=5,\"SBP_LTE_MBMS_COUNTING\",1");
        sendAtcmd("AT+ESBP=5,\"SBP_LTE_MBMS_SERVICE_CONTINUITY\",1");
        sendAtcmd("AT+ESBP=5,\"SBP_LTE_MBMS_SCELL\",1");
    }

    /**
     * Used for activate or deactive eMBMS session.
     *
     * @param isActivated flag for activate eMBMS session or not.
     * @param sessionInfo the information of eMBMS session.
     *
     */
    public void activateEmbmsSession(boolean isActivated, String sessionInfo) {
        int activated = (isActivated) ? 1 : 0;
        Log.i(TAG, "activateEmbmsSession:" + isActivated + ":" + sessionInfo);
        if (isActivated) {
            sendAtcmd("AT+EMSESS=1," + sessionInfo);
        } else {
            sendAtcmd("AT+EMSESS=0," + sessionInfo);
        }
    }

    /**
     * Used for enable or disable eMBMS service.
     *
     * @param isEnabled flag for enable eMBMS service.
     */
    public void enableEmbmsSrv(boolean isEnabled) {
        int enabled = (isEnabled) ? 1 : 0;
        mIsEmbmsEnabled = isEnabled;
        if (isEnabled) {
            sendAtcmd("AT+EMSEN="  + enabled);
            sendAtcmd("AT+EMSRV="  + enabled);
            sendAtcmd("AT+EMSLU="  + enabled);
            sendAtcmd("AT+EMSAIL=" + enabled);
            sendAtcmd("AT+EMPRI="  + enabled);
        } else {
            sendAtcmd("AT+EMPRI="  + enabled);
            sendAtcmd("AT+EMSAIL=" + enabled);
            sendAtcmd("AT+EMSLU="  + enabled);
            sendAtcmd("AT+EMSRV="  + enabled);
            sendAtcmd("AT+EMSEN="  + enabled);
        }
    }

    /**
     * Used for read current session list information.
     *
     * @return current session list from network.
     */
    public String readCurrentSessionList() {
        String rawData = sendAtcmd("AT+EMSLU?");
        return parseNwSessionList(rawData);
    }

    private String parseNwSessionList(String data) {
        StringBuffer sessions = new StringBuffer();
        String dbgString = SystemProperties.get("embms.debug.string", "");
        if (dbgString.length() != 0) {
            dbgString = dbgString.replace("|", "\n");
            data = dbgString;
        }
        String[] tmpSessions = data.split("\n");

        Log.i(TAG, "Raw data:" + data);
        Log.i(TAG, "parseNwSessionList:" + tmpSessions.length);
        for (int i = 1; i < tmpSessions.length; i++) {
            if (tmpSessions[i].indexOf("EMSLU") != -1) {
                String[] tmpSessionInfo = tmpSessions[i].split(",");
                Log.i(TAG, "tmpSessionInfo:" + tmpSessionInfo.length);
                if (tmpSessionInfo.length == 5) {
                    sessions.append(tmpSessions[i] + "\n");
                }
            }
        }
        return sessions.toString();
    }

    /**
     * Utility function for send AT command.
     *
     * @param atCmd AT command string.
     * @return the response of AT command.
     *
     */
    public String sendAtcmd(String atCmd) {
        String atCmdRsp = "";
        try {
            TelephonyManager telephonyManager =
            (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            byte[] rawData = atCmd.getBytes();
            byte[] cmdByte = new byte[rawData.length + 1];
            byte[] cmdResp = new byte[MAX_AT_CMD_RESPONSE];
            System.arraycopy(rawData, 0, cmdByte, 0, rawData.length);
            cmdByte[cmdByte.length - 1] = 0;
            Log.i(TAG, "sendAtcmd:" + atCmd);
            int ret = telephonyManager.invokeOemRilRequestRaw(cmdByte, cmdResp);
            if (ret != -1) {
                cmdResp[ret] = 0;
                atCmdRsp = new String(cmdResp);
            }
        } catch (NullPointerException ee) {
                ee.printStackTrace();
        }
        return atCmdRsp;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.i(TAG, "key:" + key);
    }

    private PhoneStateListener getPhoneStateListener(int subId) {
        PhoneStateListener phoneStateListener = new PhoneStateListener(subId) {
            @Override
            public void onServiceStateChanged(ServiceState state) {
                if (state.getState() == ServiceState.STATE_IN_SERVICE) {
                    if (state.getDataNetworkType() == TelephonyManager.NETWORK_TYPE_LTE) {
                        sendInterestedEmbmsSession();
                    }
                }
            }
        };
        return phoneStateListener;
    }

    private int getDefaultPhoneId() {
        int phoneId = SystemProperties.getInt(
                                PhoneConstants.PROPERTY_CAPABILITY_SWITCH, 1) - 1;
        if (phoneId < 0 || phoneId >= TelephonyManager.getDefault().getPhoneCount()) {
            phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        }
        return phoneId;
    }

    /**
     * Utility function for interested sessions.
     *
     */
    public void sendInterestedEmbmsSession() {
        ArrayList<CdsEmbmsSessionEntry> sessionList =
                                new ArrayList<CdsEmbmsSessionEntry>();
        SharedPreferences dataStore = this.getSharedPreferences(
                                CdsEmbmsConsts.EMBMS_FILE, 0);
        for (int i = 0; i < CdsEmbmsConsts.MAX_SESSION_NUM; i ++) {
            String sessionName = CdsEmbmsConsts.EMBMS_SESSION_INFO + i;
            String sessionInfo = dataStore.getString(sessionName, "");
            CdsEmbmsSessionEntry entry = new CdsEmbmsSessionEntry();
            Log.i(TAG, "prepareListData:" + i + ":" + sessionInfo);
            if (sessionInfo.length() != 0) {
                String[] sessions = sessionInfo.split(CdsEmbmsConsts.SEP);
                for (int j = 0; j < sessions.length; j++) {
                    String s = sessions[j];
                        switch (j) {
                            case 0:
                                boolean v = (("true".equals(s)) ? true : false);
                                entry.setIsEnabled(v);
                                break;
                            case 1:
                                if (s != null) {
                                    entry.setTmgi(s);
                                } else {
                                    j = sessions.length; // Exit the loop
                                }
                                break;
                            case 2:
                                if (s != null) {
                                    entry.setSessionId(s);
                                } else {
                                    j = sessions.length; // Exit the loop
                                }
                                break;
                            case 3:
                                if (s != null) {
                                    entry.setSaiList(s);
                                }
                                break;
                            case 4:
                                if (s != null) {
                                    entry.setFreqList(s);
                                }
                                break;
                            default:
                                break;
                        }
                }
                // Send interested sessions.
                if (entry.getIsEnabled()) {
                    activateEmbmsSession(true, entry.toCmdString());
                }
            }
        }
    }
}