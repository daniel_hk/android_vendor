package com.mediatek.rcs.message.cloudbackup;

import android.content.Context;
import android.util.Log;

import com.mediatek.rcs.message.utils.RcsMessageUtils;

import java.io.File;
/**
 * FileUtils is a tool class.
 *
 */
public class FileUtils {
    private static final String CLASS_TAG = CloudBrUtils.MODULE_TAG + "FileUtils";

    /**
     * backup and restore message path.
     */
    public class ModulePath {
        public static final String FOLDER_MMS = "Mms";
        public static final String FOLDER_SMS = "Sms";
        public static final String FOLDER_IPMSG = "IpMessage";
        public static final String FOLDER_FAVORITE = "Favorite";
        public static final String FILE_EXT_PDU = ".pdu";

        public static final String SMS_VMSG = "sms.vmsg";
        public static final String MMS_XML = "mms_backup.xml";
        public static final String FT_FILE_PATHRECEIVE = "/storage/emulated/0/joyn/";
        public static final String FT_FILE_PATHSEND = "/storage/emulated/0/.Rcse/";
        public static final String BACKUP_DATA_FOLDER = "/storage/sdcard0/cloud/";
        public static final String RESTORE_BACKUP_FOLDER = "/storage/sdcard0/cloudTemp/";
        public static final String FAVORITE_FOLDER = "/storage/sdcard0/cloudFav/";
    }

    /**
     * create folders with the given folder path.
     * Delete the original folder if the folders has existed before create.
     * @param folderPath.
     * @return true if create folder success.
     */
    public static boolean createFolder(String folderPath) {
        Log.d(CLASS_TAG, "createFolder folderPath = " + folderPath);
        if (folderPath == null) {
            return false;
        }
        File folder = new File(folderPath);
        if (folder != null && folder.exists()) {
             boolean deleteResult = deleteFileOrFolder(folder);
             if (!deleteResult) {
                 Log.e(CLASS_TAG, "createFolder, deleteResult = " + deleteResult);
                 return false;
             }
        }
        Log.d(CLASS_TAG, "createFolder folder.mkdirs");
        return folder.mkdirs();
    }



    /**
     * get the ft message file path of favorite.
     * @return ft message path.
     */
    public static String getFavFtFilePath(Context context) {
        String path = RcsMessageUtils.getFavoritePath(context, "favorite_message");
        return path;
    }

    /**
     * delete file or folder.
     * @param file.
     * @return true if delete success.
     */
    public static boolean deleteFileOrFolder(File file) {
        boolean result = true;
        if (file == null || !file.exists()) {
            return result;
        }
        if (file.isFile()) {
            return file.delete();
        } else if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File f : files) {
                    if (!deleteFileOrFolder(f)) {
                        result = false;
                    }
                }
            }
            if (!file.delete()) {
                result = false;
            }
        }
        return result;
    }

    /**
     * @param folderName.
     * @return true means this folder is empty.
     */
    public static boolean isEmptyFolder(File folderName) {
        boolean ret = true;

        if (folderName != null && folderName.exists()) {
            if (folderName.isFile()) {
                ret = false;
            } else {
                File[] files = folderName.listFiles();
                if (files != null) {
                    for (File file : files) {
                        if (!isEmptyFolder(file)) {
                            ret = false;
                            break;
                        }
                    }
                }
            }
        }
        return ret;
    }
}
