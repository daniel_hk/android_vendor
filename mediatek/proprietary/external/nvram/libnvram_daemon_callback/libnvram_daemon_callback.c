/*****************************************************************************
 *  Copyright Statement:
 *  --------------------
 *  This software is protected by Copyright and the information contained
 *  herein is confidential. The software may not be copied and the information
 *  contained herein may not be used or disclosed except with the written
 *  permission of MediaTek Inc. (C) 2008
 *
 *  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 *  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
 *  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 *  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 *  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 *  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
 *  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
 *  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
 *  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 *  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 *  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
 *  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
 *  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
 *  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
 *  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
 *  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
 *
 *****************************************************************************/
/*****************************************************************************
 *
 * Filename:
 * ---------
 *   libnvram_sec.c
 *
 * Project:
 * --------
 *   YuSu
 *
 * Description:
 * ------------
 *   Security interface for moto.
 *
 *
 * Author:
 * -------
 *   Jian lin (mtk81139)
 *
 ****************************************************************************/

#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <cutils/properties.h>
#include "libnvram_daemon_callback.h"

extern pfCallbackForDaemon callback_for_nvram_daemon;

#include "libnvram_log.h"
#include "libnvram.h"
#include "libfile_op.h"
#include "CFG_PRODUCT_INFO_File.h"
#include "CFG_BT_File.h"
#include "CFG_Wifi_File.h"
#include "CFG_BT_Default.h"
#include "CFG_WIFI_Default.h"
extern pfCallbackForDaemon callback_for_nvram_daemon;
extern int iFilePRODUCT_INFOLID;
extern int iFileBTAddrLID;
extern int iFileWIFILID;
char * NVM_GetData(int file_lid)
{
	F_ID FileID;
	int result = 0;
	int RecSize = 0;
	int RecNum = 0;
	bool IsRead = true;
	char* Data = NULL;
	FileID = NVM_GetFileDesc(file_lid, &RecSize, &RecNum, IsRead);
	if(FileID.iFileDesc < 0){
		return Data;
		}
	Data = (char *)malloc(RecSize);
	if(Data == NULL){
		NVRAM_LOG("[NVRAM]: malloc pro_info failed,error:%s\n",strerror(errno));
		NVM_CloseFileDesc(FileID);
		return Data;
		}
	if(RecSize != read(FileID.iFileDesc,Data,RecSize)){
		NVRAM_LOG("[NVRAM]: read pro_info failed,error:%s\n",strerror(errno));
		NVM_CloseFileDesc(FileID);
		free(Data);
		Data = NULL;
		return Data;
		}
   	NVM_CloseFileDesc(FileID);
	return Data;
}
bool NVM_WriteData(int file_lid,char * Data)
{
	F_ID FileID;
	int result = 0;
	int RecSize = 0;
	int RecNum = 0;
	bool IsRead = false;
	int index = 0;
	if(Data == NULL){
		NVRAM_LOG("[NVRAM]:NVM_WriteData NULL Pointer\n");
		return false;
		}
	FileID = NVM_GetFileDesc(file_lid, &RecSize, &RecNum, IsRead);
	if(FileID.iFileDesc < 0){
		NVRAM_LOG("[NVRAM]: Open RAW Data error:%s\n",strerror(errno));
		free(Data);
		return false;
		}
	for(index = 0;index < RecNum;++index){
		if(RecSize != write(FileID.iFileDesc,Data,RecSize)){
			NVRAM_LOG("[NVRAM]: write Data failed:%s\n",strerror(errno));
			NVM_CloseFileDesc(FileID);
			free(Data);
			return false;
			}
		}
	free(Data);
   	NVM_CloseFileDesc(FileID);
	return true;
}
static bool NVM_Sync_bt_wifi_address()
{
	struct timeval time_tv;
	struct timezone time_tz;
	struct _MT6620_CFG_PARAM_STRUCT *WIFI_INFO = NULL;
	ap_nvram_btradio_mt6610_struct *BT_INFO =NULL;
	unsigned char default_bt_addr[6] = {0x94,0x71,0xBC,0x91,0x65,0x00};   //add by Xiong Bailu
	unsigned char default_wifi_addr[6] = {0x00,0x66,0x75,0x2d,0xe4,0x00}; //add by Xiong Bailu
	int index = 0;
	int seed;
	bool flag = 0;
	NVRAM_LOG("[NvRAM]:sync BT/WIFI addres(in)\n");
	BT_INFO = (ap_nvram_btradio_mt6610_struct*) NVM_GetData(iFileBTAddrLID);
	if(BT_INFO == NULL){
		NVRAM_LOG("[NvRAM]:PRO_INFO NULL pointer!\n");
		return false;
		}
	if((BT_INFO->addr[0] == 0x00) ||(BT_INFO->addr[0] == 0x72))
	{    //set bt mac address
		memcpy(BT_INFO, &stBtDefault_6752, sizeof(ap_nvram_btradio_mt6610_struct));  //not for bt address, for other data
	    	for(index = 1;index < 6 ;++index)
        	{
			gettimeofday( &time_tv, &time_tz );
			seed = (unsigned int)time_tv.tv_sec;
			seed += (unsigned int)time_tv.tv_usec;
			seed *= getpid();
			seed += &seed;
			seed %= RAND_MAX;
			srand(seed);
			BT_INFO->addr[index] = 0 + (int)( 255.0 *rand()/(RAND_MAX + 1.0));   
        	}
		memcpy(BT_INFO->addr,default_bt_addr,3);
		BT_INFO->addr[0] = 0x94;	    
		if(true != NVM_WriteData(iFileBTAddrLID,(char *)BT_INFO))
		NVRAM_LOG("[NvRAM]:sync BT address faile!\n");
		NVM_AddBackupFileNum(iFileBTAddrLID);
	}
	WIFI_INFO = (struct _MT6620_CFG_PARAM_STRUCT *) NVM_GetData(iFileWIFILID);
	if(WIFI_INFO == NULL){
		NVRAM_LOG("[NvRAM]:WIFI_INFO NULL pointer!\n");
		return false;
	}
	if(WIFI_INFO->aucMacAddress[0] == 0x00)
	{
		memcpy(WIFI_INFO, &stWifiCfgDefault, sizeof(WIFI_CFG_PARAM_STRUCT));  //not for bt address, for other data
	   	for(index = 1;index < 6 ;++index)
        	{
			gettimeofday( &time_tv, &time_tz );
			seed = (unsigned int)time_tv.tv_sec;
			seed += (unsigned int)time_tv.tv_usec;
			seed *= getpid();
			seed += &seed;
			seed %= RAND_MAX;
			srand(seed);
            		WIFI_INFO->aucMacAddress[index] = 0 + (int)( 255.0 *rand()/(RAND_MAX + 1.0));
        	}			     
		memcpy(WIFI_INFO->aucMacAddress,default_wifi_addr,3);
		WIFI_INFO->aucMacAddress[0] = 0x10;
		if(true != NVM_WriteData(iFileWIFILID,(char *)WIFI_INFO))
			NVRAM_LOG("[NvRAM]:sync WIFI address faile!\n");
		NVM_AddBackupFileNum(iFileWIFILID);
	}
done:
	NVRAM_LOG("[NvRAM]:sync BT/WIFI addres(out)\n");
	return true;
}
int my_callback(void)
{
    ALOGD("nvram daemon callback will run!!!");
    NVM_Sync_bt_wifi_address();
    return 0;
}

int init_callback(void)
{
//#ifdef MTK_PRODUCT_INFO_SUPPORT
    callback_for_nvram_daemon = my_callback;
//#endif
    return 0;
}
