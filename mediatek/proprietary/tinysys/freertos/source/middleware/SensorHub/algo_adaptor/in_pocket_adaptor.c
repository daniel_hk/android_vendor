#include "stdio.h"
#include "sensors.h"
#include "sensor_manager.h"
#include "feature_struct.h"
#include "algo_adaptor.h"

#ifdef _PC_VERSION_
#define LOGE(fmt, args...)    printf("[In Pocket] ERR: "fmt, ##args)
#define LOGD(fmt, args...)    printf("[In Pocket] DBG: "fmt, ##args)
#else
#define LOGE(fmt, args...)    PRINTF_D("[In Pocket] ERR: "fmt, ##args)
#define LOGD(fmt, args...)    PRINTF_D("[In Pocket] DBG: "fmt, ##args)
#endif

#define IN_POCKET_INPUT_SAMPLE_DELAY    60      // unit: ms
#define IN_POCKET_INPUT_ACCUMULATE      160     // unit: ms

#define IN_POCKET_STABLE_BUFFER_TIME    6000    // timer for stable result (unit: ms)
#define IN_POCKET_INITIAL_BUFFER_TIME   1000    // timer for distinguish "initial value" or "on_change value" (unit: ms)

#define IN_POCKET_STATE_UNKNOWN         -1
#define IN_POCKET_STATE_FAR             0
#define IN_POCKET_STATE_NEAR            1

//#define _INPOCKET_DEBUG_

struct input_list_t input_comp_prx;
struct input_list_t input_comp_acc;

struct in_pocket_adaptor_t {
    uint64_t time_stamp_ns;         // unit: ns
    uint32_t inpk_operate_ref_ms;   // last inpk operate time (unit: ms)
    uint32_t inpk_state_ref_ms;     // last inpk state change time (unit: ms)

    int inpk_state;                 // inpk
    int last_stable_inpk_state;     // last stable inpk state
};
static struct in_pocket_adaptor_t in_pocket_adaptor;
static int start_notify_in_pocket = 0;

static INT32 run_in_pocket(struct data_t * const output)
{
    output->data_exist_count = 1;
    output->data->time_stamp = in_pocket_adaptor.time_stamp_ns;
    output->data->sensor_type = SENSOR_TYPE_INPOCKET;
    output->data->inpocket_event.state = IN_POCKET_STATE_NEAR;
    return 1;
}

static INT32 set_in_pocket_data(const struct data_t *input_list, void *reserve)
{
    struct data_unit_t *data_start = input_list->data;
    uint32_t input_time_stamp_ms = data_start->time_stamp / 1000000;
    in_pocket_adaptor.time_stamp_ns = data_start->time_stamp;

    if (data_start->sensor_type == SENSOR_TYPE_PROXIMITY) {
        int current_state = ((int)data_start->proximity_t.oneshot == 0) ? IN_POCKET_STATE_NEAR : IN_POCKET_STATE_FAR;
#ifdef _INPOCKET_DEBUG_
        PRINTF_D("[Inpocket_Pro_State] %u, %d\n", input_time_stamp_ms, current_state);
#endif

        // last_stable_inpk_state = IN_POCKET_STATE_UNKNOWN
        if (start_notify_in_pocket && (in_pocket_adaptor.last_stable_inpk_state == IN_POCKET_STATE_UNKNOWN)) {
            if ((in_pocket_adaptor.inpk_operate_ref_ms > input_time_stamp_ms)
                    || (input_time_stamp_ms - in_pocket_adaptor.inpk_operate_ref_ms) < IN_POCKET_INITIAL_BUFFER_TIME) {
                // proximity sensor output is "initial"
                in_pocket_adaptor.last_stable_inpk_state = current_state;
                in_pocket_adaptor.inpk_state = current_state;
            } else {
                // proximity sensor output is "change"
                in_pocket_adaptor.last_stable_inpk_state = (current_state == 0) ? 1 : 0;
                in_pocket_adaptor.inpk_state = (current_state == 0) ? 1 : 0;
            }
            in_pocket_adaptor.inpk_state_ref_ms = input_time_stamp_ms;
#ifdef _INPOCKET_DEBUG_
            PRINTF_D("[Inpocket_Pro_Initial] %u, %d\n", input_time_stamp_ms, in_pocket_adaptor.inpk_state);
#endif
        }

        // last "different" inpk_state and inpk_state_ref_ms
        if (start_notify_in_pocket && (in_pocket_adaptor.last_stable_inpk_state != IN_POCKET_STATE_UNKNOWN)
                && (current_state != in_pocket_adaptor.inpk_state)) {
            in_pocket_adaptor.inpk_state = current_state;
            in_pocket_adaptor.inpk_state_ref_ms = input_time_stamp_ms;
#ifdef _INPOCKET_DEBUG_
            PRINTF_D("[Inpocket_Pro_Change] %u, %d\n", input_time_stamp_ms, in_pocket_adaptor.inpk_state);
#endif
        }

    } else if (data_start->sensor_type == SENSOR_TYPE_ACCELEROMETER) {
        // only use for time counter
        if (start_notify_in_pocket && (in_pocket_adaptor.last_stable_inpk_state != in_pocket_adaptor.inpk_state) \
                && (input_time_stamp_ms > in_pocket_adaptor.inpk_state_ref_ms)
                && (input_time_stamp_ms - in_pocket_adaptor.inpk_state_ref_ms >= IN_POCKET_STABLE_BUFFER_TIME)) {
            // inpk state different from last_stable_inpk_state and stable time larger than IN_POCKET_STABLE_BUFFER_TIME
            in_pocket_adaptor.last_stable_inpk_state = in_pocket_adaptor.inpk_state;

            if (in_pocket_adaptor.last_stable_inpk_state == IN_POCKET_STATE_NEAR) {
                sensor_subsys_algorithm_notify(SENSOR_TYPE_INPOCKET);
                PRINTF_D("[In-pocket] in-pocket at %u, %d", input_time_stamp_ms, in_pocket_adaptor.last_stable_inpk_state);
            }
        }
    }

    return 1;
}

static INT32 in_pocket_operate(Sensor_Command command, void* buffer_in, INT32 size_in, \
                               void* buffer_out, INT32 size_out)
{
    int err = 0;
    int value = 0;
    if (NULL == buffer_in) {
        return -1;
    }
    switch (command) {
        case ACTIVATE:
            if ((buffer_in == NULL) || (size_in < sizeof(int))) {
                err = -1;
            } else {
                value = *(int *)buffer_in;
                if (0 == value) {
                    start_notify_in_pocket = 0;

                    in_pocket_adaptor.inpk_operate_ref_ms = read_xgpt_stamp_ns() / 1000000;

                    in_pocket_adaptor.inpk_state = IN_POCKET_STATE_UNKNOWN;
                    in_pocket_adaptor.last_stable_inpk_state = IN_POCKET_STATE_UNKNOWN;

                } else {
                    in_pocket_adaptor.inpk_operate_ref_ms = read_xgpt_stamp_ns() / 1000000;

                    start_notify_in_pocket = 1;
                }
            }
            break;
        default:
            break;
    }
    PRINTF_D("[In-pocket] start_notify_in_pocket = %d, %u", start_notify_in_pocket, in_pocket_adaptor.inpk_operate_ref_ms);
    return err;
}

static int in_pocket_register(void)
{
    int ret = 0;
    in_pocket_adaptor.time_stamp_ns = 0;
    in_pocket_adaptor.inpk_operate_ref_ms = 0;
    in_pocket_adaptor.inpk_state_ref_ms = 0;

    in_pocket_adaptor.inpk_state = IN_POCKET_STATE_UNKNOWN;
    in_pocket_adaptor.last_stable_inpk_state = IN_POCKET_STATE_UNKNOWN;

    input_comp_prx.input_type = SENSOR_TYPE_PROXIMITY;
    input_comp_prx.sampling_delay = IN_POCKET_INPUT_SAMPLE_DELAY * ACC_EVENT_COUNT_PER_FIFO_LOOP;

    input_comp_prx.next_input = &input_comp_acc;
    input_comp_acc.input_type = SENSOR_TYPE_ACCELEROMETER;
    input_comp_acc.sampling_delay = IN_POCKET_INPUT_SAMPLE_DELAY * ACC_EVENT_COUNT_PER_FIFO_LOOP;

    input_comp_acc.next_input = NULL;

    struct SensorDescriptor_t in_pocket_desp = {
        SENSOR_TYPE_INPOCKET, 1, one_shot, {20, 0},
        &input_comp_prx, in_pocket_operate, run_in_pocket,
        set_in_pocket_data, IN_POCKET_INPUT_ACCUMULATE
    };

    ret = sensor_subsys_algorithm_register_type(&in_pocket_desp);
    if (ret < 0)
        LOGE("fail to register in pocket. \r\n");

    ret = sensor_subsys_algorithm_register_data_buffer(SENSOR_TYPE_INPOCKET, 1);
    if (ret < 0)
        LOGE("fail to register buffer for in pocket \r\n");
    return ret;
}

int in_pocket_init(void)
{
    in_pocket_register();
    return 1;
}
#ifdef _EVEREST_MODULE_DECLARE_
MODULE_DECLARE(virt_pkt_init, MOD_VIRT_SENSOR, in_pocket_init);
#endif
