#include <utils/Log.h>
#include <fcntl.h>
#include <math.h>

#include "SonyPdafTransCoord.h"
#include <pd_imx230mipiraw.h>
#include <aaa_log.h>
#include <cutils/properties.h>
#include <stdlib.h>
#include "string.h"

#define LOG_TAG "pd_buf_mg_imx230mipiraw"

//define calibration block number for imx230. This is depended on sensor setting.
#define _PD_BLK_NUM_W_ 8
#define _PD_BLK_NUM_H_ 6
//define pd block number for imx230. This is depended on calibration data in EEPROM/OTP
#define _CALI_BLK_NUM_W_ 8
#define _CALI_BLK_NUM_H_ 6

//Sensor output data isze is 16*12*5(PD/CL data)+5(Header) Bytes, no matter what kind of sensor setting is.
#define dSize 16*12
//calibration data block size.
#define cSize _CALI_BLK_NUM_W_*_CALI_BLK_NUM_H_
SPDProfile_t iPdProfile_temp;

PDBufMgrOpen*
PD_IMX230MIPIRAW::getInstance()
{
    static PD_IMX230MIPIRAW singleton;
    return &singleton;

}


PD_IMX230MIPIRAW::PD_IMX230MIPIRAW()
{
    m_phase_difference = new MUINT16 [dSize];
    m_confidence_level = new MUINT16 [dSize];
    m_calibration_data = new MUINT16 [cSize];
    m_ParamDataForConvertingAddress = new SonyPdLibSensorCoordSetting_t;



    m_XKnotNum1 = _CALI_BLK_NUM_W_;
    m_YKnotNum1 = _CALI_BLK_NUM_H_;

    m_XKnotNum2 = _PD_BLK_NUM_W_;
    m_YKnotNum2 = _PD_BLK_NUM_H_;

    m_PointNumForThrLine = 4;

    m_CurrMode = 0; //default : all pixel mode.

    // Malloc
    m_SonyPdLibInputData.p_SlopeData				= (signed long *)malloc( sizeof(signed long) * m_XKnotNum1 * m_YKnotNum1 );
    m_SonyPdLibInputData.p_OffsetData 				= (signed long *)malloc( sizeof(signed long) * m_XKnotNum1 * m_YKnotNum1 );
    m_SonyPdLibInputData.p_XAddressKnotSlopeOffset	= (unsigned short *)malloc( sizeof(unsigned short) * m_XKnotNum1 );
    m_SonyPdLibInputData.p_YAddressKnotSlopeOffset	= (unsigned short *)malloc( sizeof(unsigned short) * m_YKnotNum1 );
    m_SonyPdLibInputData.p_DefocusOKNGThrLine 		= (DefocusOKNGThrLine_t *)malloc( sizeof(DefocusOKNGThrLine_t) * m_XKnotNum1 * m_YKnotNum1 );

    for( unsigned short i = 0; i < m_XKnotNum1*m_YKnotNum1; i++ )
    {
        m_SonyPdLibInputData.p_DefocusOKNGThrLine[i].p_AnalogGain = (unsigned long *)malloc( sizeof(unsigned long) * m_PointNumForThrLine );
        m_SonyPdLibInputData.p_DefocusOKNGThrLine[i].p_Confidence = (unsigned long *)malloc( sizeof(unsigned long) * m_PointNumForThrLine );
    }

    m_SonyPdLibInputData.p_XAddressKnotDefocusOKNG	= (unsigned short *)malloc( sizeof(unsigned short) * m_XKnotNum1 );
    m_SonyPdLibInputData.p_YAddressKnotDefocusOKNG	= (unsigned short *)malloc( sizeof(unsigned short) * m_YKnotNum1 );


    ALOGD("[%s]\n", __FUNCTION__);

}

PD_IMX230MIPIRAW::~PD_IMX230MIPIRAW()
{
    if( m_phase_difference)
        delete m_phase_difference;

    if( m_confidence_level)
        delete m_confidence_level;

    if( m_calibration_data)
        delete m_calibration_data;

    if( m_ParamDataForConvertingAddress)
        delete m_ParamDataForConvertingAddress;

    m_phase_difference = NULL;
    m_confidence_level = NULL;
    m_calibration_data = NULL;
    m_ParamDataForConvertingAddress = NULL;


    // Finalize
    // Free
    free ( m_SonyPdLibInputData.p_YAddressKnotDefocusOKNG );
    free ( m_SonyPdLibInputData.p_XAddressKnotDefocusOKNG );

    for( unsigned short i = 0; i < m_XKnotNum1*m_YKnotNum1; i++ )
    {
        free ( m_SonyPdLibInputData.p_DefocusOKNGThrLine[i].p_Confidence );
        free ( m_SonyPdLibInputData.p_DefocusOKNGThrLine[i].p_AnalogGain );
    }

    free ( m_SonyPdLibInputData.p_DefocusOKNGThrLine );
    free ( m_SonyPdLibInputData.p_YAddressKnotSlopeOffset );
    free ( m_SonyPdLibInputData.p_XAddressKnotSlopeOffset );
    free ( m_SonyPdLibInputData.p_OffsetData );
    free ( m_SonyPdLibInputData.p_SlopeData  );

}



MINT32 PD_IMX230MIPIRAW::SetRegData(unsigned short &CurrMode)
{
    MINT32 ret=D_SONY_PD_LIB_REGDATA_IS_NG;

    memset( m_ParamDataForConvertingAddress, 0, sizeof(SonyPdLibSensorCoordSetting_t));
    SonyPdLibSensorCoordRegData_t ImageSensorRegisterData;

    switch( iPdProfile_temp.uImgYsz)
    {
        case 4016 : // 4:3 full mode
//From reg tool at 2016-7-16 19:50             
ImageSensorRegisterData.reg_addr_0x0101 = 0x00;
ImageSensorRegisterData.reg_addr_0x0220 = 0x00;
ImageSensorRegisterData.reg_addr_0x0221 = 0x11;
ImageSensorRegisterData.reg_addr_0x0344 = 0x00;
ImageSensorRegisterData.reg_addr_0x0345 = 0x00;
ImageSensorRegisterData.reg_addr_0x0346 = 0x00;
ImageSensorRegisterData.reg_addr_0x0347 = 0x00;
ImageSensorRegisterData.reg_addr_0x0348 = 0x14;
ImageSensorRegisterData.reg_addr_0x0349 = 0xDF;
ImageSensorRegisterData.reg_addr_0x034A = 0x0F;
ImageSensorRegisterData.reg_addr_0x034B = 0xAF;
ImageSensorRegisterData.reg_addr_0x0381 = 0x01;
ImageSensorRegisterData.reg_addr_0x0383 = 0x01;
ImageSensorRegisterData.reg_addr_0x0385 = 0x01;
ImageSensorRegisterData.reg_addr_0x0387 = 0x01;
ImageSensorRegisterData.reg_addr_0x0900 = 0x00;
ImageSensorRegisterData.reg_addr_0x0901 = 0x11;
ImageSensorRegisterData.reg_addr_0x034C = 0x14;
ImageSensorRegisterData.reg_addr_0x034D = 0xE0;
ImageSensorRegisterData.reg_addr_0x034E = 0x0F;
ImageSensorRegisterData.reg_addr_0x034F = 0xB0;
ImageSensorRegisterData.reg_addr_0x0401 = 0x00;
ImageSensorRegisterData.reg_addr_0x0404 = 0x00;
ImageSensorRegisterData.reg_addr_0x0405 = 0x10;
ImageSensorRegisterData.reg_addr_0x0408 = 0x00;
ImageSensorRegisterData.reg_addr_0x0409 = 0x00;
ImageSensorRegisterData.reg_addr_0x040A = 0x00;
ImageSensorRegisterData.reg_addr_0x040B = 0x00;
ImageSensorRegisterData.reg_addr_0x040C = 0x14;
ImageSensorRegisterData.reg_addr_0x040D = 0xE0;
ImageSensorRegisterData.reg_addr_0x040E = 0x0F;
ImageSensorRegisterData.reg_addr_0x040F = 0xB0;
        break;
        case 2008 : // 4:3 2bin mode
//From reg tool at 2016-7-16 19:50             
ImageSensorRegisterData.reg_addr_0x0101 = 0x00;
ImageSensorRegisterData.reg_addr_0x0220 = 0x00;
ImageSensorRegisterData.reg_addr_0x0221 = 0x11;
ImageSensorRegisterData.reg_addr_0x0344 = 0x00;
ImageSensorRegisterData.reg_addr_0x0345 = 0x00;
ImageSensorRegisterData.reg_addr_0x0346 = 0x00;
ImageSensorRegisterData.reg_addr_0x0347 = 0x00;
ImageSensorRegisterData.reg_addr_0x0348 = 0x14;
ImageSensorRegisterData.reg_addr_0x0349 = 0xDF;
ImageSensorRegisterData.reg_addr_0x034A = 0x0F;
ImageSensorRegisterData.reg_addr_0x034B = 0xAF;
ImageSensorRegisterData.reg_addr_0x0381 = 0x01;
ImageSensorRegisterData.reg_addr_0x0383 = 0x01;
ImageSensorRegisterData.reg_addr_0x0385 = 0x01;
ImageSensorRegisterData.reg_addr_0x0387 = 0x01;
ImageSensorRegisterData.reg_addr_0x0900 = 0x01;
ImageSensorRegisterData.reg_addr_0x0901 = 0x22;
ImageSensorRegisterData.reg_addr_0x034C = 0x0A;
ImageSensorRegisterData.reg_addr_0x034D = 0x70;
ImageSensorRegisterData.reg_addr_0x034E = 0x07;
ImageSensorRegisterData.reg_addr_0x034F = 0xD8;
ImageSensorRegisterData.reg_addr_0x0401 = 0x00;
ImageSensorRegisterData.reg_addr_0x0404 = 0x00;
ImageSensorRegisterData.reg_addr_0x0405 = 0x10;
ImageSensorRegisterData.reg_addr_0x0408 = 0x00;
ImageSensorRegisterData.reg_addr_0x0409 = 0x00;
ImageSensorRegisterData.reg_addr_0x040A = 0x00;
ImageSensorRegisterData.reg_addr_0x040B = 0x00;
ImageSensorRegisterData.reg_addr_0x040C = 0x0A;
ImageSensorRegisterData.reg_addr_0x040D = 0x70;
ImageSensorRegisterData.reg_addr_0x040E = 0x07;
ImageSensorRegisterData.reg_addr_0x040F = 0xD8;
        
        break;
        case 3008 : // 16:9 full mode
//From reg tool at 2016-7-16 19:50 
ImageSensorRegisterData.reg_addr_0x0101 = 0x00;
ImageSensorRegisterData.reg_addr_0x0220 = 0x00;
ImageSensorRegisterData.reg_addr_0x0221 = 0x11;
ImageSensorRegisterData.reg_addr_0x0344 = 0x00;
ImageSensorRegisterData.reg_addr_0x0345 = 0x00;
ImageSensorRegisterData.reg_addr_0x0346 = 0x01;
ImageSensorRegisterData.reg_addr_0x0347 = 0xF8;
ImageSensorRegisterData.reg_addr_0x0348 = 0x14;
ImageSensorRegisterData.reg_addr_0x0349 = 0xDF;
ImageSensorRegisterData.reg_addr_0x034A = 0x0D;
ImageSensorRegisterData.reg_addr_0x034B = 0xB7;
ImageSensorRegisterData.reg_addr_0x0381 = 0x01;
ImageSensorRegisterData.reg_addr_0x0383 = 0x01;
ImageSensorRegisterData.reg_addr_0x0385 = 0x01;
ImageSensorRegisterData.reg_addr_0x0387 = 0x01;
ImageSensorRegisterData.reg_addr_0x0900 = 0x00;
ImageSensorRegisterData.reg_addr_0x0901 = 0x11;
ImageSensorRegisterData.reg_addr_0x034C = 0x14;
ImageSensorRegisterData.reg_addr_0x034D = 0xE0;
ImageSensorRegisterData.reg_addr_0x034E = 0x0B;
ImageSensorRegisterData.reg_addr_0x034F = 0xC0;
ImageSensorRegisterData.reg_addr_0x0401 = 0x00;
ImageSensorRegisterData.reg_addr_0x0404 = 0x00;
ImageSensorRegisterData.reg_addr_0x0405 = 0x10;
ImageSensorRegisterData.reg_addr_0x0408 = 0x00;
ImageSensorRegisterData.reg_addr_0x0409 = 0x00;
ImageSensorRegisterData.reg_addr_0x040A = 0x00;
ImageSensorRegisterData.reg_addr_0x040B = 0x00;
ImageSensorRegisterData.reg_addr_0x040C = 0x14;
ImageSensorRegisterData.reg_addr_0x040D = 0xE0;
ImageSensorRegisterData.reg_addr_0x040E = 0x0B;
ImageSensorRegisterData.reg_addr_0x040F = 0xC0;

        break;
        case 1504 : // 16:9 2bin mode
//From reg tool at 2016-7-16 19:50 
ImageSensorRegisterData.reg_addr_0x0101 = 0x00;
ImageSensorRegisterData.reg_addr_0x0220 = 0x00;
ImageSensorRegisterData.reg_addr_0x0221 = 0x11;
ImageSensorRegisterData.reg_addr_0x0344 = 0x00;
ImageSensorRegisterData.reg_addr_0x0345 = 0x00;
ImageSensorRegisterData.reg_addr_0x0346 = 0x01;
ImageSensorRegisterData.reg_addr_0x0347 = 0xF8;
ImageSensorRegisterData.reg_addr_0x0348 = 0x14;
ImageSensorRegisterData.reg_addr_0x0349 = 0xDF;
ImageSensorRegisterData.reg_addr_0x034A = 0x0D;
ImageSensorRegisterData.reg_addr_0x034B = 0xB7;
ImageSensorRegisterData.reg_addr_0x0381 = 0x01;
ImageSensorRegisterData.reg_addr_0x0383 = 0x01;
ImageSensorRegisterData.reg_addr_0x0385 = 0x01;
ImageSensorRegisterData.reg_addr_0x0387 = 0x01;
ImageSensorRegisterData.reg_addr_0x0900 = 0x01;
ImageSensorRegisterData.reg_addr_0x0901 = 0x22;
ImageSensorRegisterData.reg_addr_0x034C = 0x0A;
ImageSensorRegisterData.reg_addr_0x034D = 0x70;
ImageSensorRegisterData.reg_addr_0x034E = 0x05;
ImageSensorRegisterData.reg_addr_0x034F = 0xE0;
ImageSensorRegisterData.reg_addr_0x0401 = 0x00;
ImageSensorRegisterData.reg_addr_0x0404 = 0x00;
ImageSensorRegisterData.reg_addr_0x0405 = 0x10;
ImageSensorRegisterData.reg_addr_0x0408 = 0x00;
ImageSensorRegisterData.reg_addr_0x0409 = 0x00;
ImageSensorRegisterData.reg_addr_0x040A = 0x00;
ImageSensorRegisterData.reg_addr_0x040B = 0x00;
ImageSensorRegisterData.reg_addr_0x040C = 0x0A;
ImageSensorRegisterData.reg_addr_0x040D = 0x70;
ImageSensorRegisterData.reg_addr_0x040E = 0x05;
ImageSensorRegisterData.reg_addr_0x040F = 0xE0;

        break;
        case 1080 : // 1080 mode 
//From reg tool at 2016-7-16 19:50 
ImageSensorRegisterData.reg_addr_0x0101 = 0x00;
ImageSensorRegisterData.reg_addr_0x0220 = 0x00;
ImageSensorRegisterData.reg_addr_0x0221 = 0x11;
ImageSensorRegisterData.reg_addr_0x0344 = 0x00;
ImageSensorRegisterData.reg_addr_0x0345 = 0x00;
ImageSensorRegisterData.reg_addr_0x0346 = 0x01;
ImageSensorRegisterData.reg_addr_0x0347 = 0xF8;
ImageSensorRegisterData.reg_addr_0x0348 = 0x14;
ImageSensorRegisterData.reg_addr_0x0349 = 0xDF;
ImageSensorRegisterData.reg_addr_0x034A = 0x0D;
ImageSensorRegisterData.reg_addr_0x034B = 0xB7;
ImageSensorRegisterData.reg_addr_0x0381 = 0x01;
ImageSensorRegisterData.reg_addr_0x0383 = 0x01;
ImageSensorRegisterData.reg_addr_0x0385 = 0x01;
ImageSensorRegisterData.reg_addr_0x0387 = 0x01;
ImageSensorRegisterData.reg_addr_0x0900 = 0x01;
ImageSensorRegisterData.reg_addr_0x0901 = 0x22;
ImageSensorRegisterData.reg_addr_0x034C = 0x07;
ImageSensorRegisterData.reg_addr_0x034D = 0x80;
ImageSensorRegisterData.reg_addr_0x034E = 0x04;
ImageSensorRegisterData.reg_addr_0x034F = 0x38;
ImageSensorRegisterData.reg_addr_0x0401 = 0x02;
ImageSensorRegisterData.reg_addr_0x0404 = 0x00;
ImageSensorRegisterData.reg_addr_0x0405 = 0x16;
ImageSensorRegisterData.reg_addr_0x0408 = 0x00;
ImageSensorRegisterData.reg_addr_0x0409 = 0x10;
ImageSensorRegisterData.reg_addr_0x040A = 0x00;
ImageSensorRegisterData.reg_addr_0x040B = 0x0A;
ImageSensorRegisterData.reg_addr_0x040C = 0x0A;
ImageSensorRegisterData.reg_addr_0x040D = 0x52;
ImageSensorRegisterData.reg_addr_0x040E = 0x05;
ImageSensorRegisterData.reg_addr_0x040F = 0xCE;

        break;
        case 2160 : // 4K mode 
//From reg tool at 2016-7-16 19:50 
ImageSensorRegisterData.reg_addr_0x0101 = 0x00;
ImageSensorRegisterData.reg_addr_0x0220 = 0x00;
ImageSensorRegisterData.reg_addr_0x0221 = 0x11;
ImageSensorRegisterData.reg_addr_0x0344 = 0x00;
ImageSensorRegisterData.reg_addr_0x0345 = 0x00;
ImageSensorRegisterData.reg_addr_0x0346 = 0x01;
ImageSensorRegisterData.reg_addr_0x0347 = 0xF8;
ImageSensorRegisterData.reg_addr_0x0348 = 0x14;
ImageSensorRegisterData.reg_addr_0x0349 = 0xDF;
ImageSensorRegisterData.reg_addr_0x034A = 0x0D;
ImageSensorRegisterData.reg_addr_0x034B = 0xB7;
ImageSensorRegisterData.reg_addr_0x0381 = 0x01;
ImageSensorRegisterData.reg_addr_0x0383 = 0x01;
ImageSensorRegisterData.reg_addr_0x0385 = 0x01;
ImageSensorRegisterData.reg_addr_0x0387 = 0x01;
ImageSensorRegisterData.reg_addr_0x0900 = 0x00;
ImageSensorRegisterData.reg_addr_0x0901 = 0x11;
ImageSensorRegisterData.reg_addr_0x034C = 0x10;
ImageSensorRegisterData.reg_addr_0x034D = 0x00;
ImageSensorRegisterData.reg_addr_0x034E = 0x08;
ImageSensorRegisterData.reg_addr_0x034F = 0x70;
ImageSensorRegisterData.reg_addr_0x0401 = 0x02;
ImageSensorRegisterData.reg_addr_0x0404 = 0x00;
ImageSensorRegisterData.reg_addr_0x0405 = 0x14;
ImageSensorRegisterData.reg_addr_0x0408 = 0x00;
ImageSensorRegisterData.reg_addr_0x0409 = 0x70;
ImageSensorRegisterData.reg_addr_0x040A = 0x00;
ImageSensorRegisterData.reg_addr_0x040B = 0x9A;
ImageSensorRegisterData.reg_addr_0x040C = 0x14;
ImageSensorRegisterData.reg_addr_0x040D = 0x02;
ImageSensorRegisterData.reg_addr_0x040E = 0x0A;
ImageSensorRegisterData.reg_addr_0x040F = 0x8E;
        
        break;
        default : 
        ALOGD("[Sony]PD mode is not confirmed\n");
        break;		
    }		

        // extract parameter data for converting address from image sensor register data
        ret = SonyPdLibInterpretRegData(
                  //Input
                  &ImageSensorRegisterData, 		// image sensor register data
                  //Output
                  m_ParamDataForConvertingAddress );	// parameter data for converting address
	

    return ret;



}

MBOOL PD_IMX230MIPIRAW::IsSupport( SPDProfile_t &iPdProfile)
{
	MBOOL ret = MFALSE;
	iPdProfile_temp = iPdProfile;
	//enable/disable debug log	
    char value[PROPERTY_VALUE_MAX] = {'\0'};
    property_get("debug.af_mgr.enable", value, "0");
    m_bDebugEnable = atoi(value);


    //binning ( V:1/2, H:1/2) mode and all-pixel mode.
	if ( iPdProfile.uImgYsz==2008 || iPdProfile.uImgYsz==1080 || iPdProfile.uImgYsz==1504)
	{
		ret = MTRUE;
		m_CurrMode = 2;
	}
	else if ( iPdProfile.uImgYsz==4016 || iPdProfile.uImgYsz==3008 || iPdProfile.uImgYsz==2160)
	{
		ret = MTRUE;
		m_CurrMode = 0;
	}
	else if ( iPdProfile.uImgYsz==720 || iPdProfile.uImgYsz==480 )
	{
		//ret = MTRUE;
		m_CurrMode = 4;
	}
	else 
	{
		ALOGD("[Sony]PD mode is not Supported (%d, %d)\n", iPdProfile.uImgXsz, iPdProfile.uImgYsz);
	}	
		ALOGD("[Sony]m_Currmode (%d)\n", m_CurrMode);
	//for transform ROI coordinate.
	m_IsCfgCoordSetting = SetRegData( m_CurrMode);

	ALOGD("[SONY] [%s] %d, CurMode=%d, ImgSZ=(%d, %d)\n", __FUNCTION__, ret, m_CurrMode, iPdProfile.uImgXsz, iPdProfile.uImgYsz);
	return ret;

}

MINT32 PD_IMX230MIPIRAW::GetPDCalSz()
{
	return 96;
}


MBOOL PD_IMX230MIPIRAW::GetPDInfo2HybridAF( MINT32 i4InArySz, MINT32 *i4OutAry)
{
    MBOOL ret=MFALSE;
    //rest
    memset( i4OutAry, 0, i4InArySz*sizeof(MINT32));

    //i4InArySz should be 10 which is defined by core PD algorithm.
    if( i4InArySz==10 && i4OutAry)
    {
        // output block size setting for Hybrid AF to crop PD alayzing area.
        switch( m_CurrMode)
        {
            ret=MTRUE;
        case 0 : // all pixel mode
            i4OutAry[0]=5344/_PD_BLK_NUM_W_;
            i4OutAry[1]=4016/_PD_BLK_NUM_H_;
            break;
        case 2 : // bining mode
            i4OutAry[0]=2672/_PD_BLK_NUM_W_;
            i4OutAry[1]=2008/_PD_BLK_NUM_H_;
            break;
        default :
            ret=MFALSE;
            ALOGD("Current Sensor mode(%d) is not support PDAF\n", m_CurrMode);
            break;
        }
    }
    else
    {
        ret=MFALSE;
        ALOGD("[%s] Fail, Sz=%d, Addr=0x%x\n", __FUNCTION__, i4InArySz, i4OutAry);
    }

    return ret;


}



MBOOL PD_IMX230MIPIRAW::ExtractPDCL()
{
    MUINT8 flexibleEn = m_databuf[0];
    MUINT8 modesel = m_databuf[1]>>6;


    MUINT8 offset = 5;
    MUINT8 *ptr = &m_databuf[offset];

    for( int i=0; i<dSize; i++)
    {
        m_confidence_level[i] = 0xff  & (ptr[i*5]);
        m_phase_difference[i] = 0x3ff & (ptr[i*5+1]<<2 | ptr[i*5+2]>>6);

    }

    char value[200] = {'\0'};
    property_get("vc.dump.enable", value, "0");
    MBOOL bEnable = atoi(value);
    if (bEnable)
    {
        char fileName[64];
        sprintf(fileName, "/sdcard/vc/%d_pd.raw", m_frm_num);
        FILE *fp = fopen(fileName, "w");
        if (NULL == fp)
        {
            return MFALSE;
        }
        fwrite(reinterpret_cast<void *>(m_phase_difference), 1, 16*12*2, fp);
        fclose(fp);

        sprintf(fileName, "/sdcard/vc/%d_cl.raw", m_frm_num);
        fp = fopen(fileName, "w");
        if (NULL == fp)
        {
            return MFALSE;
        }
        fwrite(reinterpret_cast<void *>(m_confidence_level), 1, 16*12*2, fp);
        fclose(fp);
    }

    return MTRUE;


}


MBOOL PD_IMX230MIPIRAW::ExtractCaliData()
{
    ALOGD("[%s] Sz=%d", __FUNCTION__, cSize);

    for( int i=0; i<cSize; i++)
    {
        m_calibration_data[i] = 0xffff & ( m_calidatabuf[i*2]<<8 | m_calidatabuf[i*2+1]);

    }

    // Slope and offset (defocus vs phase difference)
    // Set slope
    for( unsigned short i = 0; i < m_XKnotNum1*m_YKnotNum1; i++ )
    {
        m_SonyPdLibInputData.p_SlopeData[i] = m_calibration_data[i];
        ALOGD("Slope Data[%d] : %6d\n", i,(int)m_SonyPdLibInputData.p_SlopeData[i]);
    }

    // Set offset
    for( unsigned short i = 0; i < m_XKnotNum1*m_YKnotNum1; i++ )
    {
        m_SonyPdLibInputData.p_OffsetData[i] = 0;
        ALOGD("Offset Data[%d] : %6d\n", i,(int)m_SonyPdLibInputData.p_OffsetData[i]);
    }


    char value[200] = {'\0'};
    property_get("vc.dump.enable", value, "0");
    MBOOL bEnable = atoi(value);
    if (bEnable)
    {
        char fileName[64];
        sprintf(fileName, "/sdcard/vc/0_cali.raw");
        FILE *fp = fopen(fileName, "w");
        if (NULL == fp)
        {
            return MFALSE;
        }
        fwrite(reinterpret_cast<void *>(m_calibration_data), 1, 8*6*2, fp);
        fclose(fp);
    }

    return MTRUE;


}



MRESULT PD_IMX230MIPIRAW::GetVersionOfPdafLibrary( SPDLibVersion_t &tOutSWVer)
{
    // Input
    SonyPdLibVersion_t SonyPdLibVersion;

    // Execute
    SonyPdLibGetVersion(&SonyPdLibVersion);

    tOutSWVer.MajorVersion = SonyPdLibVersion.MajorVersion;
    tOutSWVer.MinorVersion = SonyPdLibVersion.MinorVersion;

    // Output
    //ALOGD_IF( m_bDebugEnable, "[SONY]%s = %d.%d\n", __FUNCTION__, (int)tOutSWVer.MajorVersion,(int)tOutSWVer.MinorVersion);
    ALOGD( "[SONY]%s = %d.%d\n", __FUNCTION__, (int)tOutSWVer.MajorVersion,(int)tOutSWVer.MinorVersion);

    if( tOutSWVer.MajorVersion==0 && tOutSWVer.MinorVersion==0)
    {
        ALOGD("[WAR] [%s] Please lincess IMX230 pdaf library from Sony FAE!!!\n", __FUNCTION__);
    }

    return MTRUE;
}



MBOOL PD_IMX230MIPIRAW::TransROICoord( SPDROI_T &srcROI, SPDROI_T &dstROI)
{
	MBOOL ret = MFALSE;
	int temp = 0, center_x,center_y,imx230_AFwin_NUM;
	int imx230_AF_window_X[4],imx230_AFwinnum_X;
	int imx230_AF_window_Y[5],imx230_AFwinnum_Y;
	
    // declare variables
    SonyPdLibRect_t               InputWindowAddress;				// window address on image sensor output image (input)
    SonyPdLibRect_t               OutputWindowAddress;				// window address for PDAF Library             (output)
	ALOGD("[Sony] PD window Xstart=%d, Xend=%d)\n", srcROI.i4XStart, srcROI.i4XEnd);
	ALOGD("[Sony] PD window Ystart=%d, Yend=%d)\n", srcROI.i4YStart, srcROI.i4YEnd);	
	
	//if( srcROI.i4XStart < srcROI.i4XEnd)
	//{
	//	temp = srcROI.i4XStart;
	//	srcROI.i4XStart	= srcROI.i4XEnd;
	//	srcROI.i4XEnd = temp;
	//}
	//if( srcROI.i4YStart < srcROI.i4YEnd)
	//{
	//	temp = srcROI.i4YStart;
	//	srcROI.i4YStart	= srcROI.i4YEnd;
	//	srcROI.i4YEnd = temp;
	//}
	center_x = (srcROI.i4XStart + srcROI.i4XEnd)/2;
	center_y = (srcROI.i4YStart + srcROI.i4YEnd)/2;

	switch( iPdProfile_temp.uImgYsz)
	{
		case 4016 : // 4:3 full mode
			imx230_AF_window_X[0]=112;imx230_AF_window_X[1]=1684;imx230_AF_window_X[2]=3660;imx230_AF_window_X[3]=5232;
			imx230_AF_window_Y[0]=88;imx230_AF_window_Y[1]=1246;imx230_AF_window_Y[2]=2008;imx230_AF_window_Y[3]=2770;imx230_AF_window_Y[4]=3928;
			break;
		case 2008 : // 4:3 2bin mode
			imx230_AF_window_X[0]=56;imx230_AF_window_X[1]=842;imx230_AF_window_X[2]=1830;imx230_AF_window_X[3]=2616;
			imx230_AF_window_Y[0]=44;imx230_AF_window_Y[1]=624;imx230_AF_window_Y[2]=1004;imx230_AF_window_Y[3]=1386;imx230_AF_window_Y[4]=1964;
			break;
/*
		case 3008 : // 16:9 full mode
			imx230_AF_window_X[0]=0;imx230_AF_window_X[1]=1684;imx230_AF_window_X[2]=3660;imx230_AF_window_X[4]=5344;
			imx230_AF_window_Y[0]=0;imx230_AF_window_Y[1]=2008;imx230_AF_window_Y[2]=3008;
			break;
		case 1504 : // 16:9 2bin mode
			imx230_AF_window_X[0]=0;imx230_AF_window_X[1]=842;imx230_AF_window_X[2]=1830;imx230_AF_window_X[4]=2672;
			imx230_AF_window_Y[0]=0;imx230_AF_window_Y[1]=1004;imx230_AF_window_Y[2]=1504;
			break;
		case 1080 : // 1080 mode 
			imx230_AF_window_X[0]=0;imx230_AF_window_X[1]=842;imx230_AF_window_X[2]=1830;imx230_AF_window_X[4]=2672;
			imx230_AF_window_Y[0]=0;imx230_AF_window_Y[1]=1004;imx230_AF_window_Y[2]=1504;
			break;
		case 2160 : // 4K mode 
			imx230_AF_window_X[0]=0;imx230_AF_window_X[1]=1684;imx230_AF_window_X[2]=3660;imx230_AF_window_X[4]=5344;
			imx230_AF_window_Y[0]=0;imx230_AF_window_Y[1]=2008;imx230_AF_window_Y[2]=4016;
			break;
*/
		default : 
			ALOGD("[Sony]PD mode is not confirmed\n");
			break;		
	}		
	
	// if parameter data can be extracted successfully
	if( m_IsCfgCoordSetting == D_SONY_PD_LIB_REGDATA_IS_OK)
	{
		// input window address on image sensor output image
		if(center_x <= imx230_AF_window_X[1]) 
		{
			if(center_y <= imx230_AF_window_Y[2]) 
			{
				imx230_AFwin_NUM = 1;
				InputWindowAddress.sta.x = imx230_AF_window_X[0];
				InputWindowAddress.end.x = imx230_AF_window_X[1];
				InputWindowAddress.sta.y = imx230_AF_window_Y[0];
				InputWindowAddress.end.y = imx230_AF_window_Y[2];
			}else
			{
				imx230_AFwin_NUM = 4;
				InputWindowAddress.sta.x = imx230_AF_window_X[0];
				InputWindowAddress.end.x = imx230_AF_window_X[1];
				InputWindowAddress.sta.y = imx230_AF_window_Y[2];
				InputWindowAddress.end.y = imx230_AF_window_Y[4];
			}	
		}else if(center_x <= imx230_AF_window_X[2]) 
		{
			if(center_y <= imx230_AF_window_Y[1]) 
			{
				imx230_AFwin_NUM = 2;
				InputWindowAddress.sta.x = imx230_AF_window_X[1];
				InputWindowAddress.end.x = imx230_AF_window_X[2];
				InputWindowAddress.sta.y = imx230_AF_window_Y[0];
				InputWindowAddress.end.y = imx230_AF_window_Y[1];
			}else if (center_y <= imx230_AF_window_Y[3])
			{
				imx230_AFwin_NUM = 0;
				InputWindowAddress.sta.x = imx230_AF_window_X[1];
				InputWindowAddress.end.x = imx230_AF_window_X[2];
				InputWindowAddress.sta.y = imx230_AF_window_Y[1];
				InputWindowAddress.end.y = imx230_AF_window_Y[3];
			}else
			{
				imx230_AFwin_NUM = 5;
				InputWindowAddress.sta.x = imx230_AF_window_X[1];
				InputWindowAddress.end.x = imx230_AF_window_X[2];
				InputWindowAddress.sta.y = imx230_AF_window_Y[3];
				InputWindowAddress.end.y = imx230_AF_window_Y[4];
			}
		}else
		{
			if(center_y <= imx230_AF_window_Y[2]) 
			{
				imx230_AFwin_NUM = 3;
				InputWindowAddress.sta.x = imx230_AF_window_X[2];
				InputWindowAddress.end.x = imx230_AF_window_X[3];
				InputWindowAddress.sta.y = imx230_AF_window_Y[0];
				InputWindowAddress.end.y = imx230_AF_window_Y[2];
			}else
			{
				imx230_AFwin_NUM = 6;
				InputWindowAddress.sta.x = imx230_AF_window_X[2];
				InputWindowAddress.end.x = imx230_AF_window_X[3];
				InputWindowAddress.sta.y = imx230_AF_window_Y[2];
				InputWindowAddress.end.y = imx230_AF_window_Y[4];
			}	
		}
		m_SonyPdLibInputData.PhaseDifference = (signed long) (m_phase_difference[imx230_AFwin_NUM]>=512 ? m_phase_difference[imx230_AFwin_NUM]-1024 : m_phase_difference[imx230_AFwin_NUM]);
		m_SonyPdLibInputData.ConfidenceLevel = (signed long) (m_confidence_level[imx230_AFwin_NUM]);
		ALOGD("[Sony] TransROICoord::imx230_AFwin_NUM          =%d\n", imx230_AFwin_NUM);	
		
        ALOGD("[%s] Input : Win Start(%4d, %4d), Win End(%4d, %4d)\n",
               __FUNCTION__,
               InputWindowAddress.sta.x,
               InputWindowAddress.sta.y,
               InputWindowAddress.end.x,
               InputWindowAddress.end.y);


        // convert address for PDAF Library
        OutputWindowAddress = SonyPdLibTransOutputRectToPdafRect(
                                  //Input
                                  InputWindowAddress,					// window address on image sensor output image
                                  m_ParamDataForConvertingAddress );	// parameter data for converting address

		//output X coordinate
		dstROI.i4XStart = OutputWindowAddress.sta.x;
		dstROI.i4XEnd	= OutputWindowAddress.end.x;

		//output Y coordinate
		dstROI.i4YStart = OutputWindowAddress.sta.y;
		dstROI.i4YEnd	= OutputWindowAddress.end.y;

        // the converted address for PDAF Library
        ALOGD_IF( m_bDebugEnable, "D_SONY_PD_LIB_REGDATA_IS_OK\n");

        ret = MTRUE;
    }
    else
    {
        ret = MFALSE;
        ALOGD_IF( m_bDebugEnable, "D_SONY_PD_LIB_REGDATA_IS_NG\n");
    }

    ALOGD("[%s] Output : Win Start(%4d, %4d), Win End(%4d, %4d)\n\n",
           __FUNCTION__,
           dstROI.i4XStart,
           dstROI.i4YStart,
           dstROI.i4XEnd,
           dstROI.i4YEnd);
    return ret;


}


MBOOL PD_IMX230MIPIRAW::GetDefocus( SPDROIInput_T &iPDInputData, SPDROIResult_T &oPdOutputData)
{

    signed long    Ret = D_SONY_PD_LIB_E_NG;
    unsigned short i   = 0;
    unsigned long  k   = 0;

    unsigned short XSize = iPDInputData.XSizeOfImage;
    unsigned short YSize = iPDInputData.YSizeOfImage;

    // transform ROI coordinate for Mirror / Flip case.
    SPDROI_T convertedROI;
    TransROICoord( iPDInputData.ROI, convertedROI);

    // Input
    // Phase difference data and confidence level
	//m_SonyPdLibInputData.PhaseDifference = (signed long) (m_phase_difference[0]>=512 ? m_phase_difference[0]-1024 : m_phase_difference[0]);
	//m_SonyPdLibInputData.ConfidenceLevel = (signed long) (m_confidence_level[0]);
	
	//ALOGD("[Sony] **Phase difference data and confidence level sz=(%d,%d), idx=(%d,%d)\n", iPDInputData.XSizeOfImage, iPDInputData.YSizeOfImage, Xidx, Yidx);
			
	ALOGD("[Sony] [%s] PhaseDifference : %4d\n", __FUNCTION__, (int)m_SonyPdLibInputData.PhaseDifference);
	ALOGD("[Sony] [%s] ConfidenceLevel : %4d\n", __FUNCTION__, (int)m_SonyPdLibInputData.ConfidenceLevel);		
    // PDAF window
    // Address is required to be converted into all-pixel mode address
    // before scaling, cropping, mirroring and flipping

    // PDAF window information must be
    // in synchronization with phase difference data and confidence level

    // Set size
	m_SonyPdLibInputData.XSizeOfImage = 5344;
	m_SonyPdLibInputData.YSizeOfImage = 4016;
	ALOGD("[Sony] SonyPdLibInputData.XSizeOfImage          :%d(%d)\n", (int)m_SonyPdLibInputData.XSizeOfImage, iPDInputData.XSizeOfImage);
	ALOGD("[Sony] SonyPdLibInputData.YSizeOfImage          :%d(%d)\n", (int)m_SonyPdLibInputData.YSizeOfImage, iPDInputData.YSizeOfImage);
			
    // Set PDAF window
    m_SonyPdLibInputData.XAddressOfWindowStart	= convertedROI.i4XStart;
    m_SonyPdLibInputData.YAddressOfWindowStart	= convertedROI.i4YStart;
    m_SonyPdLibInputData.XAddressOfWindowEnd	= convertedROI.i4XEnd;
    m_SonyPdLibInputData.YAddressOfWindowEnd	= convertedROI.i4YEnd;
    ALOGD("[%s] WinAddr (%4d,%4d), (%4d,%4d)\n",
           __FUNCTION__,
           (int)m_SonyPdLibInputData.XAddressOfWindowStart,
           (int)m_SonyPdLibInputData.YAddressOfWindowStart,
           (int)m_SonyPdLibInputData.XAddressOfWindowEnd,
           (int)m_SonyPdLibInputData.YAddressOfWindowEnd);


    // Set the number of knots
    m_SonyPdLibInputData.XKnotNumSlopeOffset = m_XKnotNum1;
    m_SonyPdLibInputData.YKnotNumSlopeOffset = m_YKnotNum1;


    // Set x address of konts
    for( i = 0; i < m_XKnotNum1; i++ )
    {
        m_SonyPdLibInputData.p_XAddressKnotSlopeOffset[i] = 0 + i * XSize / (m_XKnotNum1-1);	// Value is as an example
    }

    // Set y address of konts
    for( i = 0; i < m_YKnotNum1; i++ )
    {
        m_SonyPdLibInputData.p_YAddressKnotSlopeOffset[i] = 0 + i * YSize / (m_YKnotNum1-1);	// Value is as an example
    }

    // Set adjustment coefficient of slope according to image sensor mode
    // Set phase detection pixel density aaccording to image sensor mode
    switch( m_CurrMode)
    {
		case 0 : // all pixel mode
			m_SonyPdLibInputData.DensityOfPhasePix = D_SONY_PD_LIB_DENSITY_SENS_MODE0;	
			m_SonyPdLibInputData.AdjCoeffSlope	   = D_SONY_PD_LIB_SLOPE_ADJ_COEFF_SENS_MODE0;
			break;
		case 2 : // 2bining mode	
			m_SonyPdLibInputData.DensityOfPhasePix = D_SONY_PD_LIB_DENSITY_SENS_MODE2;
			m_SonyPdLibInputData.AdjCoeffSlope     = D_SONY_PD_LIB_SLOPE_ADJ_COEFF_SENS_MODE2;
			break;
		case 4 : // 4bining mode	
			m_SonyPdLibInputData.DensityOfPhasePix = D_SONY_PD_LIB_DENSITY_SENS_MODE4;
			m_SonyPdLibInputData.AdjCoeffSlope     = D_SONY_PD_LIB_SLOPE_ADJ_COEFF_SENS_MODE4;
        break;
    default : // all pixel mode
        m_SonyPdLibInputData.DensityOfPhasePix = D_SONY_PD_LIB_DENSITY_SENS_MODE0;
        m_SonyPdLibInputData.AdjCoeffSlope	   = D_SONY_PD_LIB_SLOPE_ADJ_COEFF_SENS_MODE0;
        break;

    }
    ALOGD("[%s] AdjCoeffSlope     : %d\n", __FUNCTION__, (int)m_SonyPdLibInputData.AdjCoeffSlope);
    ALOGD("[%s] DensityOfPhasePix : %d\n", __FUNCTION__, (int)m_SonyPdLibInputData.DensityOfPhasePix);

    // Defocus OK/NG : not using
    // Set image sensor analog gain
    // which must be in synchronization with phase difference data and confidence level
    m_SonyPdLibInputData.ImagerAnalogGain = 10;			// Value is as an example //@Tim

    // Set the number of knots
	m_SonyPdLibInputData.XKnotNumDefocusOKNG = m_XKnotNum1;	// Value is as an example
	m_SonyPdLibInputData.YKnotNumDefocusOKNG = m_YKnotNum1;	// Value is as an example

    // Set the threshold line
	for( i = 0; i < m_XKnotNum1*m_YKnotNum1; i++ )
    {
        m_SonyPdLibInputData.p_DefocusOKNGThrLine[i].PointNum = m_PointNumForThrLine;

        for( k = 0; k < m_PointNumForThrLine; k++ )
        {
            m_SonyPdLibInputData.p_DefocusOKNGThrLine[i].p_AnalogGain[k] = 10 * k;	// Value is as an example
            m_SonyPdLibInputData.p_DefocusOKNGThrLine[i].p_Confidence[k] =  2 * k;	// Value is as an example
        }
    }

    // Set x address of konts
	for( i = 0; i < m_XKnotNum1; i++ )
	{
		m_SonyPdLibInputData.p_XAddressKnotDefocusOKNG[i] = 0 + i * XSize / (m_XKnotNum1-1);	// Value is as an example
    }

    // Set y address of konts
	for( i = 0; i < m_YKnotNum1; i++ )
	{
		m_SonyPdLibInputData.p_YAddressKnotDefocusOKNG[i] = 0 + i * YSize / (m_YKnotNum1-1);	// Value is as an example
    }


    // Execute
    // Get defocus data
    Ret = SonyPdLibGetDefocus(&m_SonyPdLibInputData, &m_SonyPdLibOutputData);

    // Output
    if( Ret == D_SONY_PD_LIB_E_OK )
    {


        //(1) normalize to fit API spec : output target position.
        oPdOutputData.Defocus                = iPDInputData.curLensPos + ((-1)*m_SonyPdLibOutputData.Defocus/16384);
        //(2) normalize to fit API spec : confidence level is in ragne 0~100.
        oPdOutputData.DefocusConfidence      = (int)(m_SonyPdLibInputData.ConfidenceLevel*100/255);
        //(1) normalize to fit API spec : confidence level is in ragne 0~100.
        oPdOutputData.DefocusConfidenceLevel = (int)(m_SonyPdLibInputData.ConfidenceLevel*100/255);
        //(1) normalize to fit API spec : pixel base.
        oPdOutputData.PhaseDifference        = (-1)*m_SonyPdLibInputData.PhaseDifference*1000/16;

        ALOGD_IF( m_bDebugEnable,"[%s] Output:Reurn Value == D_SONY_PD_LIB_E_OK\n", __FUNCTION__);
        ALOGD("[%s] ---------------------------------\n", __FUNCTION__);
        // Defocus data
        ALOGD("[%s] CurrLensPos           : %4d\n", __FUNCTION__, (int)iPDInputData.curLensPos);
        ALOGD("[%s] Defocus               : %4d\n", __FUNCTION__, (int)m_SonyPdLibOutputData.Defocus);
        ALOGD("[%s] DefocusConfidence     : %4d\n", __FUNCTION__, (int)m_SonyPdLibOutputData.DefocusConfidence);
        ALOGD("[%s] DefocusConfidenceLevel: %4d\n", __FUNCTION__, (int)m_SonyPdLibOutputData.DefocusConfidenceLevel);
        ALOGD("[%s] PhaseDifference       : %4d\n", __FUNCTION__, (int)m_SonyPdLibOutputData.PhaseDifference);
        ALOGD("[%s][Output] Target %4d DAC, CL %4d, PD %4d\n",
               __FUNCTION__,
               oPdOutputData.Defocus,
               oPdOutputData.DefocusConfidenceLevel,
               oPdOutputData.PhaseDifference);
    }
    else
    {
        oPdOutputData.Defocus                = 0;
        oPdOutputData.DefocusConfidence      = 0;
        oPdOutputData.DefocusConfidenceLevel = 0;
        oPdOutputData.PhaseDifference        = 0;
        ALOGD("[%s] Output:Reurn Value == D_SONY_PD_LIB_E_NG\n", __FUNCTION__);
    }



    ALOGD("[%s] ---------------------------------\n", __FUNCTION__);

    return true;
}
