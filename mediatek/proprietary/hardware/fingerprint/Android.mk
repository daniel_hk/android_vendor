# Copyright (C) 2013 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ifeq ($(strip $(MTK_FINGERPRINT_SUPPORT)),yes)
ifeq ($(strip $(MTK_FINGERPRINT_SELECT)),GF318M)
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := libgf_hal
LOCAL_SRC_FILES_64 := goodix/debug/arm64-v8a/libgf_hal.so
LOCAL_SRC_FILES_32 := goodix/debug/armeabi-v7a/libgf_hal.so
LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libgf_ca
LOCAL_MULTILIB := both
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := fingerprint.default
LOCAL_MODULE_RELATIVE_PATH := hw

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/goodix/public

LOCAL_SRC_FILES := goodix/fingerprint.c

LOCAL_SHARED_LIBRARIES := \
    libgf_hal \
    liblog

LOCAL_MODULE_TAGS := optional
include $(BUILD_SHARED_LIBRARY)

endif
endif
