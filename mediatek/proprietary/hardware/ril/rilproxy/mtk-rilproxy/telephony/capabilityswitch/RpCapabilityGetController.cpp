/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 * Include
 *****************************************************************************/

#include <cutils/properties.h>
#include <ril.h>
#include <Errors.h>
#include "RpCapabilityGetController.h"
#include "RfxLog.h"
#include "RfxStatusManager.h"

#define RFX_LOG_TAG "RpCapabilityGetController"

/*****************************************************************************
 * Class RfxController
 *****************************************************************************/

RFX_IMPLEMENT_CLASS("RpCapabilityGetController", RpCapabilityGetController, RfxController);

RpCapabilityGetController::RpCapabilityGetController() {
}

RpCapabilityGetController::~RpCapabilityGetController() {
}

void RpCapabilityGetController::onInit() {
    RfxController::onInit(); // Required: invoke super class implementation
    RFX_LOG_D(RFX_LOG_TAG, "RpCapabilityGetController, onInit");
    const int urc_id_list[] = {
        RIL_UNSOL_RADIO_CAPABILITY
    };

    // register request & URC id list
    // NOTE. one id can only be registered by one controller
    registerToHandleUrc(0, urc_id_list, (sizeof(urc_id_list)/sizeof(int)));
    registerToHandleUrc(1, urc_id_list, (sizeof(urc_id_list)/sizeof(int)));
    //registerToHandleUrc(urc_id_list, (sizeof(urc_id_list)/sizeof(int)));
}

bool RpCapabilityGetController::onHandleUrc(const sp<RfxMessage>& message) {
    RFX_LOG_D(RFX_LOG_TAG, "onHandleUrc, handle: %s", requestToString(message->getId()));
    int msg_id = message->getId();
    switch (msg_id) {
    case RIL_UNSOL_RADIO_CAPABILITY: {
            RFX_LOG_D(RFX_LOG_TAG, "onHandleUrc,slotId= %d",message->getSlotId());
            int modemOffStateUrc = getNonSlotScopeStatusManager()->getIntValue(
                    RFX_STATUS_KEY_MODEM_OFF_STATE, MODEM_OFF_IN_IDLE);
            if (modemOffStateUrc == MODEM_OFF_BY_SIM_SWITCH) {
                RFX_LOG_D(RFX_LOG_TAG, "Reset modemOffState on URC stag");
                getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_MODEM_OFF_STATE,
                                      MODEM_OFF_IN_IDLE);
            }
            getStatusManager(message->getSlotId())->setIntValue(RFX_STATUS_KEY_SLOT_CAPABILITY,getRadioAcessFamily(message));
            responseToRilj(message);
        }
        break;
    default:
        break;
    }
    return true;
}

int RpCapabilityGetController::getRadioAcessFamily(const sp<RfxMessage>& message) {
    int status;
    int request;
    int token;
    RIL_RadioCapability rc;
    int t;
    Parcel* p = message->getParcel();

    memset (&rc, 0, sizeof(RIL_RadioCapability));

    status = p->readInt32(&t);
    rc.version = (int)t;
    //if (status != NO_ERROR) {
    //    goto invalid;
    //}

    status = p->readInt32(&t);
    rc.session= (int)t;
    //if (status != NO_ERROR) {
    //    goto invalid;
    //}

    status = p->readInt32(&t);
    rc.phase= (int)t;
    //if (status != NO_ERROR) {
    //    goto invalid;
    //}

    status = p->readInt32(&t);
    rc.rat = (int)t;
    //if (status != NO_ERROR) {
    //    goto invalid;
    //}

    //status = readStringFromParcelInplace(p, rc.logicalModemUuid, sizeof(rc.logicalModemUuid));
    //if (status != NO_ERROR) {
    //    goto invalid;
    //}

    //status = p->readInt32(&t);
    //rc.status = (int)t;

    //if (status != NO_ERROR) {
    //    goto invalid;
    //}
    RFX_LOG_D(RFX_LOG_TAG, "getRadioAcessFamily,rc.rat = %d",rc.rat);
    return rc.rat;

}

char* RpCapabilityGetController::requestToString(int reqId) {
    switch (reqId) {
        case RIL_REQUEST_SET_RADIO_CAPABILITY:
            return "SET_RADIO_CAPABILITY";
        default:
            RFX_LOG_E(RFX_LOG_TAG, "requestToString, reqId: %d", reqId);
            return "UNKNOWN_REQUEST";
    }
}

char* RpCapabilityGetController::urcToString(int urcId) {
    switch (urcId) {
        default:
            RFX_LOG_E(RFX_LOG_TAG, "requestToString, urcId: %d", urcId);
            return "UNKNOWN_URC";
    }
}

