/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 * Include
 *****************************************************************************/
#include <cutils/properties.h>
#include "RpNwRatController.h"
#include "RfxLog.h"
#include "RfxStatusDefs.h"
#include "RpNwController.h"
#include "RpGsmNwRatSwitchHandler.h"
#include "RpCdmaNwRatSwitchHandler.h"
#include "RpCdmaLteNwRatSwitchHandler.h"
#include "modecontroller/RpCdmaLteDefs.h"
#include "modecontroller/RpCdmaLteModeController.h"
#include "ril.h"

#define RAT_CTRL_TAG "RpNwRatController"

/*****************************************************************************
 * Class RpNwRatController
 *****************************************************************************/

RFX_IMPLEMENT_CLASS("RpNwRatController", RpNwRatController, RfxController);

bool RpNwRatController::sIsInSwitching = false;

RpNwRatController::RpNwRatController() :
    mCurAppFamilyType(APP_FAM_UNKNOWN),
    mCurPreferedNetWorkType(-1),
    mCurNwsMode(NWS_MODE_CSFB),
    mCapabilitySlotId(0),
    mNewAppFamilyType(APP_FAM_UNKNOWN),
    mNewPreferedNetWorkType(-1),
    mNewNwsMode(NWS_MODE_CSFB),
    mNwRatSwitchHandler(NULL),
    mPreferredNetWorkTypeFromRILJ(-1) {
}

RpNwRatController::~RpNwRatController() {
    RFX_OBJ_CLOSE(mNwRatSwitchHandler);
}

void RpNwRatController::setPreferredNetworkType(const int prefNwType,
        const sp<RfxAction>& action) {
    if (getEnginenerMode() == ENGINEER_MODE_AUTO) {
        if (prefNwType == -1) {
            logD(RAT_CTRL_TAG, "setPreferredNetworkType() leaving restricted mode");
            mPendingRestrictedRatSwitchRecord.prefNwType = -1;
            doPendingRatSwitchRecord();
        } else {
            logD(RAT_CTRL_TAG, "setPreferredNetworkType() entering restricted mode: %d", prefNwType);
            mPendingRestrictedRatSwitchRecord.prefNwType = prefNwType;
            mPendingRestrictedRatSwitchRecord.appFamType = APP_FAM_3GPP2;
            mPendingRestrictedRatSwitchRecord.ratSwitchCaller = RAT_SWITCH_RESTRICT;
            mPendingRestrictedRatSwitchRecord.nwsMode = mCurNwsMode;
            if (mPendingInitRatSwitchRecord.prefNwType == -1
                    && mPendingNormalRatSwitchRecord.prefNwType == -1) {
                // save current state to pending queue
                queueRatSwitchRecord(mCurAppFamilyType, mCurPreferedNetWorkType, mCurNwsMode,
                        RAT_SWITCH_INIT, action, NULL);
            }
            switchNwRat(APP_FAM_3GPP2, PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA, mCurNwsMode,
                    RAT_SWITCH_RESTRICT, action, NULL);
        }
    }
}

void RpNwRatController::setPreferredNetworkType(const AppFamilyType appFamType,
        const int prefNwType, int type, const sp<RfxAction>& action) {
    logD(RAT_CTRL_TAG, "setPreferredNetworkType() from mode controller, appFamType is %d, prefNwType is %d, type is %d",
            appFamType, prefNwType, type);
    if (getChipTestMode() == 1) {
        logD(RAT_CTRL_TAG, "ChipTest! setPreferredNetworkType not executed! action->act()");
        if (action != NULL) {
            action->act();
        }
        return;
    } else {
        int capabilitySlotId = getCapabilitySlotId();
        if (type == CARD_STATE_NO_CHANGED && appFamType == mNewAppFamilyType
                && mCapabilitySlotId == capabilitySlotId) {
            if (action != NULL) {
                action->act();
            }
            return;
        }
        if (type == CARD_STATE_NOT_HOT_PLUG || type == CARD_STATE_HOT_PLUGIN) {
            clearInvalidPendingRecords();
        }
        mCapabilitySlotId = capabilitySlotId;
    }

    int oldAppFamilyType = mNewAppFamilyType;
    mNewAppFamilyType = appFamType;

    logD(RAT_CTRL_TAG, "setPreferredNetworkType() prefNwType is %d, mPreferredNetWorkTypeFromRILJ is %d",
            prefNwType, mPreferredNetWorkTypeFromRILJ);
    int newPrefNwType = prefNwType;
    if (mPreferredNetWorkTypeFromRILJ != -1
            && mPreferredNetWorkTypeFromRILJ != PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA) {
        if (appFamType == APP_FAM_3GPP2) {
             newPrefNwType = filterPrefNwType(appFamType,
                     mPreferredNetWorkTypeFromRILJ, prefNwType);
         } else {
             newPrefNwType = mPreferredNetWorkTypeFromRILJ;
         }
         logD(RAT_CTRL_TAG, "mPreferredNetWorkTypeFromRILJ filtered as %d.", newPrefNwType);
    }

    if (appFamType == APP_FAM_3GPP) {
        /* For gsm card, create the ratSwitchHandler and set the related state*/
        switchNwRat(appFamType, newPrefNwType, NWS_MODE_CSFB, RAT_SWITCH_INIT,
                    action, NULL);
    } else {
        if (getEnginenerMode() == ENGINEER_MODE_AUTO) {
            if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
                switchNwRat(appFamType, newPrefNwType,
                        mPendingNormalRatSwitchRecord.nwsMode, RAT_SWITCH_INIT,
                        action, NULL);
            } else {
                /* For ct 3g dual mode card Home and roaming switch */
                NwsMode newNwsMode = NWS_MODE_CDMALTE;
                if(type == CARD_STATE_CARD_TYPE_CHANGED && oldAppFamilyType == appFamType) {
                    newNwsMode = mNewNwsMode;
                }

                switchNwRat(appFamType, newPrefNwType, newNwsMode,
                        RAT_SWITCH_INIT, action, NULL);
            }
        } else {
            doNwSwitchForEngMode(action);
        }
    }
}

void RpNwRatController::setPreferredNetworkType(const int prefNwType,
        const sp<RfxMessage>& message) {
    logD(RAT_CTRL_TAG, "setPreferredNetworkType() from normal, prefNwType is %d,mNewPreferedNetWorkType is %d. ",
            prefNwType, mNewPreferedNetWorkType);
    if (getChipTestMode() == 1) {
        logD(RAT_CTRL_TAG, "ChipTest! setPreferredNetworkType not executed!");
        sp<RfxMessage> resToRilj = RfxMessage::obtainResponse(
                RIL_E_GENERIC_FAILURE, message);
        responseToRilj(resToRilj);
        return;
    }
    AppFamilyType appFamilyType = mNewAppFamilyType;

    if (appFamilyType == APP_FAM_3GPP) {
        /* For gsm card, create the ratSwitchHandler and set the related state*/
        switchNwRat(appFamilyType, prefNwType, NWS_MODE_CSFB, RAT_SWITCH_NORMAL,
                    NULL, message);
    } else if (appFamilyType == APP_FAM_3GPP2) {
        RpCdmaLteModeController *modeController = (RpCdmaLteModeController *) findController(-1,
                    RFX_OBJ_CLASS_INFO(RpCdmaLteModeController));
        int capability = modeController->getDefaultNetworkType(getSlotId());
        int targetPrefNwType = filterPrefNwType(appFamilyType, prefNwType, capability);
        logD(RAT_CTRL_TAG, "the prefer network type is filtered as %d, capability is %d .",
                      targetPrefNwType, capability);

        int engineerMode = getEnginenerMode();
        if ((engineerMode == ENGINEER_MODE_AUTO)
                || (engineerMode == ENGINEER_MODE_CDMA &&
                        (targetPrefNwType == PREF_NET_TYPE_CDMA_ONLY
                        || targetPrefNwType == PREF_NET_TYPE_EVDO_ONLY
                        || targetPrefNwType == PREF_NET_TYPE_CDMA_EVDO_AUTO))) {
            NwsMode nwsMode = mNewNwsMode;

            if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
                nwsMode = mPendingNormalRatSwitchRecord.nwsMode;
                logD(RAT_CTRL_TAG, "Has pending normal: update nwsMode from %d to %d.",
                        mNewNwsMode, nwsMode);
            } else if (mPendingInitRatSwitchRecord.prefNwType != -1) {
                nwsMode = mPendingInitRatSwitchRecord.nwsMode;
                logD(RAT_CTRL_TAG, "Has pending init: update nwsMode from %d to %d.",
                        mNewNwsMode, nwsMode);
            }

            switchNwRat(appFamilyType, targetPrefNwType, nwsMode, RAT_SWITCH_NORMAL,
                    NULL, message);
        } else {
            logD(RAT_CTRL_TAG, "setPreferredNetworkType: return directly with nothing to do.");
            sp<RfxMessage> resToRilj = RfxMessage::obtainResponse(
                    RIL_E_SUCCESS, message);
            responseToRilj(resToRilj);
        }
    } else {
        logD(RAT_CTRL_TAG, "setPreferredNetworkType APP_FAM_UNKNOWN!");
        sp<RfxMessage> resToRilj = RfxMessage::obtainResponse(
                RIL_E_GENERIC_FAILURE, message);
        responseToRilj(resToRilj);
    }
}

int RpNwRatController::filterPrefNwType(const AppFamilyType appFamilyType,
        int prefNwType, int capability) {
    int targetPrefNwType = prefNwType;
    switch (prefNwType) {
    case PREF_NET_TYPE_LTE_CDMA_EVDO:
    case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
        if (appFamilyType == APP_FAM_3GPP2) {
            if (capability == PREF_NET_TYPE_CDMA_EVDO_AUTO) {
                targetPrefNwType = PREF_NET_TYPE_CDMA_EVDO_AUTO;
            }
            if (capability == PREF_NET_TYPE_LTE_CDMA_EVDO) {
                targetPrefNwType = PREF_NET_TYPE_LTE_CDMA_EVDO;
            }
        } else if (appFamilyType == APP_FAM_3GPP) {
            if (capability == PREF_NET_TYPE_LTE_GSM_WCDMA) {
                targetPrefNwType = PREF_NET_TYPE_LTE_GSM_WCDMA;
            }
            if (capability == PREF_NET_TYPE_GSM_WCDMA) {
                targetPrefNwType = PREF_NET_TYPE_GSM_WCDMA;
            }
            if (capability == PREF_NET_TYPE_GSM_ONLY) {
                targetPrefNwType = PREF_NET_TYPE_GSM_ONLY;
            }
        }
        break;
    case PREF_NET_TYPE_CDMA_EVDO_AUTO:
    case PREF_NET_TYPE_CDMA_ONLY:
    case PREF_NET_TYPE_EVDO_ONLY:
    case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
    case PREF_NET_TYPE_GSM_WCDMA:
    case PREF_NET_TYPE_GSM_ONLY:
    case PREF_NET_TYPE_WCDMA:
    case PREF_NET_TYPE_GSM_WCDMA_AUTO:
    case PREF_NET_TYPE_LTE_ONLY:
    case PREF_NET_TYPE_LTE_TDD_ONLY:
        break;
    case PREF_NET_TYPE_LTE_GSM_WCDMA:
    case PREF_NET_TYPE_LTE_WCDMA:
    case PREF_NET_TYPE_LTE_GSM:
        /* the cdma card is in 3g mode, filter the 4g mode to 3g mode.*/
        if (appFamilyType == APP_FAM_3GPP2) {
            if (capability == PREF_NET_TYPE_CDMA_EVDO_AUTO) {
                targetPrefNwType = PREF_NET_TYPE_GSM_WCDMA;
            }
        }
        break;
    }
    return targetPrefNwType;
}

void RpNwRatController::creatRatSwitchHandlerIfNeeded(const AppFamilyType appFamType){
    logD(RAT_CTRL_TAG,"creatRatSwitchHandlerIfNeeded(), appFamType is %d. ",appFamType);
    if (appFamType == APP_FAM_3GPP) {
        // boot up work flow for gsm card.
        if (mNwRatSwitchHandler == NULL) {
            RFX_OBJ_CREATE_EX(mNwRatSwitchHandler, RpGsmNwRatSwitchHandler, this, (this));
            // for sim card hot plug in/out
        } else if (appFamType != mCurAppFamilyType) {
            if (!isRestrictedModeSupport()) {
                clearInvalidPendingRecords();
            }
            RFX_OBJ_CLOSE(mNwRatSwitchHandler);
            RFX_OBJ_CREATE_EX(mNwRatSwitchHandler, RpGsmNwRatSwitchHandler, this, (this));
        }
    } else if (appFamType == APP_FAM_3GPP2) {
        // boot up work flow for cdma card.
        if (mNwRatSwitchHandler == NULL) {
            RFX_OBJ_CREATE_EX(mNwRatSwitchHandler, RpCdmaLteNwRatSwitchHandler, this, (this));
            // for sim card hot plug in/out
        } else if (appFamType != mCurAppFamilyType) {
            if (!isRestrictedModeSupport()) {
                clearInvalidPendingRecords();
            }
            RFX_OBJ_CLOSE(mNwRatSwitchHandler);
            RFX_OBJ_CREATE_EX(mNwRatSwitchHandler, RpCdmaLteNwRatSwitchHandler, this, (this));
        }
    }
    logD(RAT_CTRL_TAG, "creatRatSwitchHandlerIfNeeded(), mNwRatSwitchHandler is %p. ",
            mNwRatSwitchHandler);
}

AppFamilyType RpNwRatController::getAppFamilyType() {
    logD(RAT_CTRL_TAG,"getAppFamilyType(), mCurAppFamilyType is %d. ",mCurAppFamilyType);
    return mCurAppFamilyType;
}

int RpNwRatController::getPreferredNetworkType() {
    logD(RAT_CTRL_TAG, "getPreferredNetworkType(), mCurPreferedNetWorkType is %d.",
            mCurPreferedNetWorkType);
    return mCurPreferedNetWorkType;
}

void RpNwRatController::setNwsMode(const NwsMode nwsMode, const sp<RfxAction>& action) {
    logD(RAT_CTRL_TAG, "setNwsMode(), nwsMode is %d, mNewNwsMode is %d.", nwsMode, mNewNwsMode);
    if (getChipTestMode() == 1) {
        logD(RAT_CTRL_TAG, "ChipTest! setNwsMode not executed! action->act()");
        if (action != NULL) {
            action->act();
        }
        return;
    }
    if (getEnginenerMode() == ENGINEER_MODE_AUTO) {
        int prefNwType = mNewPreferedNetWorkType;
        AppFamilyType appFamilyType = mNewAppFamilyType;

        if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
            prefNwType = mPendingNormalRatSwitchRecord.prefNwType;
        }

        if (appFamilyType == APP_FAM_3GPP2 && nwsMode != mNewNwsMode) {
            switchNwRat(appFamilyType, prefNwType, nwsMode, RAT_SWITCH_NWS,
                    action, NULL);
        }
    }
}

NwsMode RpNwRatController::getNwsMode() {
    logD(RAT_CTRL_TAG,"getNwsMode(), mCurNwsMode is %d. ",mCurNwsMode);
    return mCurNwsMode;
}

void RpNwRatController::onInit() {
    // Required: invoke super class implementation
    RfxController::onInit();

    logD(RAT_CTRL_TAG,"onInit");

    // define and register request & urc id list
    const int request_id_list[] = {
        RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE,
        RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE,
        RIL_REQUEST_CONFIG_EVDO_MODE,
        RIL_REQUEST_VOICE_RADIO_TECH,
        RIL_REQUEST_SET_SVLTE_RAT_MODE};
    const int atci_request_id_list[] = {
        RIL_LOCAL_REQUEST_OEM_HOOK_ATCI_INTERNAL};
    const int urc_id_list[] = { };
    registerToHandleRequest(request_id_list, sizeof(request_id_list)/sizeof(int));
    if (getChipTestMode() != 0) {
        registerToHandleRequest(atci_request_id_list,
                sizeof(atci_request_id_list)/sizeof(int), HIGHEST);
    }
    registerToHandleUrc(urc_id_list, 0);

    getStatusManager()->setIntValue(RFX_STATUS_KEY_NWS_MODE, mCurNwsMode);
    getStatusManager()->setIntValue(RFX_STATUS_KEY_PREFERRED_NW_TYPE, mCurPreferedNetWorkType);
}

void RpNwRatController::onDeinit() {
    RfxController::onDeinit();
}

void RpNwRatController::doNwSwitchForEngMode(const sp<RfxAction>& action) {
    logD(RAT_CTRL_TAG, "Radio Avaliable, have get the app family type ");
    if (mNewAppFamilyType == APP_FAM_3GPP2) {
        switch (getEnginenerMode()) {
        case ENGINEER_MODE_CDMA:
            logD(RAT_CTRL_TAG, "Radio Avaliable, CDMA only mode. ");
            switchNwRat(APP_FAM_3GPP2, PREF_NET_TYPE_CDMA_EVDO_AUTO,
                    NWS_MODE_CDMALTE, RAT_SWITCH_INIT, action, NULL);
            break;
        case ENGINEER_MODE_CSFB:
            logD(RAT_CTRL_TAG, "Radio Avaliable, CSFB only mode. ");
            switchNwRat(APP_FAM_3GPP2, PREF_NET_TYPE_LTE_GSM_WCDMA,
                    NWS_MODE_CSFB, RAT_SWITCH_INIT, action, NULL);
            break;
        case ENGINEER_MODE_LTE:
            logD(RAT_CTRL_TAG, "Radio Avaliable, LTE only mode. ");
            switchNwRat(APP_FAM_3GPP2, PREF_NET_TYPE_LTE_ONLY, NWS_MODE_CSFB,
                    RAT_SWITCH_INIT, action, NULL);
            break;
        default:
            logD(RAT_CTRL_TAG, "Radio Avaliable, auto mode, do nothing. ");
            break;
        }
    }
}

bool RpNwRatController::onHandleRequest(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    logD(RAT_CTRL_TAG,"handle req %s.", requestToString(msg_id));

    //  This is only for restart, setting is earlier than mode controller.
    if (msg_id == RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE
        && mNwRatSwitchHandler == NULL) {
        int32_t stgCount;
        int32_t nwType;
        message->getParcel()->readInt32(&stgCount);
        message->getParcel()->readInt32(&nwType);
        mPreferredNetWorkTypeFromRILJ = nwType;
        logD(RAT_CTRL_TAG, " setting is earlier than mode controller,"
                " send failure response, nwType is %d.", nwType);
        sp<RfxMessage> resToRilj = RfxMessage::obtainResponse(
                RIL_E_GENERIC_FAILURE, message);
        responseToRilj(resToRilj);
    }

    if (mNwRatSwitchHandler != NULL) {
        switch (msg_id) {
        case RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE:
            int32_t stgCount;
            int32_t nwType;
            message->getParcel()->readInt32(&stgCount);
            message->getParcel()->readInt32(&nwType);
            mPreferredNetWorkTypeFromRILJ = nwType;
            setPreferredNetworkType(nwType, message);
            return true;
            
        case RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE:
            logD(RAT_CTRL_TAG, "[handleGetPreferredNwType] mPreferredNetworkTypeFromRILJ:%d",
                mPreferredNetWorkTypeFromRILJ);
            switch (mPreferredNetWorkTypeFromRILJ) {
                case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
                case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
                case PREF_NET_TYPE_LTE_CDMA_EVDO: {
                    sp<RfxMessage> resToRilj = RfxMessage::obtainResponse(RIL_E_SUCCESS, message);
                    resToRilj->getParcel()->writeInt32(1);
                    resToRilj->getParcel()->writeInt32(mPreferredNetWorkTypeFromRILJ);
                    responseToRilj(resToRilj);
                    return true;
                 }
                 default:
                     break;
            }
            mNwRatSwitchHandler->requestGetPreferredNetworkType(message);
            return true;

        case RIL_REQUEST_VOICE_RADIO_TECH:
            logD(RAT_CTRL_TAG, "request voice radio tech, mNewNwsMode = %d.", mNewNwsMode);
            if (mNewNwsMode == NWS_MODE_CSFB) {
                mNwRatSwitchHandler->requestGetVoiceRadioTech(message);
            } else {
                sp<RfxMessage> resToRilj = RfxMessage::obtainResponse(
                        RIL_E_SUCCESS, message);
                resToRilj->getParcel()->writeInt32(1);
                resToRilj->getParcel()->writeInt32(RADIO_TECH_1xRTT);
                logD(RAT_CTRL_TAG, "request voice radio tech, send response.voiceRadioTech = 6.");
                responseToRilj(resToRilj);
            }
            return true;
        }
    }
    return false;
}

bool RpNwRatController::onHandleAtciRequest(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    int targetSlotId = 0;
    char simNo[PROPERTY_VALUE_MAX] = {0};

    switch (msg_id) {
        case RIL_LOCAL_REQUEST_OEM_HOOK_ATCI_INTERNAL: {
            sp<RfxMessage> request
                    = RfxMessage::obtainRequest(RADIO_TECH_GROUP_GSM, msg_id, message, true);
            Parcel *p = request->getParcel();
            p->setDataPosition(0);
            int len;
            char *data;

            p->readInt32(&len);
            logD(RAT_CTRL_TAG, "len: %d", len);

            if (len > 0) {
                data = (char*) p->readInplace(len);
                logD(RAT_CTRL_TAG, "data: %s", data);
                if (strncmp(data, "AT+ERAT=", 8) == 0) {
                    int rat = -1;
                    property_get("persist.service.atci.sim", simNo, "0");
                    logD(RAT_CTRL_TAG, "[onHandleAtciRequest] simNo: %d.", simNo[0]);

                    if (simNo[0] == '0') {
                        targetSlotId = 0;
                    } else if (simNo[0] == '1') {
                        targetSlotId = 1;
                    } else {
                        logD(RAT_CTRL_TAG, "Not support slot: %d.", simNo[0]);
                        return true;
                    }

                    if (targetSlotId == m_slot_id) {
                        sscanf(data, "AT+ERAT=%d", &rat);
                        switch (rat) {
                            case 0:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_GSM_ONLY;
                                break;
                            case 1:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_WCDMA;
                                break;
                            case 2:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_GSM_WCDMA;
                                break;
                            case 3:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_LTE_ONLY;
                                break;
                            case 4:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_LTE_GSM;
                                break;
                            case 5:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_LTE_WCDMA;
                                break;
                            case 6:
                                mCurAppFamilyType = APP_FAM_3GPP;
                                mCurNwsMode = NWS_MODE_CSFB;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_LTE_GSM_WCDMA;
                                break;
                            case 7:
                                mCurAppFamilyType = APP_FAM_3GPP2;
                                mCurNwsMode = NWS_MODE_CDMALTE;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_CDMA_EVDO_AUTO;
                                break;
                            case 11:
                                mCurAppFamilyType = APP_FAM_3GPP2;
                                mCurNwsMode = NWS_MODE_CDMALTE;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_LTE_CDMA_EVDO;
                                break;
                            case 14:
                                mCurAppFamilyType = APP_FAM_3GPP2;
                                mCurPreferedNetWorkType = PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA;
                                break;
                            default:
                                break;
                        }
                    }
                }
            } else {
                logD(RAT_CTRL_TAG, "[onHandleAtciRequest] len=0");
                break;
            }
        }
        break;
        default:
        break;
    }
    return false;
}

bool RpNwRatController::onHandleResponse(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    logD(RAT_CTRL_TAG,"handle %s response.", requestToString(msg_id));

    if (mNwRatSwitchHandler != NULL) {
        switch (msg_id) {
        case RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE:
            if (mNewAppFamilyType == APP_FAM_3GPP) {
                mNwRatSwitchHandler->responseSetPreferredNetworkType(message);
            }
            return true;
        case RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE:
            mNwRatSwitchHandler->responseGetPreferredNetworkType(message);
            return true;
        case RIL_REQUEST_CONFIG_EVDO_MODE:
            mNwRatSwitchHandler->responseSetEvdoMode(message);
            return true;
        case RIL_REQUEST_SET_SVLTE_RAT_MODE: {
            if (mNwRatSwitchHandler->isCdma3gDualModeCard()) {
                logD(RAT_CTRL_TAG, "3gCdmaSim handle RIL_REQUEST_SET_SVLTE_RAT_MODE,"
                        " error is %d", message->getError());
                sp<RfxMessage> msg = sp < RfxMessage > (NULL);
                ResponseStatus responseStatus = preprocessResponse(message, msg,
                        RfxWaitResponseTimedOutCallback(mNwRatSwitchHandler,
                                &RpBaseNwRatSwitchHandler::onResponseTimeOut),
                        s2ns(10));
                if (message->getError() != RIL_E_SUCCESS
                        || responseStatus == RESPONSE_STATUS_HAVE_MATCHED) {
                    mNwRatSwitchHandler->responseSetRatMode(message);
                }
            } else {
                mNwRatSwitchHandler->responseSetRatMode(message);
            }
            return true;
        }
        case RIL_REQUEST_VOICE_RADIO_TECH:
            mNwRatSwitchHandler->responseGetVoiceRadioTech(message);
            return true;
        }
    }
    return false;
}

bool RpNwRatController::onHandleUrc(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    logD(RAT_CTRL_TAG,"handle urc %s.", urcToString(msg_id));

    switch (msg_id) {
    default:
        break;
    }
    return true;
}

char* RpNwRatController::requestToString(int reqId) {
    switch (reqId) {
    case RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE:
        return "RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE";
    case RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE:
        return "RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE";
    case RIL_REQUEST_CONFIG_EVDO_MODE:
        return "RIL_REQUEST_CONFIG_EVDO_MODE";
    case RIL_REQUEST_SET_SVLTE_RAT_MODE:
        return "RIL_REQUEST_SET_SVLTE_RAT_MODE";
    case RIL_REQUEST_VOICE_RADIO_TECH:
        return "RIL_REQUEST_VOICE_RADIO_TECH";
    case RIL_LOCAL_REQUEST_OEM_HOOK_ATCI_INTERNAL:
        return "RIL_LOCAL_REQUEST_OEM_HOOK_ATCI_INTERNAL";
    default:
        logD(RAT_CTRL_TAG,"<UNKNOWN_REQUEST>");
        break;
    }
    return "";
}

char* RpNwRatController::urcToString(int reqId) {
    switch (reqId) {
    default:
        logD(RAT_CTRL_TAG,"<UNKNOWN_URC>");
        break;
    }
    return "";
}

void RpNwRatController::registerRatSwitchCallback(IRpNwRatSwitchCallback* callback) {
    mRpNwRatSwitchListener = callback;
}

void RpNwRatController::unRegisterRatSwitchCallback(IRpNwRatSwitchCallback* callback) {
    mRpNwRatSwitchListener = NULL;
}

void RpNwRatController::onRatSwitchStart(const int prefNwType, const NwsMode newNwsMode){
    if (mRpNwRatSwitchListener != NULL) {
        mRpNwRatSwitchListener->onRatSwitchStart(mCurPreferedNetWorkType,
                prefNwType, mCurNwsMode, newNwsMode);
    }
}
void RpNwRatController::onRatSwitchDone(const int prefNwType){
    if (mRpNwRatSwitchListener != NULL) {
        mRpNwRatSwitchListener->onRatSwitchDone(mCurPreferedNetWorkType,
                prefNwType);
    }
}
void RpNwRatController::onEctModeChangeDone(const int prefNwType){
    if (mRpNwRatSwitchListener != NULL) {
        mRpNwRatSwitchListener->onEctModeChangeDone(mCurPreferedNetWorkType,
                prefNwType);
    }
}
void RpNwRatController::switchNwRat(const AppFamilyType appFamType,
        int prefNwType, NwsMode nwsMode, const RatSwitchCaller ratSwitchCaller,
        const sp<RfxAction>& action, const sp<RfxMessage>& message) {
    logD(RAT_CTRL_TAG, "switchNwRat(), appFamType is %d, prefNwType is %d, nwsMode is %d . "
            "ratSwitchCaller is %d, sIsInSwitching is %s. ",
            appFamType, prefNwType, nwsMode, ratSwitchCaller,
            sIsInSwitching ? "true" : "false");

    if (sIsInSwitching) {
        queueRatSwitchRecord(appFamType, prefNwType, nwsMode, ratSwitchCaller, action, message);
    } else {
        if (ratSwitchCaller != RAT_SWITCH_RESTRICT
                && mPendingRestrictedRatSwitchRecord.prefNwType != -1) {
            logD(RAT_CTRL_TAG, "switchNwRat(), in restricted mode!");
            queueRatSwitchRecord(appFamType, prefNwType, nwsMode, ratSwitchCaller, action, message);
            return;
        }
        sIsInSwitching = true;

        /* create the rat switch handler if need. */
        creatRatSwitchHandlerIfNeeded(appFamType);
        mCurAppFamilyType = appFamType;

        ModemSettings mdSettings = mNwRatSwitchHandler->calculateModemSettings(
                prefNwType, appFamType, nwsMode);

        if (appFamType == APP_FAM_3GPP2) {
            /* Calculate the target preferred network type. */
            int targetPrefNwType = calculateTargetPreferredNwType(appFamType, prefNwType, nwsMode);
            logD(RAT_CTRL_TAG, "switchNwRat(), targetPrefNwType is %d.", targetPrefNwType);

            /* Handle the network preferred network switch. */
            if (isValidPreferredNwType(appFamType, targetPrefNwType, nwsMode)) {
                mNewPreferedNetWorkType = targetPrefNwType;
                mNewNwsMode = nwsMode;
                mNwRatSwitchHandler->doNwRatSwitch(targetPrefNwType, nwsMode,
                        ratSwitchCaller, action, message);
            } else {
                logD(RAT_CTRL_TAG, "switchNwRat(), invalid prefNwType is %d.", prefNwType);
                if (ratSwitchCaller == RAT_SWITCH_NORMAL && message != NULL) {
                    mNwRatSwitchHandler->responseSetPreferredNetworkType(message);
                }

                // Reset state and callback action if the RAT is invalid.
                updateState(mCurNwsMode, mCurPreferedNetWorkType);
                if (action != NULL) {
                    action->act();
                }
            }
        } else if (appFamType == APP_FAM_3GPP) {
            if (mdSettings.prefNwType != -1) {
                logD(RAT_CTRL_TAG, "switchNwRat(), set radio capability as 1.");
                RpRadioController* radioController =
                        (RpRadioController *) findController(RFX_OBJ_CLASS_INFO(RpRadioController));
                RpSuggestRadioCapabilityCallback callback =
                        RpSuggestRadioCapabilityCallback(this,
                                &RpNwRatController::onSuggestRadioCapabilityResult);
                radioController->suggestedCapability(
                        getSuggestedRadioCapability(mdSettings), callback);
                mNewPreferedNetWorkType = mdSettings.prefNwType;
                mNewNwsMode = nwsMode;
                mNwRatSwitchHandler->doNwRatSwitch(mdSettings, ratSwitchCaller, action, message);
                mNwRatSwitchHandler->updatePhone(mdSettings);
                updateState(mNewNwsMode, mNewPreferedNetWorkType);
            } else {
                logD(RAT_CTRL_TAG, "switchNwRat(), invalid prefNwType is %d.", prefNwType);
                if (message != NULL) {
                    sp<RfxMessage> rlt = RfxMessage::obtainResponse(
                            RIL_E_GENERIC_FAILURE, message);
                    responseToRilj(rlt);
                }
                updateState(mCurNwsMode, mCurPreferedNetWorkType);
            }
            if (action != NULL) {
                action->act();
            }
            logD(RAT_CTRL_TAG, "switchNwRat(), rat switch done for 3gpp card.");
        }
    }
}

int RpNwRatController::getSuggestedRadioCapability(ModemSettings mdSettings) {
    int suggestedRadio = RIL_CAPABILITY_NONE;
    bool md1Radio = mdSettings.md1Radio;
    bool md3Radio = mdSettings.md3Radio;
    if (md1Radio && md3Radio) {
        suggestedRadio = RIL_CAPABILITY_CDMA_ON_LTE;
    } else if (md1Radio && !md3Radio) {
        suggestedRadio = RIL_CAPABILITY_GSM_ONLY;
    } else if (!md1Radio && md3Radio) {
        suggestedRadio = RIL_CAPABILITY_CDMA_ONLY;
    }
    logD(RAT_CTRL_TAG, "suggestedRadio:%d", suggestedRadio);
    return suggestedRadio;
}

void RpNwRatController::queueRatSwitchRecord(const AppFamilyType appFamType,
        int prefNwType, NwsMode nwsMode, const RatSwitchCaller ratSwitchCaller,
        const sp<RfxAction>& action, const sp<RfxMessage>& message) {
    /* Pending if in switching. */
    logD(RAT_CTRL_TAG, "queueRatSwitchRecord(), ratSwitchCaller:%d prefNwType:%d",
            ratSwitchCaller, prefNwType);
    if (ratSwitchCaller == RAT_SWITCH_RESTRICT) {
        mPendingRestrictedRatSwitchRecord.appFamType = appFamType;
        mPendingRestrictedRatSwitchRecord.prefNwType = prefNwType;
        mPendingRestrictedRatSwitchRecord.nwsMode = nwsMode;
        mPendingRestrictedRatSwitchRecord.ratSwitchCaller = ratSwitchCaller;
        mPendingRestrictedRatSwitchRecord.action = action;
        mPendingRestrictedRatSwitchRecord.message = message;
    } else if (ratSwitchCaller == RAT_SWITCH_INIT) {
        mPendingInitRatSwitchRecord.appFamType = appFamType;
        mPendingInitRatSwitchRecord.prefNwType = prefNwType;
        mPendingInitRatSwitchRecord.nwsMode = nwsMode;
        mPendingInitRatSwitchRecord.ratSwitchCaller = ratSwitchCaller;
        mPendingInitRatSwitchRecord.action = action;
        mPendingInitRatSwitchRecord.message = message;
    } else {
        if (mPendingNormalRatSwitchRecord.prefNwType != -1
                && mPendingNormalRatSwitchRecord.message != NULL) {
            logD(RAT_CTRL_TAG, "switchNwRat(), request set prefer network type is pending, "
                    "will be ignored, send response.");
            mNwRatSwitchHandler->responseSetPreferredNetworkType(
                    mPendingNormalRatSwitchRecord.message);
        }
        mPendingNormalRatSwitchRecord.appFamType = appFamType;
        mPendingNormalRatSwitchRecord.prefNwType = prefNwType;
        mPendingNormalRatSwitchRecord.nwsMode = nwsMode;
        mPendingNormalRatSwitchRecord.ratSwitchCaller = ratSwitchCaller;
        mPendingNormalRatSwitchRecord.action = action;
        mPendingNormalRatSwitchRecord.message = message;
    }
}

bool RpNwRatController::isValidPreferredNwType(const AppFamilyType appFamType,
        int prefNwType, NwsMode nwsMode) {
    bool isValidPreferredNwType = true;
    if (appFamType == APP_FAM_3GPP2) {
        if (nwsMode == NWS_MODE_CDMALTE) {
            switch (prefNwType) {
                case PREF_NET_TYPE_CDMA_EVDO_AUTO:
                case PREF_NET_TYPE_CDMA_ONLY:
                case PREF_NET_TYPE_EVDO_ONLY:
                case PREF_NET_TYPE_LTE_CDMA_EVDO:
                case PREF_NET_TYPE_LTE_ONLY:
                case PREF_NET_TYPE_LTE_TDD_ONLY:
                    isValidPreferredNwType = true;
                    break;

                case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
                    if (isRestrictedModeSupport()) {
                        isValidPreferredNwType = true;
                        break;
                    }
                case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
                case PREF_NET_TYPE_GSM_WCDMA:
                case PREF_NET_TYPE_GSM_ONLY:
                case PREF_NET_TYPE_WCDMA:
                case PREF_NET_TYPE_GSM_WCDMA_AUTO:
                case PREF_NET_TYPE_LTE_GSM_WCDMA:
                case PREF_NET_TYPE_LTE_WCDMA:
                case PREF_NET_TYPE_LTE_GSM:
                    isValidPreferredNwType = false;
                    break;
            }
        }

        if (nwsMode == NWS_MODE_CSFB) {
            switch (prefNwType) {
            case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
                if (isRestrictedModeSupport()) {
                    isValidPreferredNwType = true;
                    break;
                }
            case PREF_NET_TYPE_CDMA_EVDO_AUTO:
            case PREF_NET_TYPE_CDMA_ONLY:
            case PREF_NET_TYPE_EVDO_ONLY:
            case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
            case PREF_NET_TYPE_LTE_CDMA_EVDO:
                isValidPreferredNwType = false;
                break;

            case PREF_NET_TYPE_GSM_WCDMA:
            case PREF_NET_TYPE_GSM_ONLY:
            case PREF_NET_TYPE_WCDMA:
            case PREF_NET_TYPE_GSM_WCDMA_AUTO:
            case PREF_NET_TYPE_LTE_GSM_WCDMA:
            case PREF_NET_TYPE_LTE_ONLY:
            case PREF_NET_TYPE_LTE_TDD_ONLY:
            case PREF_NET_TYPE_LTE_WCDMA:
            case PREF_NET_TYPE_LTE_GSM:
                isValidPreferredNwType = true;
                break;
            }
        }
    }
    return isValidPreferredNwType;
}

void RpNwRatController::doPendingRatSwitchRecord() {
    // Handle the pending item
    if (mPendingRestrictedRatSwitchRecord.prefNwType != -1) {
        logD(RAT_CTRL_TAG, "doPendingRestrictedRatSwitchRecord(), appFamType is %d, "
                "prefNwType is %d, nwsMode is %d . ratSwitchCaller is %d",
                mPendingRestrictedRatSwitchRecord.appFamType,
                mPendingRestrictedRatSwitchRecord.prefNwType,
                mPendingRestrictedRatSwitchRecord.nwsMode,
                mPendingRestrictedRatSwitchRecord.ratSwitchCaller);
        logD(RAT_CTRL_TAG, "doPendingRestrictedRatSwitchRecord(), "
                "mCurPreferedNetWorkType is %d, mCurNwsMode is %d.",
                mCurPreferedNetWorkType, mCurNwsMode);
        if (mCurPreferedNetWorkType == mPendingRestrictedRatSwitchRecord.prefNwType) {
            logD(RAT_CTRL_TAG, "doPendingRestrictedRatSwitchRecord(), "
                    "in restricted mode: prefNwType=%d, nwsMode=%d",
                    mCurPreferedNetWorkType, mCurNwsMode);
        } else {
            switchNwRat(mPendingRestrictedRatSwitchRecord.appFamType,
                    mPendingRestrictedRatSwitchRecord.prefNwType,
                    mPendingRestrictedRatSwitchRecord.nwsMode,
                    mPendingRestrictedRatSwitchRecord.ratSwitchCaller,
                    mPendingRestrictedRatSwitchRecord.action,
                    mPendingRestrictedRatSwitchRecord.message);
        }
    } else if (mPendingInitRatSwitchRecord.prefNwType != -1) {
        logD(RAT_CTRL_TAG, "doPendingInitRatSwitchRecord(), appFamType is %d, "
                "prefNwType is %d, nwsMode is %d . ratSwitchCaller is %d",
                mPendingInitRatSwitchRecord.appFamType,
                mPendingInitRatSwitchRecord.prefNwType,
                mPendingInitRatSwitchRecord.nwsMode,
                mPendingInitRatSwitchRecord.ratSwitchCaller);
        PendingRatSwitchRecord tempInitRatSwitchRecord;
        tempInitRatSwitchRecord.appFamType = mPendingInitRatSwitchRecord.appFamType;
        tempInitRatSwitchRecord.prefNwType = mPendingInitRatSwitchRecord.prefNwType;
        tempInitRatSwitchRecord.nwsMode = mPendingInitRatSwitchRecord.nwsMode;
        tempInitRatSwitchRecord.ratSwitchCaller = mPendingInitRatSwitchRecord.ratSwitchCaller;
        tempInitRatSwitchRecord.action = mPendingInitRatSwitchRecord.action;
        tempInitRatSwitchRecord.message = mPendingInitRatSwitchRecord.message;
        mPendingInitRatSwitchRecord.prefNwType = -1;
        switchNwRat(tempInitRatSwitchRecord.appFamType,
                tempInitRatSwitchRecord.prefNwType,
                tempInitRatSwitchRecord.nwsMode,
                tempInitRatSwitchRecord.ratSwitchCaller,
                tempInitRatSwitchRecord.action,
                tempInitRatSwitchRecord.message);

    } else if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
        logD(RAT_CTRL_TAG, "doPendingNormalRatSwitchRecord(), appFamType is %d, "
                "prefNwType is %d, nwsMode is %d . ratSwitchCaller is %d",
                mPendingNormalRatSwitchRecord.appFamType,
                mPendingNormalRatSwitchRecord.prefNwType,
                mPendingNormalRatSwitchRecord.nwsMode,
                mPendingNormalRatSwitchRecord.ratSwitchCaller);
        PendingRatSwitchRecord tempNormalRatSwitchRecord;
        tempNormalRatSwitchRecord.appFamType = mPendingNormalRatSwitchRecord.appFamType;
        tempNormalRatSwitchRecord.prefNwType = mPendingNormalRatSwitchRecord.prefNwType;
        tempNormalRatSwitchRecord.nwsMode = mPendingNormalRatSwitchRecord.nwsMode;
        tempNormalRatSwitchRecord.ratSwitchCaller = mPendingNormalRatSwitchRecord.ratSwitchCaller;
        tempNormalRatSwitchRecord.action = mPendingNormalRatSwitchRecord.action;
        tempNormalRatSwitchRecord.message = mPendingNormalRatSwitchRecord.message;
        mPendingNormalRatSwitchRecord.prefNwType = -1;
        switchNwRat(tempNormalRatSwitchRecord.appFamType,
                tempNormalRatSwitchRecord.prefNwType,
                tempNormalRatSwitchRecord.nwsMode,
                tempNormalRatSwitchRecord.ratSwitchCaller,
                tempNormalRatSwitchRecord.action,
                tempNormalRatSwitchRecord.message);

    } else {
        RpNwRatController *another = (RpNwRatController *) findController(
                getSlotId() == 0 ? 1 : 0,
                RFX_OBJ_CLASS_INFO(RpNwRatController));
        if (another != NULL && another->hasPendingRecord()) {
            logD(RAT_CTRL_TAG, "doPendingRatSwitchRecord, another SIM has pending record, "
                    "current is %d", getSlotId());
            another->doPendingRatSwitchRecord();
        }

        logD(RAT_CTRL_TAG, "doPendingRatSwitchRecord(), no pending record, "
                "another sim has no pending record also, finish");
    }
}

bool RpNwRatController::hasPendingRecord() {
    if (mPendingInitRatSwitchRecord.prefNwType != -1
            || mPendingNormalRatSwitchRecord.prefNwType != -1
            || mPendingRestrictedRatSwitchRecord.prefNwType != -1) {
        return true;
    }
    return false;
}

int RpNwRatController::calculateTargetPreferredNwType(const AppFamilyType appFamType, int prefNwType, NwsMode nwsMode) {
    int targetPrefNwType;
    if (nwsMode == NWS_MODE_CDMALTE) {
        if (appFamType == APP_FAM_3GPP) {
            targetPrefNwType = prefNwType;
        } else if (appFamType == APP_FAM_3GPP2) {
            switch (prefNwType) {
            case PREF_NET_TYPE_CDMA_EVDO_AUTO:
            case PREF_NET_TYPE_CDMA_ONLY:
            case PREF_NET_TYPE_EVDO_ONLY:
            case PREF_NET_TYPE_LTE_ONLY:
            case PREF_NET_TYPE_LTE_TDD_ONLY:
                targetPrefNwType = prefNwType;
                break;
            case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
            case PREF_NET_TYPE_GSM_WCDMA:
            case PREF_NET_TYPE_GSM_ONLY:
            case PREF_NET_TYPE_WCDMA:
            case PREF_NET_TYPE_GSM_WCDMA_AUTO:
                targetPrefNwType = PREF_NET_TYPE_CDMA_EVDO_AUTO;
                break;
            case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
                if (isRestrictedModeSupport()) {
                    targetPrefNwType = prefNwType;
                    break;
                }
            case PREF_NET_TYPE_LTE_CDMA_EVDO:
            case PREF_NET_TYPE_LTE_GSM_WCDMA:
            case PREF_NET_TYPE_LTE_WCDMA:
            case PREF_NET_TYPE_LTE_GSM:
                targetPrefNwType = PREF_NET_TYPE_LTE_CDMA_EVDO;
                break;
            }
        }
    }

    if (nwsMode == NWS_MODE_CSFB) {
        switch (prefNwType) {
        case PREF_NET_TYPE_CDMA_EVDO_AUTO:
        case PREF_NET_TYPE_CDMA_ONLY:
        case PREF_NET_TYPE_EVDO_ONLY:
        case PREF_NET_TYPE_GSM_WCDMA_CDMA_EVDO_AUTO:
            targetPrefNwType = PREF_NET_TYPE_GSM_WCDMA;
            break;
        case PREF_NET_TYPE_LTE_CMDA_EVDO_GSM_WCDMA:
            if (isRestrictedModeSupport()) {
                targetPrefNwType = prefNwType;
                break;
            }
        case PREF_NET_TYPE_LTE_CDMA_EVDO:
            targetPrefNwType = PREF_NET_TYPE_LTE_GSM_WCDMA;
            break;
        case PREF_NET_TYPE_GSM_WCDMA:
        case PREF_NET_TYPE_GSM_ONLY:
        case PREF_NET_TYPE_WCDMA:
        case PREF_NET_TYPE_GSM_WCDMA_AUTO:
        case PREF_NET_TYPE_LTE_GSM_WCDMA:
        case PREF_NET_TYPE_LTE_ONLY:
        case PREF_NET_TYPE_LTE_TDD_ONLY:
        case PREF_NET_TYPE_LTE_WCDMA:
        case PREF_NET_TYPE_LTE_GSM:
            targetPrefNwType = prefNwType;
            break;
        }
    }
    return targetPrefNwType;
}

void RpNwRatController:: updateState(NwsMode nwsMode, int prefNwType){
    logD(RAT_CTRL_TAG,"updateNwsMode(), nwsMode is %d, prefNwType is %d. ", nwsMode, prefNwType);
    mCurNwsMode = nwsMode;
    mCurPreferedNetWorkType = prefNwType;
    getStatusManager()->setIntValue(RFX_STATUS_KEY_NWS_MODE, mCurNwsMode);
    getStatusManager()->setIntValue(RFX_STATUS_KEY_PREFERRED_NW_TYPE, mCurPreferedNetWorkType);
    sIsInSwitching = false;
}

bool RpNwRatController::getSwitchState() {
    return sIsInSwitching;
}

int RpNwRatController::getEnginenerMode() {
    char property_value[PROPERTY_VALUE_MAX] = { 0 };
    property_get("persist.radio.ct.ir.engmode", property_value, "0");
    int engineerMode = atoi(property_value);
    logD(RAT_CTRL_TAG,"getEnginenerMode(), engineerMode is %d. ",engineerMode);
    return engineerMode;
}

int RpNwRatController::getChipTestMode() {
    int mode = 0;
    char chipsetMode[PROPERTY_VALUE_MAX] = { 0 };
    property_get("persist.chiptest.enable", chipsetMode, "0");
    mode = atoi(chipsetMode);
    logD(RAT_CTRL_TAG,"getChipTestMode():%d", mode);
    return mode;
}

void RpNwRatController::clearSuggetRadioCapability() {
    if (getChipTestMode() != 1) {
        RpRadioController* radioController =
                (RpRadioController *) findController(RFX_OBJ_CLASS_INFO(RpRadioController));
        RpSuggestRadioCapabilityCallback callback = RpSuggestRadioCapabilityCallback(this, &RpNwRatController::onSuggestRadioCapabilityResult);
        radioController->suggestedCapability(RIL_CAPABILITY_NONE, callback);
        logD(RAT_CTRL_TAG, "clearSuggetRadioCapability");
    }
}

void RpNwRatController::clearInvalidPendingRecords() {
    if (mPendingInitRatSwitchRecord.prefNwType != -1) {
        logD(RAT_CTRL_TAG, "has pending init rat switch record: appFamType is %d, "
                "prefNwType is %d, nwsMode is %d . ratSwitchCaller is %d",
                mPendingInitRatSwitchRecord.appFamType,
                mPendingInitRatSwitchRecord.prefNwType,
                mPendingInitRatSwitchRecord.nwsMode,
                mPendingInitRatSwitchRecord.ratSwitchCaller);
        if (mPendingInitRatSwitchRecord.action != NULL) {
            mPendingInitRatSwitchRecord.action->act();
        }
        if (mPendingInitRatSwitchRecord.message != NULL) {
            mNwRatSwitchHandler->responseSetPreferredNetworkType(
                    mPendingInitRatSwitchRecord.message);
        }
        mPendingInitRatSwitchRecord.prefNwType = -1;
    }
    if (mPendingNormalRatSwitchRecord.prefNwType != -1) {
        logD(RAT_CTRL_TAG, "has pending normal rat switch record: appFamType is %d, "
                "prefNwType is %d, nwsMode is %d . ratSwitchCaller is %d",
                mPendingNormalRatSwitchRecord.appFamType,
                mPendingNormalRatSwitchRecord.prefNwType,
                mPendingNormalRatSwitchRecord.nwsMode,
                mPendingNormalRatSwitchRecord.ratSwitchCaller);
        if (mPendingNormalRatSwitchRecord.action != NULL) {
            mPendingNormalRatSwitchRecord.action->act();
        }
        if (mPendingNormalRatSwitchRecord.message != NULL) {
            mNwRatSwitchHandler->responseSetPreferredNetworkType(
                    mPendingNormalRatSwitchRecord.message);
        }
        mPendingNormalRatSwitchRecord.prefNwType = -1;
    }
    logD(RAT_CTRL_TAG, "clearInvalidPendingRecords");
}

bool RpNwRatController::isRestrictedModeSupport() {
    bool mode = false;
    char prop_val[PROPERTY_VALUE_MAX] = { 0 };
    property_get("ro.operator.optr", prop_val, "");
    if (strcmp("OP12", prop_val) == 0) {
        mode = true;
    }
    return mode;
}

void RpNwRatController::onSuggestRadioCapabilityResult(SuggestRadioResult result) {
    logD(RAT_CTRL_TAG, "onSuggestRadioCapabilityResult, result is %d .",
            result);
}

NwsMode RpNwRatController::getNwsModeForSwitchCardType() {
    if (mNwRatSwitchHandler != NULL) {
        logD(RAT_CTRL_TAG, "getNwsModeForSwitchCardType(), NwsMode is %d. ",
                mNwRatSwitchHandler->getNwsModeForSwitchCardType());
        return mNwRatSwitchHandler->getNwsModeForSwitchCardType();
    } else {
        logD(RAT_CTRL_TAG, "getNwsModeForSwitchCardType(), mNwRatSwitchHandler has not "
                "been initialized ");
        return NWS_MODE_CSFB;
    }
}

int RpNwRatController::getCapabilitySlotId() {
    char tempstr[PROPERTY_VALUE_MAX];
    memset(tempstr, 0, sizeof(tempstr));
    property_get("persist.radio.simswitch", tempstr, "1");
    int capabilitySlotId = atoi(tempstr) - 1;
    logD(RAT_CTRL_TAG, "getCapabilitySlotId, capability slot is %d .",capabilitySlotId);
    return capabilitySlotId;
}
