# Copyright (C) 2011 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)

# Stub sound_trigger HAL module, used for tests
include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
    $(TOPDIR)/frameworks/av/include/media \
    $(TOPDIR)/frameworks/av/include \
    $(TOPDIR)/frameworks/av/services/audioflinger \
    $(TOPDIR)/frameworks/native/include \
    $(MTK_PATH_SOURCE)/external \
    $(TOPDIR)/vendor/mediatek/proprietary/external \
    $(TOPDIR)/system/core/include/utils \
    $(MTK_PATH_CUSTOM)/cgen/cfgfileinc \

LOCAL_SHARED_LIBRARIES := \
    libmedia \
    libcutils \
    libutils \
    libbinder \
    libhardware_legacy \
    libhardware \
    libblisrc \
    libdl \
    libnvram \
    libspeech_enh_lib \
    libpowermanager \
    libaudiocustparam \
    libaudiosetting \
    libaudiocompensationfilter \
    libcvsd_mtk \
    libmsbc_mtk \
    libaudioutils \
    libaudiocomponentengine \
    libtinyalsa \
    libtinycompress \
    libaudiodcrflt \
    libcustom_nvram \
    libtinyxml

LOCAL_STATIC_LIBRARIES += libaudiostream


LOCAL_STATIC_LIBRARIES += libvoiceunlock




#LOCAL_STATIC_LIBRARIES += libvoiceui
#LOCAL_STATIC_LIBRARIES += libdrvb
#LOCAL_SHARED_LIBRARIES += libmtk_drvb
#LOCAL_MULTILIB := 32
#LOCAL_PRELINK_MODULE := false


LOCAL_MODULE := sound_trigger.primary.$(TARGET_BOARD_PLATFORM)
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_SRC_FILES := sound_trigger_hw.cpp
LOCAL_MODULE_TAGS := optional
LOCAL_32_BIT_ONLY := true

include $(BUILD_SHARED_LIBRARY)

LOCAL_STATIC_LIBRARIES += libvoiceunlock