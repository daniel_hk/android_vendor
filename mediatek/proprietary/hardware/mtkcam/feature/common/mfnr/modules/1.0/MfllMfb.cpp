#include "IMfllCore.h"
#include "MfllMfb.h"
#include "MfllLog.h"
#include "MfllExifInfo.h"

/* mtkcam related headers */
#include <hwutils/HwMisc.h>
#include <metadata/mtk_platform_metadata_tag.h>
#include <drv/isp_reg.h> // dip_x_reg_t

#include <iopipe/SImager/IImageTransform.h> // IImageTransform
#include <utils/Mutex.h> // android::Mutex

#include <memory>

#define LOG_TAG "MtkCam/MfllCore/Mfb"

//
// To print more debug information of this module
// #define __DEBUG


//
// To workaround device hang of MFB stage
// #define WORKAROUND_MFB_STAGE

using namespace NSCam::NSIoPipe::NSPostProc;
using namespace NSCam;
using namespace NSCam::Utils::Format;
using namespace NS3Av3;
using namespace mfll;

using android::sp;
using android::Mutex;
using NSCam::IImageBuffer;
using NSCam::NSIoPipe::EPortType_Memory;
using NSCam::NSIoPipe::NSSImager::IImageTransform;
using NSCam::NSIoPipe::NSPostProc::Input;
using NSCam::NSIoPipe::NSPostProc::Output;
using NSCam::NSIoPipe::NSPostProc::QParams;
using NSCam::NSIoPipe::PortID;

// defined in ispio_pipe_ports.h
// type(port type) index(port index) inout(0:in/1:out)
#include <imageio/ispio_pipe_ports.h>
static const PortID PORT_IMGI   ( NSIoPipe::EPortType_Memory, NSImageio::NSIspio::EPortIndex_IMGI,  0);
static const PortID PORT_IMGBI  ( NSIoPipe::EPortType_Memory, NSImageio::NSIspio::EPortIndex_IMGBI, 0);
static const PortID PORT_IMGCI  ( NSIoPipe::EPortType_Memory, NSImageio::NSIspio::EPortIndex_IMGCI, 0);
static const PortID PORT_VIPI   ( NSIoPipe::EPortType_Memory, NSImageio::NSIspio::EPortIndex_VIPI,  0);
static const PortID PORT_VIP3I  ( NSIoPipe::EPortType_Memory, NSImageio::NSIspio::EPortIndex_VIP3I, 0);
static const PortID PORT_DEPI   ( NSIoPipe::EPortType_Memory, NSImageio::NSIspio::EPortIndex_DEPI,  0);
static const PortID PORT_WDMAO  ( NSIoPipe::EPortType_Memory, NSImageio::NSIspio::EPortIndex_WDMAO, 1);
static const PortID PORT_WROTO  ( NSIoPipe::EPortType_Memory, NSImageio::NSIspio::EPortIndex_WROTO, 1);
static const PortID PORT_MFBO   ( NSIoPipe::EPortType_Memory, NSImageio::NSIspio::EPortIndex_MFBO,  1);
static const PortID PORT_IMG2O  ( NSIoPipe::EPortType_Memory, NSImageio::NSIspio::EPortIndex_IMG2O, 1);
static const PortID PORT_IMG3O  ( NSIoPipe::EPortType_Memory, NSImageio::NSIspio::EPortIndex_IMG3O, 1);

/**
 *  Describe the pass 2 port ID
 *
 *  Notice that, MCrpRsInfo::mGroupID is only works if only if input port is IMGI which describe:
 *  mGroupID = 1: Cropping of the source image
 *  mGroupID = 2: Cropping/Reszing of the first output image
 *  mGroupID = 3: Cropping/Reszing of the second output image
 *
 */
/******************************************************************************
 *  RAW2YUV
 *****************************************************************************/
#define RAW2YUV_PORT_IN             PORT_IMGI
#define RAW2YUV_PORT_OUT            PORT_WDMAO
#define RAW2YUV_PORT_OUT2           PORT_WROTO

/* Cropping group ID is related port ID, notice this ... */
#define RAW2YUV_GID_IN              1 // for PORT_IMGI
#define RAW2YUV_GID_OUT             2 // for PORT_WDMAO
#define RAW2YUV_GID_OUT2            3 // for PORT_WROTEO

/******************************************************************************
 *  MFB
 *****************************************************************************/
/* port */
#define MFB_PORT_IN_BASE_FRAME      PORT_IMGI
#define MFB_PORT_IN_REF_FRAME       PORT_IMGBI
#define MFB_PORT_IN_WEIGHTING       PORT_IMGCI
#define MFB_PORT_OUT_FRAME          PORT_IMG2O
#define MFB_PORT_OUT_WEIGHTING      PORT_MFBO

/* group ID in MFB stage, if not defined which means not support crop */
#define MFB_GID_OUT_FRAME           1 // IMG2O group id in MFB stage

/******************************************************************************
 *  MIX
 *****************************************************************************/
/* port */
#define MIX_PORT_IN_BASE_FRAME      PORT_IMGI
#define MIX_PORT_IN_GOLDEN_FRAME    PORT_VIPI
#define MIX_PORT_IN_WEIGHTING       PORT_IMGBI
#define MIX_PORT_OUT_FRAME          PORT_IMG2O

/*
 * Mutex for pass 2 operation
 *
 * make sure pass 2 is thread-safe, basically it's not ...
 */
static Mutex gMutexPass2Lock;

/* For P2 driver callback usage */
#define P2CBPARAM_MAGICNUM 0xABC
typedef struct _p2cbParam {
    MfllMfb         *self;
    Mutex           *mutex;
    unsigned int    magicNum;
    _p2cbParam ()
    {
        self = NULL;
        mutex = NULL;
        magicNum = P2CBPARAM_MAGICNUM;
    };
    _p2cbParam(MfllMfb *s, Mutex *m)
    {
        self = s;
        mutex = m;
        magicNum = P2CBPARAM_MAGICNUM;
    }
} p2cbParam_t;

static MVOID pass2CbFunc(QParams& rParams)
{
    mfllAutoLogFunc();
    p2cbParam_t *p = reinterpret_cast<p2cbParam_t*>(rParams.mpCookie);
    /* check Magic NUM */
    if (p->magicNum != P2CBPARAM_MAGICNUM) {
        mfllLogE("%s: pass2 param is weird, magic num is different", __FUNCTION__);
    }
    p->mutex->unlock();
}

template <typename T>
inline MVOID updateEntry(IMetadata* pMetadata, MUINT32 const tag, T const& val)
{
    if (pMetadata == NULL)
    {
        mfllLogE("pMetadata is NULL");
        return;
    }

    IMetadata::IEntry entry(tag);
    entry.push_back(val, Type2Type<T>());
    pMetadata->update(tag, entry);
}

static MRect calCrop(MRect const &rSrc, MRect const &rDst, uint32_t ratio)
{
    #define ROUND_TO_2X(x) ((x) & (~0x1))

    MRect rCrop;

    // srcW/srcH < dstW/dstH
    if (rSrc.s.w * rDst.s.h < rDst.s.w * rSrc.s.h)
    {
        rCrop.s.w = rSrc.s.w;
        rCrop.s.h = rSrc.s.w * rDst.s.h / rDst.s.w;
    }
    // srcW/srcH > dstW/dstH
    else if (rSrc.s.w * rDst.s.h > rDst.s.w * rSrc.s.h)
    {
        rCrop.s.w = rSrc.s.h * rDst.s.w / rDst.s.h;
        rCrop.s.h = rSrc.s.h;
    }
    // srcW/srcH == dstW/dstH
    else
    {
        rCrop.s.w = rSrc.s.w;
        rCrop.s.h = rSrc.s.h;
    }

    rCrop.s.w =  ROUND_TO_2X(rCrop.s.w * 100 / ratio);
    rCrop.s.h =  ROUND_TO_2X(rCrop.s.h * 100 / ratio);

    rCrop.p.x = (rSrc.s.w - rCrop.s.w) / 2;
    rCrop.p.y = (rSrc.s.h - rCrop.s.h) / 2;

    #undef ROUND_TO_2X
    return rCrop;
}

static bool workaround_MFB_stage(dip_x_reg_t *regs)
{
#ifdef WORKAROUND_MFB_STAGE
    dip_x_reg_t r = {0};

    r.DIP_X_MFB_CON.Raw     = regs->DIP_X_MFB_CON.Raw;
    r.DIP_X_MFB_LL_CON1.Raw = regs->DIP_X_MFB_LL_CON1.Raw;
    r.DIP_X_MFB_LL_CON2.Raw = regs->DIP_X_MFB_LL_CON2.Raw;
    r.DIP_X_MFB_LL_CON4.Raw = regs->DIP_X_MFB_LL_CON4.Raw;
    r.DIP_X_MFB_EDGE.Raw    = regs->DIP_X_MFB_EDGE.Raw;

    mfllLogD("--- after workaround ---");
    mfllLogD("DIP_X_CTL_YUV_EN  = %#x", regs->DIP_X_CTL_YUV_EN.Raw);
    mfllLogD("DIP_X_G2C_CONV_0A = %#x", regs->DIP_X_G2C_CONV_0A.Raw);
    mfllLogD("DIP_X_G2C_CONV_0B = %#x", regs->DIP_X_G2C_CONV_0B.Raw);
    mfllLogD("DIP_X_G2C_CONV_1A = %#x", regs->DIP_X_G2C_CONV_1A.Raw);
    mfllLogD("DIP_X_G2C_CONV_1B = %#x", regs->DIP_X_G2C_CONV_1B.Raw);
    mfllLogD("DIP_X_G2C_CONV_2A = %#x", regs->DIP_X_G2C_CONV_2A.Raw);
    mfllLogD("DIP_X_G2C_CONV_2B = %#x", regs->DIP_X_G2C_CONV_2B.Raw);

    r.DIP_X_CTL_YUV_EN.Bits.G2C_EN = 1;//enable bit
    r.DIP_X_G2C_CONV_0A.Raw = 0x0200;
    r.DIP_X_G2C_CONV_0B.Raw = 0x0;
    r.DIP_X_G2C_CONV_1A.Raw = 0x02000000;
    r.DIP_X_G2C_CONV_1B.Raw = 0x0;
    r.DIP_X_G2C_CONV_2A.Raw = 0x0;
    r.DIP_X_G2C_CONV_2B.Raw = 0x0200;

    memcpy((void*)regs, (void*)&r, sizeof(dip_x_reg_t));
    return true;
#else
    return false;
#endif
}

static void debug_pass2_registers(const dip_x_reg_t *tuningDat, int stage)
{
#ifdef __DEBUG
    auto regdump = [&tuningDat]()->void{
        mfllLogD("regdump: DIP_X_CTL_START       = %#x", tuningDat->DIP_X_CTL_START.Raw);
        mfllLogD("regdump: DIP_X_CTL_YUV_EN      = %#x", tuningDat->DIP_X_CTL_YUV_EN.Raw);
        mfllLogD("regdump: DIP_X_CTL_YUV2_EN     = %#x", tuningDat->DIP_X_CTL_YUV2_EN.Raw);
        mfllLogD("regdump: DIP_X_CTL_RGB_EN      = %#x", tuningDat->DIP_X_CTL_RGB_EN.Raw);
        mfllLogD("regdump: DIP_X_CTL_DMA_EN      = %#x", tuningDat->DIP_X_CTL_DMA_EN.Raw);
        mfllLogD("regdump: DIP_X_CTL_FMT_SEL     = %#x", tuningDat->DIP_X_CTL_FMT_SEL.Raw);
        mfllLogD("regdump: DIP_X_CTL_PATH_SEL    = %#x", tuningDat->DIP_X_CTL_PATH_SEL.Raw);
        mfllLogD("regdump: DIP_X_CTL_MISC_SEL    = %#x", tuningDat->DIP_X_CTL_MISC_SEL.Raw);
        mfllLogD("");
        mfllLogD("regdump: DIP_X_CTL_CQ_INT_EN   = %#x", tuningDat->DIP_X_CTL_CQ_INT_EN.Raw);
        mfllLogD("regdump: DIP_X_CTL_CQ_INT_EN   = %#x", tuningDat->DIP_X_CTL_CQ_INT_EN.Raw);
        mfllLogD("regdump: DIP_X_CTL_CQ_INT2_EN  = %#x", tuningDat->DIP_X_CTL_CQ_INT2_EN.Raw);
        mfllLogD("regdump: DIP_X_CTL_CQ_INT3_EN  = %#x", tuningDat->DIP_X_CTL_CQ_INT3_EN.Raw);
        mfllLogD("");

    };

    switch (stage) {
    case STAGE_MFB:
        mfllLogD("regdump: ----------------------------------------");
        mfllLogD("regdump: STAGE MFB");
        regdump();
        mfllLogD("regdump: MFB_CON               = %#x", tuningDat->DIP_X_MFB_CON.Raw);
        mfllLogD("regdump: MFB_LL_CON1           = %#x", tuningDat->DIP_X_MFB_LL_CON1.Raw);
        mfllLogD("regdump: MFB_LL_CON2           = %#x", tuningDat->DIP_X_MFB_LL_CON2.Raw);
        mfllLogD("regdump: MFB_LL_CON3           = %#x", tuningDat->DIP_X_MFB_LL_CON3.Raw);
        mfllLogD("regdump: MFB_LL_CON4           = %#x", tuningDat->DIP_X_MFB_LL_CON4.Raw);
        mfllLogD("regdump: MFB_EDGE              = %#x", tuningDat->DIP_X_MFB_EDGE.Raw);
        break;
    case STAGE_MIX:
        mfllLogD("regdump: ----------------------------------------");
        mfllLogD("regdump: STAGE MIX");
        regdump();
        mfllLogD("regdump: MIX3_CTRL_0           = %#x", tuningDat->DIP_X_MIX3_CTRL_0.Raw);
        mfllLogD("regdump: MIX3_CTRL_1           = %#x", tuningDat->DIP_X_MIX3_CTRL_1.Raw);
        mfllLogD("regdump: MIX3_SPARE            = %#x", tuningDat->DIP_X_MIX3_SPARE.Raw);
        break;
    default:
        ;
    }

    int* preg = (int*)tuningDat;
    for (int i = 0; i < sizeof(dip_x_reg_t) / 4; i += 4)
        mfllLogD("regdump: offset = %#x, value = 0x%x, 0x%x, 0x%x, 0x%x", i, preg[i], preg[i+1], preg[i+2], preg[i+3]);

#endif
}

static void dump_exif_info(IMfllCore *c, dip_x_reg_t *t, int stage)
{
//{{{
    if (c == NULL || t == NULL)
        return;
#define _D(tag, value) c->updateExifInfo((unsigned int)tag, (uint32_t)value.Raw)

    switch (stage) {
    case STAGE_BASE_YUV:
        // {{{ STAGE_BASE_YUV
        _D(MF_TAG_BEFORE_DIP_X_UDM_INTP_CRS,            t->DIP_X_UDM_INTP_CRS       );
        _D(MF_TAG_BEFORE_DIP_X_UDM_INTP_NAT,            t->DIP_X_UDM_INTP_NAT       );
        _D(MF_TAG_BEFORE_DIP_X_UDM_INTP_AUG,            t->DIP_X_UDM_INTP_AUG       );
        _D(MF_TAG_BEFORE_DIP_X_UDM_LUMA_LUT1,           t->DIP_X_UDM_LUMA_LUT1      );
        _D(MF_TAG_BEFORE_DIP_X_UDM_LUMA_LUT2,           t->DIP_X_UDM_LUMA_LUT2      );
        _D(MF_TAG_BEFORE_DIP_X_UDM_SL_CTL,              t->DIP_X_UDM_SL_CTL         );
        _D(MF_TAG_BEFORE_DIP_X_UDM_HFTD_CTL,            t->DIP_X_UDM_HFTD_CTL       );
        _D(MF_TAG_BEFORE_DIP_X_UDM_NR_STR,              t->DIP_X_UDM_NR_STR         );
        _D(MF_TAG_BEFORE_DIP_X_UDM_NR_ACT,              t->DIP_X_UDM_NR_ACT         );
        _D(MF_TAG_BEFORE_DIP_X_UDM_HF_STR,              t->DIP_X_UDM_HF_STR         );
        _D(MF_TAG_BEFORE_DIP_X_UDM_HF_ACT1,             t->DIP_X_UDM_HF_ACT1        );
        _D(MF_TAG_BEFORE_DIP_X_UDM_HF_ACT2,             t->DIP_X_UDM_HF_ACT2        );
        _D(MF_TAG_BEFORE_DIP_X_UDM_CLIP,                t->DIP_X_UDM_CLIP           );
        _D(MF_TAG_BEFORE_DIP_X_UDM_DSB,                 t->DIP_X_UDM_DSB            );
        _D(MF_TAG_BEFORE_DIP_X_UDM_TILE_EDGE,           t->DIP_X_UDM_TILE_EDGE      );
        _D(MF_TAG_BEFORE_DIP_X_UDM_DSL,                 t->DIP_X_UDM_DSL            );
        _D(MF_TAG_BEFORE_DIP_X_UDM_SPARE_1,             t->DIP_X_UDM_SPARE_1        );
        _D(MF_TAG_BEFORE_DIP_X_UDM_SPARE_2,             t->DIP_X_UDM_SPARE_2        );
        _D(MF_TAG_BEFORE_DIP_X_UDM_SPARE_3,             t->DIP_X_UDM_SPARE_3        );
        _D(MF_TAG_BEFORE_DIP_X_ANR_CON1,                t->DIP_X_ANR_CON1           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_CON2,                t->DIP_X_ANR_CON2           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_YAD1,                t->DIP_X_ANR_YAD1           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_YAD2,                t->DIP_X_ANR_YAD2           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_Y4LUT1,              t->DIP_X_ANR_Y4LUT1         );
        _D(MF_TAG_BEFORE_DIP_X_ANR_Y4LUT2,              t->DIP_X_ANR_Y4LUT2         );
        _D(MF_TAG_BEFORE_DIP_X_ANR_Y4LUT3,              t->DIP_X_ANR_Y4LUT3         );
        _D(MF_TAG_BEFORE_DIP_X_ANR_C4LUT1,              t->DIP_X_ANR_C4LUT1         );
        _D(MF_TAG_BEFORE_DIP_X_ANR_C4LUT2,              t->DIP_X_ANR_C4LUT2         );
        _D(MF_TAG_BEFORE_DIP_X_ANR_C4LUT3,              t->DIP_X_ANR_C4LUT3         );
        _D(MF_TAG_BEFORE_DIP_X_ANR_A4LUT2,              t->DIP_X_ANR_A4LUT2         );
        _D(MF_TAG_BEFORE_DIP_X_ANR_A4LUT3,              t->DIP_X_ANR_A4LUT3         );
        _D(MF_TAG_BEFORE_DIP_X_ANR_L4LUT1,              t->DIP_X_ANR_L4LUT1         );
        _D(MF_TAG_BEFORE_DIP_X_ANR_L4LUT2,              t->DIP_X_ANR_L4LUT2         );
        _D(MF_TAG_BEFORE_DIP_X_ANR_L4LUT3,              t->DIP_X_ANR_L4LUT3         );
        _D(MF_TAG_BEFORE_DIP_X_ANR_PTY,                 t->DIP_X_ANR_PTY            );
        _D(MF_TAG_BEFORE_DIP_X_ANR_CAD,                 t->DIP_X_ANR_CAD            );
        _D(MF_TAG_BEFORE_DIP_X_ANR_PTC,                 t->DIP_X_ANR_PTC            );
        _D(MF_TAG_BEFORE_DIP_X_ANR_LCE,                 t->DIP_X_ANR_LCE            );
        _D(MF_TAG_BEFORE_DIP_X_ANR_MED1,                t->DIP_X_ANR_MED1           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_MED2,                t->DIP_X_ANR_MED2           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_MED3,                t->DIP_X_ANR_MED3           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_MED4,                t->DIP_X_ANR_MED4           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_HP1,                 t->DIP_X_ANR_HP1            );
        _D(MF_TAG_BEFORE_DIP_X_ANR_HP2,                 t->DIP_X_ANR_HP2            );
        _D(MF_TAG_BEFORE_DIP_X_ANR_HP3,                 t->DIP_X_ANR_HP3            );
        _D(MF_TAG_BEFORE_DIP_X_ANR_ACT1,                t->DIP_X_ANR_ACT1           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_ACT2,                t->DIP_X_ANR_ACT2           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_ACT3,                t->DIP_X_ANR_ACT3           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_ACTYH,               t->DIP_X_ANR_ACTYH          );
        _D(MF_TAG_BEFORE_DIP_X_ANR_ACTC,                t->DIP_X_ANR_ACTC           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_ACTYL,               t->DIP_X_ANR_ACTYL          );
        _D(MF_TAG_BEFORE_DIP_X_ANR_YLAD,                t->DIP_X_ANR_YLAD           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_PTYL,                t->DIP_X_ANR_PTYL           );
        _D(MF_TAG_BEFORE_DIP_X_ANR_LCOEF,               t->DIP_X_ANR_LCOEF          );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_CON1,               t->DIP_X_ANR2_CON1          );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_CON2,               t->DIP_X_ANR2_CON2          );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_YAD1,               t->DIP_X_ANR2_YAD1          );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_Y4LUT1,             t->DIP_X_ANR2_Y4LUT1        );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_Y4LUT2,             t->DIP_X_ANR2_Y4LUT2        );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_Y4LUT3,             t->DIP_X_ANR2_Y4LUT3        );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_L4LUT1,             t->DIP_X_ANR2_L4LUT1        );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_L4LUT2,             t->DIP_X_ANR2_L4LUT2        );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_L4LUT3,             t->DIP_X_ANR2_L4LUT3        );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_CAD,                t->DIP_X_ANR2_CAD           );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_PTC,                t->DIP_X_ANR2_PTC           );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_LCE,                t->DIP_X_ANR2_LCE           );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_MED1,               t->DIP_X_ANR2_MED1          );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_MED2,               t->DIP_X_ANR2_MED2          );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_MED3,               t->DIP_X_ANR2_MED3          );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_MED4,               t->DIP_X_ANR2_MED4          );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_ACTY,               t->DIP_X_ANR2_ACTY          );
        _D(MF_TAG_BEFORE_DIP_X_ANR2_ACTC,               t->DIP_X_ANR2_ACTC          );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_CTRL,               t->DIP_X_SEEE_CTRL          );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_CLIP_CTRL_1,        t->DIP_X_SEEE_CLIP_CTRL_1   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_CLIP_CTRL_2,        t->DIP_X_SEEE_CLIP_CTRL_2   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_CLIP_CTRL_3,        t->DIP_X_SEEE_CLIP_CTRL_3   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_BLND_CTRL_1,        t->DIP_X_SEEE_BLND_CTRL_1   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_BLND_CTRL_2,        t->DIP_X_SEEE_BLND_CTRL_2   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_GN_CTRL,            t->DIP_X_SEEE_GN_CTRL       );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_LUMA_CTRL_1,        t->DIP_X_SEEE_LUMA_CTRL_1   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_LUMA_CTRL_2,        t->DIP_X_SEEE_LUMA_CTRL_2   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_LUMA_CTRL_3,        t->DIP_X_SEEE_LUMA_CTRL_3   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_LUMA_CTRL_4,        t->DIP_X_SEEE_LUMA_CTRL_4   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_SLNK_CTRL_1,        t->DIP_X_SEEE_SLNK_CTRL_1   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_SLNK_CTRL_2,        t->DIP_X_SEEE_SLNK_CTRL_2   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_GLUT_CTRL_1,        t->DIP_X_SEEE_GLUT_CTRL_1   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_GLUT_CTRL_2,        t->DIP_X_SEEE_GLUT_CTRL_2   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_GLUT_CTRL_3,        t->DIP_X_SEEE_GLUT_CTRL_3   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_GLUT_CTRL_4,        t->DIP_X_SEEE_GLUT_CTRL_4   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_GLUT_CTRL_5,        t->DIP_X_SEEE_GLUT_CTRL_5   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_GLUT_CTRL_6,        t->DIP_X_SEEE_GLUT_CTRL_6   );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_OUT_EDGE_CTRL,      t->DIP_X_SEEE_OUT_EDGE_CTRL );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_SE_Y_CTRL,          t->DIP_X_SEEE_SE_Y_CTRL     );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_SE_EDGE_CTRL_1,     t->DIP_X_SEEE_SE_EDGE_CTRL_1);
        _D(MF_TAG_BEFORE_DIP_X_SEEE_SE_EDGE_CTRL_2,     t->DIP_X_SEEE_SE_EDGE_CTRL_2);
        _D(MF_TAG_BEFORE_DIP_X_SEEE_SE_EDGE_CTRL_3,     t->DIP_X_SEEE_SE_EDGE_CTRL_3);
        _D(MF_TAG_BEFORE_DIP_X_SEEE_SE_SPECL_CTRL,      t->DIP_X_SEEE_SE_SPECL_CTRL );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_SE_SPECL_CTRL,      t->DIP_X_SEEE_SE_SPECL_CTRL );
        _D(MF_TAG_BEFORE_DIP_X_SEEE_SE_CORE_CTRL_1,     t->DIP_X_SEEE_SE_CORE_CTRL_1);
        _D(MF_TAG_BEFORE_DIP_X_SEEE_SE_CORE_CTRL_2,     t->DIP_X_SEEE_SE_CORE_CTRL_2);
        //}}} STAGE_BASE_YUV
        break;

    case STAGE_GOLDEN_YUV:
        // {{{ STAGE_GOLDEN_YUV
        _D(MF_TAG_SINGLE_DIP_X_ANR_CON1,                t->DIP_X_ANR_CON1    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_CON2,                t->DIP_X_ANR_CON2    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_YAD1,                t->DIP_X_ANR_YAD1    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_YAD2,                t->DIP_X_ANR_YAD2    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_Y4LUT1,              t->DIP_X_ANR_Y4LUT1  );
        _D(MF_TAG_SINGLE_DIP_X_ANR_Y4LUT2,              t->DIP_X_ANR_Y4LUT2  );
        _D(MF_TAG_SINGLE_DIP_X_ANR_Y4LUT3,              t->DIP_X_ANR_Y4LUT3  );
        _D(MF_TAG_SINGLE_DIP_X_ANR_C4LUT1,              t->DIP_X_ANR_C4LUT1  );
        _D(MF_TAG_SINGLE_DIP_X_ANR_C4LUT2,              t->DIP_X_ANR_C4LUT2  );
        _D(MF_TAG_SINGLE_DIP_X_ANR_C4LUT3,              t->DIP_X_ANR_C4LUT3  );
        _D(MF_TAG_SINGLE_DIP_X_ANR_A4LUT2,              t->DIP_X_ANR_A4LUT2  );
        _D(MF_TAG_SINGLE_DIP_X_ANR_A4LUT3,              t->DIP_X_ANR_A4LUT3  );
        _D(MF_TAG_SINGLE_DIP_X_ANR_L4LUT1,              t->DIP_X_ANR_L4LUT1  );
        _D(MF_TAG_SINGLE_DIP_X_ANR_L4LUT2,              t->DIP_X_ANR_L4LUT2  );
        _D(MF_TAG_SINGLE_DIP_X_ANR_L4LUT3,              t->DIP_X_ANR_L4LUT3  );
        _D(MF_TAG_SINGLE_DIP_X_ANR_PTY,                 t->DIP_X_ANR_PTY     );
        _D(MF_TAG_SINGLE_DIP_X_ANR_CAD,                 t->DIP_X_ANR_CAD     );
        _D(MF_TAG_SINGLE_DIP_X_ANR_PTC,                 t->DIP_X_ANR_PTC     );
        _D(MF_TAG_SINGLE_DIP_X_ANR_LCE,                 t->DIP_X_ANR_LCE     );
        _D(MF_TAG_SINGLE_DIP_X_ANR_MED1,                t->DIP_X_ANR_MED1    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_MED2,                t->DIP_X_ANR_MED2    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_MED3,                t->DIP_X_ANR_MED3    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_MED4,                t->DIP_X_ANR_MED4    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_HP1,                 t->DIP_X_ANR_HP1     );
        _D(MF_TAG_SINGLE_DIP_X_ANR_HP2,                 t->DIP_X_ANR_HP2     );
        _D(MF_TAG_SINGLE_DIP_X_ANR_HP3,                 t->DIP_X_ANR_HP3     );
        _D(MF_TAG_SINGLE_DIP_X_ANR_ACT1,                t->DIP_X_ANR_ACT1    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_ACT2,                t->DIP_X_ANR_ACT2    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_ACT3,                t->DIP_X_ANR_ACT3    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_ACTYH,               t->DIP_X_ANR_ACTYH   );
        _D(MF_TAG_SINGLE_DIP_X_ANR_ACTC,                t->DIP_X_ANR_ACTC    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_ACTYL,               t->DIP_X_ANR_ACTYL   );
        _D(MF_TAG_SINGLE_DIP_X_ANR_YLAD,                t->DIP_X_ANR_YLAD    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_PTYL,                t->DIP_X_ANR_PTYL    );
        _D(MF_TAG_SINGLE_DIP_X_ANR_LCOEF,               t->DIP_X_ANR_LCOEF   );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_CON1,               t->DIP_X_ANR2_CON1   );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_CON2,               t->DIP_X_ANR2_CON2   );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_YAD1,               t->DIP_X_ANR2_YAD1   );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_Y4LUT1,             t->DIP_X_ANR2_Y4LUT1 );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_Y4LUT2,             t->DIP_X_ANR2_Y4LUT2 );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_Y4LUT3,             t->DIP_X_ANR2_Y4LUT3 );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_L4LUT1,             t->DIP_X_ANR2_L4LUT1 );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_L4LUT2,             t->DIP_X_ANR2_L4LUT2 );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_L4LUT3,             t->DIP_X_ANR2_L4LUT3 );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_CAD,                t->DIP_X_ANR2_CAD    );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_PTC,                t->DIP_X_ANR2_PTC    );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_LCE,                t->DIP_X_ANR2_LCE    );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_MED1,               t->DIP_X_ANR2_MED1   );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_MED2,               t->DIP_X_ANR2_MED2   );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_MED3,               t->DIP_X_ANR2_MED3   );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_MED4,               t->DIP_X_ANR2_MED4   );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_ACTY,               t->DIP_X_ANR2_ACTY   );
        _D(MF_TAG_SINGLE_DIP_X_ANR2_ACTC,               t->DIP_X_ANR2_ACTC   );
        // }}} STAGE_GOLDEN_YUV
        break;

    case STAGE_MFB:
        // {{{ STAGE_MFB
        _D(MF_TAG_BLEND_DIP_X_MFB_CON,                  t->DIP_X_MFB_CON      );
        _D(MF_TAG_BLEND_DIP_X_MFB_LL_CON1,              t->DIP_X_MFB_LL_CON1  );
        _D(MF_TAG_BLEND_DIP_X_MFB_LL_CON2,              t->DIP_X_MFB_LL_CON2  );
        _D(MF_TAG_BLEND_DIP_X_MFB_LL_CON4,              t->DIP_X_MFB_LL_CON4  );
        // }}} STAGE_MFB
        break;

    case STAGE_MIX:
        // {{{ STAGE_MIX
        _D(MF_TAG_AFTER_DIP_X_ANR_CON1,                 t->DIP_X_ANR_CON1           );
        _D(MF_TAG_AFTER_DIP_X_ANR_CON2,                 t->DIP_X_ANR_CON2           );
        _D(MF_TAG_AFTER_DIP_X_ANR_YAD1,                 t->DIP_X_ANR_YAD1           );
        _D(MF_TAG_AFTER_DIP_X_ANR_YAD2,                 t->DIP_X_ANR_YAD2           );
        _D(MF_TAG_AFTER_DIP_X_ANR_Y4LUT1,               t->DIP_X_ANR_Y4LUT1         );
        _D(MF_TAG_AFTER_DIP_X_ANR_Y4LUT2,               t->DIP_X_ANR_Y4LUT2         );
        _D(MF_TAG_AFTER_DIP_X_ANR_Y4LUT3,               t->DIP_X_ANR_Y4LUT3         );
        _D(MF_TAG_AFTER_DIP_X_ANR_C4LUT1,               t->DIP_X_ANR_C4LUT1         );
        _D(MF_TAG_AFTER_DIP_X_ANR_C4LUT2,               t->DIP_X_ANR_C4LUT2         );
        _D(MF_TAG_AFTER_DIP_X_ANR_C4LUT3,               t->DIP_X_ANR_C4LUT3         );
        _D(MF_TAG_AFTER_DIP_X_ANR_A4LUT2,               t->DIP_X_ANR_A4LUT2         );
        _D(MF_TAG_AFTER_DIP_X_ANR_A4LUT3,               t->DIP_X_ANR_A4LUT3         );
        _D(MF_TAG_AFTER_DIP_X_ANR_L4LUT1,               t->DIP_X_ANR_L4LUT1         );
        _D(MF_TAG_AFTER_DIP_X_ANR_L4LUT2,               t->DIP_X_ANR_L4LUT2         );
        _D(MF_TAG_AFTER_DIP_X_ANR_L4LUT3,               t->DIP_X_ANR_L4LUT3         );
        _D(MF_TAG_AFTER_DIP_X_ANR_PTY,                  t->DIP_X_ANR_PTY            );
        _D(MF_TAG_AFTER_DIP_X_ANR_CAD,                  t->DIP_X_ANR_CAD            );
        _D(MF_TAG_AFTER_DIP_X_ANR_PTC,                  t->DIP_X_ANR_PTC            );
        _D(MF_TAG_AFTER_DIP_X_ANR_LCE,                  t->DIP_X_ANR_LCE            );
        _D(MF_TAG_AFTER_DIP_X_ANR_MED1,                 t->DIP_X_ANR_MED1           );
        _D(MF_TAG_AFTER_DIP_X_ANR_MED2,                 t->DIP_X_ANR_MED2           );
        _D(MF_TAG_AFTER_DIP_X_ANR_MED3,                 t->DIP_X_ANR_MED3           );
        _D(MF_TAG_AFTER_DIP_X_ANR_MED4,                 t->DIP_X_ANR_MED4           );
        _D(MF_TAG_AFTER_DIP_X_ANR_HP1,                  t->DIP_X_ANR_HP1            );
        _D(MF_TAG_AFTER_DIP_X_ANR_HP2,                  t->DIP_X_ANR_HP2            );
        _D(MF_TAG_AFTER_DIP_X_ANR_HP3,                  t->DIP_X_ANR_HP3            );
        _D(MF_TAG_AFTER_DIP_X_ANR_ACT1,                 t->DIP_X_ANR_ACT1           );
        _D(MF_TAG_AFTER_DIP_X_ANR_ACT2,                 t->DIP_X_ANR_ACT2           );
        _D(MF_TAG_AFTER_DIP_X_ANR_ACT3,                 t->DIP_X_ANR_ACT3           );
        _D(MF_TAG_AFTER_DIP_X_ANR_ACTYH,                t->DIP_X_ANR_ACTYH          );
        _D(MF_TAG_AFTER_DIP_X_ANR_ACTC,                 t->DIP_X_ANR_ACTC           );
        _D(MF_TAG_AFTER_DIP_X_ANR_ACTYL,                t->DIP_X_ANR_ACTYL          );
        _D(MF_TAG_AFTER_DIP_X_ANR_YLAD,                 t->DIP_X_ANR_YLAD           );
        _D(MF_TAG_AFTER_DIP_X_ANR_PTYL,                 t->DIP_X_ANR_PTYL           );
        _D(MF_TAG_AFTER_DIP_X_ANR_LCOEF,                t->DIP_X_ANR_LCOEF          );
        _D(MF_TAG_AFTER_DIP_X_ANR2_CON1,                t->DIP_X_ANR2_CON1          );
        _D(MF_TAG_AFTER_DIP_X_ANR2_CON2,                t->DIP_X_ANR2_CON2          );
        _D(MF_TAG_AFTER_DIP_X_ANR2_YAD1,                t->DIP_X_ANR2_YAD1          );
        _D(MF_TAG_AFTER_DIP_X_ANR2_Y4LUT1,              t->DIP_X_ANR2_Y4LUT1        );
        _D(MF_TAG_AFTER_DIP_X_ANR2_Y4LUT2,              t->DIP_X_ANR2_Y4LUT2        );
        _D(MF_TAG_AFTER_DIP_X_ANR2_Y4LUT3,              t->DIP_X_ANR2_Y4LUT3        );
        _D(MF_TAG_AFTER_DIP_X_ANR2_L4LUT1,              t->DIP_X_ANR2_L4LUT1        );
        _D(MF_TAG_AFTER_DIP_X_ANR2_L4LUT2,              t->DIP_X_ANR2_L4LUT2        );
        _D(MF_TAG_AFTER_DIP_X_ANR2_L4LUT3,              t->DIP_X_ANR2_L4LUT3        );
        _D(MF_TAG_AFTER_DIP_X_ANR2_CAD,                 t->DIP_X_ANR2_CAD           );
        _D(MF_TAG_AFTER_DIP_X_ANR2_PTC,                 t->DIP_X_ANR2_PTC           );
        _D(MF_TAG_AFTER_DIP_X_ANR2_LCE,                 t->DIP_X_ANR2_LCE           );
        _D(MF_TAG_AFTER_DIP_X_ANR2_MED1,                t->DIP_X_ANR2_MED1          );
        _D(MF_TAG_AFTER_DIP_X_ANR2_MED2,                t->DIP_X_ANR2_MED2          );
        _D(MF_TAG_AFTER_DIP_X_ANR2_MED3,                t->DIP_X_ANR2_MED3          );
        _D(MF_TAG_AFTER_DIP_X_ANR2_MED4,                t->DIP_X_ANR2_MED4          );
        _D(MF_TAG_AFTER_DIP_X_ANR2_ACTY,                t->DIP_X_ANR2_ACTY          );
        _D(MF_TAG_AFTER_DIP_X_ANR2_ACTC,                t->DIP_X_ANR2_ACTC          );
        _D(MF_TAG_AFTER_DIP_X_SEEE_CTRL,                t->DIP_X_SEEE_CTRL          );
        _D(MF_TAG_AFTER_DIP_X_SEEE_CLIP_CTRL_1,         t->DIP_X_SEEE_CLIP_CTRL_1   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_CLIP_CTRL_2,         t->DIP_X_SEEE_CLIP_CTRL_2   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_CLIP_CTRL_3,         t->DIP_X_SEEE_CLIP_CTRL_3   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_BLND_CTRL_1,         t->DIP_X_SEEE_BLND_CTRL_1   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_BLND_CTRL_2,         t->DIP_X_SEEE_BLND_CTRL_2   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_GN_CTRL,             t->DIP_X_SEEE_GN_CTRL       );
        _D(MF_TAG_AFTER_DIP_X_SEEE_LUMA_CTRL_1,         t->DIP_X_SEEE_LUMA_CTRL_1   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_LUMA_CTRL_2,         t->DIP_X_SEEE_LUMA_CTRL_2   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_LUMA_CTRL_3,         t->DIP_X_SEEE_LUMA_CTRL_3   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_LUMA_CTRL_4,         t->DIP_X_SEEE_LUMA_CTRL_4   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_SLNK_CTRL_1,         t->DIP_X_SEEE_SLNK_CTRL_1   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_SLNK_CTRL_2,         t->DIP_X_SEEE_SLNK_CTRL_2   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_GLUT_CTRL_1,         t->DIP_X_SEEE_GLUT_CTRL_1   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_GLUT_CTRL_2,         t->DIP_X_SEEE_GLUT_CTRL_2   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_GLUT_CTRL_3,         t->DIP_X_SEEE_GLUT_CTRL_3   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_GLUT_CTRL_4,         t->DIP_X_SEEE_GLUT_CTRL_4   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_GLUT_CTRL_5,         t->DIP_X_SEEE_GLUT_CTRL_5   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_GLUT_CTRL_6,         t->DIP_X_SEEE_GLUT_CTRL_6   );
        _D(MF_TAG_AFTER_DIP_X_SEEE_OUT_EDGE_CTRL,       t->DIP_X_SEEE_OUT_EDGE_CTRL );
        _D(MF_TAG_AFTER_DIP_X_SEEE_SE_Y_CTRL,           t->DIP_X_SEEE_SE_Y_CTRL     );
        _D(MF_TAG_AFTER_DIP_X_SEEE_SE_EDGE_CTRL_1,      t->DIP_X_SEEE_SE_EDGE_CTRL_1);
        _D(MF_TAG_AFTER_DIP_X_SEEE_SE_EDGE_CTRL_2,      t->DIP_X_SEEE_SE_EDGE_CTRL_2);
        _D(MF_TAG_AFTER_DIP_X_SEEE_SE_EDGE_CTRL_3,      t->DIP_X_SEEE_SE_EDGE_CTRL_3);
        _D(MF_TAG_AFTER_DIP_X_SEEE_SE_SPECL_CTRL,       t->DIP_X_SEEE_SE_SPECL_CTRL );
        _D(MF_TAG_AFTER_DIP_X_SEEE_SE_CORE_CTRL_1,      t->DIP_X_SEEE_SE_CORE_CTRL_1);
        _D(MF_TAG_AFTER_DIP_X_SEEE_SE_CORE_CTRL_2,      t->DIP_X_SEEE_SE_CORE_CTRL_2);
        _D(MF_TAG_AFTER_DIP_X_MIX3_CTRL_0,              t->DIP_X_MIX3_CTRL_0        );
        _D(MF_TAG_AFTER_DIP_X_MIX3_CTRL_1,              t->DIP_X_MIX3_CTRL_1        );
        _D(MF_TAG_AFTER_DIP_X_MIX3_SPARE,               t->DIP_X_MIX3_SPARE         );
        // }}} STAGE_MIX
        break;
    }
//}}}
}

//-----------------------------------------------------------------------------

/**
 *  M F L L    M F B
 */
IMfllMfb* IMfllMfb::createInstance(void)
{
    return (IMfllMfb*)new MfllMfb();
}

void IMfllMfb::destroyInstance(void)
{
    decStrong((void*)this);
}

MfllMfb::MfllMfb(void)
{
    m_pNormalStream = NULL;
    m_p3A = NULL;
    m_shotMode = (enum MfllMode)0;
    m_syncPrivateData = NULL;
    m_syncPrivateDataSize = 0;
    m_encodeYuvCount = 0;
    m_blendCount = 0;
    m_bIsInited = false;
    m_nrType = NoiseReductionType_None;
    for (size_t i = 0; i < STAGE_SIZE; i++)
        m_bExifDumpped[i] = 0;
}

MfllMfb::~MfllMfb(void)
{
    mfllAutoLogFunc();

    if (m_pNormalStream) {
        m_pNormalStream->uninit(LOG_TAG);
        m_pNormalStream->destroyInstance();
        m_pNormalStream = NULL;
    }

    if (m_p3A) {
        m_p3A->destroyInstance(LOG_TAG);
    }
}

enum MfllErr MfllMfb::init(int sensorId)
{
    enum MfllErr err = MfllErr_Ok;
    Mutex::Autolock _l(m_mutex);
    mfllAutoLogFunc();

    if (m_bIsInited) { // do not init twice
        goto lbExit;
    }

    mfllLogD("Init MfllMfb with sensor id --> %d", sensorId);
    m_sensorId = sensorId;

    m_pNormalStream = INormalStream::createInstance(sensorId);
    if (m_pNormalStream == NULL) {
        mfllLogE("create INormalStream fail");
        err = MfllErr_UnexpectedError;
        goto lbExit;
    }
    else {
        m_pNormalStream->init(LOG_TAG);
    }

    m_p3A = IHal3A::createInstance(IHal3A::E_Camera_3, sensorId, LOG_TAG);
    if (m_p3A == NULL) {
        mfllLogE("create IHal3A fail");
        err = MfllErr_UnexpectedError;
        goto lbExit;
    }
    /* mark as inited */
    m_bIsInited = true;

lbExit:
    return err;
}

enum MfllErr MfllMfb::blend(IMfllImageBuffer *base, IMfllImageBuffer *ref, IMfllImageBuffer *out, IMfllImageBuffer *wt_in, IMfllImageBuffer *wt_out)
{
    mfllAutoLogFunc();

    /* It MUST NOT be NULL, so don't do error handling */
    if (base == NULL)
        mfllLogE("%s: base is NULL", __FUNCTION__);
    if (ref == NULL)
        mfllLogE("%s: ref is NULL", __FUNCTION__);
    if (out == NULL)
        mfllLogE("%s: out is NULL", __FUNCTION__);
    if (wt_out == NULL)
        mfllLogE("%s: wt_out is NULL", __FUNCTION__);

    return blend(
            (IImageBuffer*)base->getImageBuffer(),
            (IImageBuffer*)ref->getImageBuffer(),
            (IImageBuffer*)out->getImageBuffer(),
            wt_in ? (IImageBuffer*)wt_in->getImageBuffer() : NULL,
            (IImageBuffer*)wt_out->getImageBuffer()
            );
}

enum MfllErr MfllMfb::blend(IImageBuffer *base, IImageBuffer *ref, IImageBuffer *out, IImageBuffer *wt_in, IImageBuffer *wt_out)
{
    enum MfllErr err = MfllErr_Ok;
    MBOOL bRet = MTRUE;

    mfllAutoLogFunc();
    mfllAutoTraceFunc();

    /* get member resource here */
    m_mutex.lock();
    enum NoiseReductionType nrType = m_nrType;
    EIspProfile_T profile = (EIspProfile_T)0; // which will be set later.
    int sensorId = m_sensorId;
    void *privateData = m_syncPrivateData;
    size_t privateDataSize = m_syncPrivateDataSize;
    int index = m_pCore->getIndexByNewIndex(0);
    m_blendCount++;
    /* get metaset */
    if (index >= m_vMetaSet.size()) {
        mfllLogE("%s: index(%d) is out of size of metaset(%d)", __FUNCTION__, index, m_vMetaSet.size());
        m_mutex.unlock();
        return MfllErr_UnexpectedError;
    }
    MetaSet_T metaset = m_vMetaSet[index];
    m_mutex.unlock();

    /**
     *  P A S S 2
     *
     *  Configure input parameters
     */
    QParams params;
    params.mvStreamTag.push_back(NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_MFB_Bld);

    /* input: source frame, base frame [IMGI port] */
    {
        Input p;
        p.mBuffer       = base;
        p.mPortID       = MFB_PORT_IN_BASE_FRAME;
        p.mPortID.group = 0;
        params.mvIn.push_back(p);
        /* IMGI in this stage doesn't supports cropping */
    }

    /* input: reference frame */
    {
        Input p;
        p.mBuffer       = ref;
        p.mPortID       = MFB_PORT_IN_REF_FRAME;
        p.mPortID.group = 0;
        params.mvIn.push_back(p);
        /* This input port in this stage doesn't support crop */
    }

    /* input: weighting table, for not the first blending, we need to give weighting map */
    if (wt_in) {
        Input p;
        p.mBuffer       = wt_in;
        p.mPortID       = MFB_PORT_IN_WEIGHTING;
        p.mPortID.group = 0;
        params.mvIn.push_back(p);
        /* This intput port doesn't support crop */
    }

    /* output: blended frame */
    {
        Output p;
        p.mBuffer       = out;
        p.mPortID       = MFB_PORT_OUT_FRAME;
        p.mPortID.group = 0;
        params.mvOut.push_back(p);

        /* crop info */
        MCrpRsInfo crop;
        crop.mGroupID                   = MFB_GID_OUT_FRAME;
        crop.mCropRect.p_integral.x     = 0; // position of cropping window, in pixel, integer
        crop.mCropRect.p_integral.y     = 0;
        crop.mCropRect.p_fractional.x   = 0;
        crop.mCropRect.p_fractional.y   = 0;
        crop.mCropRect.s.w              = base->getImgSize().w;
        crop.mCropRect.s.h              = base->getImgSize().h;
        crop.mResizeDst.w               = out->getImgSize().w;
        crop.mResizeDst.h               = out->getImgSize().h;
        crop.mFrameGroup                = 0;
        params.mvCropRsInfo.push_back(crop);
    }

    /* output: new weighting table */
    {
        Output p;
        p.mBuffer       = wt_out;
        p.mPortID       = MFB_PORT_OUT_WEIGHTING;
        p.mPortID.group = 0;
        params.mvOut.push_back(p);
        /* This port doesn't support cropping */
    }

    /* determine ISP profile */
    if (isZsdMode(m_shotMode)) {
        profile = (nrType == NoiseReductionType_SWNR ? EIspProfile_VSS_MFB_Blending_All_Off_SWNR :  EIspProfile_VSS_MFB_Blending_All_Off);
    }
    else {
        profile = (nrType == NoiseReductionType_SWNR ? EIspProfile_MFB_Blending_All_Off_SWNR :  EIspProfile_MFB_Blending_All_Off);
    }

    /* execute pass 2 operation */
    {
        /* pass 2 critical section */
        Mutex::Autolock _l(gMutexPass2Lock);

        /* add tuning data is necessary */
        std::unique_ptr<char> tuning_reg(new char[sizeof(dip_x_reg_t)]());
        void *tuning_lsc;

        mfllLogD("%s: create tuning register data chunk with size %d",
                __FUNCTION__, sizeof(dip_x_reg_t));

        TuningParam rTuningParam = { NULL, NULL};
        rTuningParam.pRegBuf = tuning_reg.get();

        updateEntry<MUINT8>(&metaset.halMeta, MTK_3A_PGN_ENABLE, 0);
        updateEntry<MUINT8>(&metaset.halMeta, MTK_3A_ISP_PROFILE, profile);
        // to makes debug info different
        updateEntry<MINT32>(&metaset.halMeta, MTK_PIPELINE_REQUEST_NUMBER, m_blendCount - 1);

        MetaSet_T rMetaSet;

        if (m_p3A->setIsp(0, metaset, &rTuningParam, &rMetaSet) == 0) {
            mfllLogD("%s: get tuning data, reg=%p, lsc=%p",
                    __FUNCTION__, rTuningParam.pRegBuf, rTuningParam.pLsc2Buf);

            {
                int bilinearFilter = (base->getImgSize() == ref->getImgSize() ? 0 : 1);
                dip_x_reg_t *tuningDat = (dip_x_reg_t*)tuning_reg.get();

                workaround_MFB_stage(tuningDat);

                /* MFB CON */
                tuningDat->DIP_X_MFB_CON.Bits.BLD_LL_BRZ_EN = bilinearFilter;
                // 0: first time, 1: blend with input weighting
                tuningDat->DIP_X_MFB_CON.Bits.BLD_MODE      = (index == 0 ? 0 : 1);

                /* MFB_LL_CON3 */
                /* output resolution */
                tuningDat->DIP_X_MFB_LL_CON3.Raw =
                    (wt_out->getImgSize().h << 16) | wt_out->getImgSize().w;

                if (m_bExifDumpped[STAGE_MFB] == 0) {
                    dump_exif_info(m_pCore, tuningDat, STAGE_MFB);
                    m_bExifDumpped[STAGE_MFB] = 1;
                }
#ifdef __DEBUG
                debug_pass2_registers(tuningDat, STAGE_MFB);
#endif
            }
            params.mvTuningData.push_back((void*)tuning_reg.get()); // adding tuning data

        }
        else {
            mfllLogE("%s: set ISP profile failed...", __FUNCTION__);
            err = MfllErr_UnexpectedError;
            goto lbExit;
        }

        /**
         *  Finally, we're going to invoke P2 driver to encode Raw. Notice that,
         *  we must use async function call to trigger P2 driver, which means
         *  that we have to give a callback function to P2 driver.
         *
         *  E.g.:
         *      QParams::mpfnCallback = CALLBACK_FUNCTION
         *      QParams::mpCookie --> argument that CALLBACK_FUNCTION will get
         *
         *  Due to we wanna make the P2 driver as a synchronized flow, here we
         *  simply using Mutex to sync async call of P2 driver.
         */
        Mutex mutex;
        mutex.lock();

        p2cbParam_t p2cbPack(this, &mutex);

        params.mpfnCallback = pass2CbFunc;
        params.mpCookie = (void*)&p2cbPack;

        mfllLogD("%s: enque pass 2", __FUNCTION__);
        m_pNormalStream->enque(params);
        mfllLogD("%s: deque pass 2", __FUNCTION__);

        /* lock again, and wait */
        mutex.lock(); // locked, and wait unlock from pass2CbFunc.
        mutex.unlock(); // unlock.

        mfllLogD("mfb dequed");
    }
lbExit:
    return err;
}

enum MfllErr MfllMfb::mix(IMfllImageBuffer *base, IMfllImageBuffer *ref, IMfllImageBuffer *out, IMfllImageBuffer *wt)
{
    mfllAutoLogFunc();
    mfllAutoTraceFunc();

    enum MfllErr err = MfllErr_Ok;

    IImageBuffer *img_src = 0;
    IImageBuffer *img_ref = 0;
    IImageBuffer *img_dst = 0;
    IImageBuffer *img_wt = 0;

    /* check buffers */
    if (base == 0 || ref == 0 || out == 0 || wt == 0) {
        mfllLogE("%s: any argument cannot be NULL", __FUNCTION__);
        return MfllErr_BadArgument;
    }

    img_src = (IImageBuffer*)base->getImageBuffer();
    img_ref = (IImageBuffer*)ref->getImageBuffer();
    img_dst = (IImageBuffer*)out->getImageBuffer();
    img_wt =  (IImageBuffer*)wt->getImageBuffer();

    /* check resolution */
    {
        MSize size_src = img_src->getImgSize();
        MSize size_dst = img_dst->getImgSize();
        MSize size_wt  = img_wt->getImgSize();

        if (size_src != size_dst || size_src != size_wt) {
            mfllLogE("%s: Resolution of images are not the same", __FUNCTION__);
            mfllLogE("%s: src=%dx%d, dst=%dx%d, wt=%dx%d",
                    __FUNCTION__,
                    size_src.w, size_src.h,
                    size_dst.w, size_dst.h,
                    size_wt.w, size_wt.h);
            return MfllErr_BadArgument;
        }
    }

    m_mutex.lock();
    int sensorId = m_sensorId;
    enum NoiseReductionType nrType = m_nrType;
    enum MfllMode shotMode = m_shotMode;
    EIspProfile_T profile = (EIspProfile_T)0; // which will be set later.
    void *privateData = m_syncPrivateData;
    size_t privateDataSize = m_syncPrivateDataSize;
    int index = m_pCore->getIndexByNewIndex(0);
    /* get metaset */
    if (index >= m_vMetaSet.size()) {
        mfllLogE("%s: index(%d) is out of size of metaset(%d)", __FUNCTION__, index, m_vMetaSet.size());
        m_mutex.unlock();
        return MfllErr_UnexpectedError;
    }
    MetaSet_T metaset = m_vMetaSet[index];
    m_mutex.unlock();

	MBOOL 	ret = MTRUE;

    /**
     * P A S S 2
     */
    QParams params;

    /* Mixing */
    params.mvStreamTag.push_back(NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_MFB_Mix);

    // input: blended frame [IMGI port]
    {
        Input p;
        p.mPortID       = MIX_PORT_IN_BASE_FRAME; // should be IMGI port
        p.mPortID.group = 0;
        p.mBuffer       = img_src;
        params.mvIn.push_back(p);

        /* cropping info */
        // Even though no cropping necessary, but we have to pass in a crop info
        // with group id = 1 because pass 2 driver MUST need it
        MCrpRsInfo crop;
        crop.mGroupID       = 1;
        crop.mCropRect.p_integral.x = 0; // position pixel, in integer
        crop.mCropRect.p_integral.y = 0;
        crop.mCropRect.p_fractional.x = 0; // always be 0
        crop.mCropRect.p_fractional.y = 0;
        crop.mCropRect.s.w  = img_src->getImgSize().w;
        crop.mCropRect.s.h  = img_src->getImgSize().h;
        crop.mResizeDst.w   = img_src->getImgSize().w;
        crop.mResizeDst.h   = img_src->getImgSize().h;
        crop.mFrameGroup    = 0;
        params.mvCropRsInfo.push_back(crop);
    }

    // input: golden frame
    {
        Input p;
        p.mPortID       = MIX_PORT_IN_GOLDEN_FRAME;
        p.mPortID.group = 0;
        p.mBuffer       = img_ref;
        params.mvIn.push_back(p);
        /* this port is not support crop info */
    }

    // input: weighting
    {
        Input p;
        p.mPortID       = MIX_PORT_IN_WEIGHTING;
        p.mPortID.group = 0;
        p.mBuffer       = img_wt;
        params.mvIn.push_back(p);
        /* this port is not support crop info */
    }

    // output: a frame
    {
        Output p;
        p.mPortID       = MIX_PORT_OUT_FRAME;
        p.mPortID.group = 0;
        p.mBuffer       = img_dst;
        params.mvOut.push_back(p);
        /* this port is not support crop info */
    }

    /* determine ISP profile */
    if (isZsdMode(shotMode)) {
        profile = (nrType == NoiseReductionType_SWNR ? EIspProfile_VSS_MFB_PostProc_Mixing_SWNR : EIspProfile_VSS_MFB_PostProc_Mixing);
    }
    else {
        profile = (nrType == NoiseReductionType_SWNR ? EIspProfile_MFB_PostProc_Mixing_SWNR : EIspProfile_MFB_PostProc_Mixing);
    }

    /* execute pass 2 operation */
    {
        /* pass 2 critical section */
        Mutex::Autolock _l(gMutexPass2Lock);

        /* add tuning data is necessary */
        std::unique_ptr<char> tuning_reg(new char[sizeof(dip_x_reg_t)]());
        void *tuning_lsc;

        mfllLogD("%s: create tuning register data chunk with size %d",
                __FUNCTION__, sizeof(dip_x_reg_t));

        TuningParam rTuningParam = { NULL, NULL};
        rTuningParam.pRegBuf = tuning_reg.get();

        updateEntry<MUINT8>(&metaset.halMeta, MTK_3A_PGN_ENABLE, 0);
        updateEntry<MUINT8>(&metaset.halMeta, MTK_3A_ISP_PROFILE, profile);

        MetaSet_T rMetaSet;

        if (m_p3A->setIsp(0, metaset, &rTuningParam, &rMetaSet) == 0) {

            mfllLogD("%s: get tuning data, reg=%p, lsc=%p",
                    __FUNCTION__, rTuningParam.pRegBuf, rTuningParam.pLsc2Buf);

            dip_x_reg_t *tuningDat = (dip_x_reg_t*)tuning_reg.get();

            if (m_bExifDumpped[STAGE_MIX] == 0) {
                dump_exif_info(m_pCore, tuningDat, STAGE_MIX);
                m_bExifDumpped[STAGE_MIX] = 1;
            }

#ifdef __DEBUG
            debug_pass2_registers(tuningDat, STAGE_MIX);
#endif

            params.mvTuningData.push_back(tuning_reg.get()); // adding tuning data
        }
        else {
            mfllLogE("%s: set ISP profile failed...", __FUNCTION__);
            err = MfllErr_UnexpectedError;
            return err;
        }

        /**
         *  Finally, we're going to invoke P2 driver to encode Raw. Notice that,
         *  we must use async function call to trigger P2 driver, which means
         *  that we have to give a callback function to P2 driver.
         *
         *  E.g.:
         *      QParams::mpfnCallback = CALLBACK_FUNCTION
         *      QParams::mpCookie --> argument that CALLBACK_FUNCTION will get
         *
         *  Due to we wanna make the P2 driver as a synchronized flow, here we
         *  simply using Mutex to sync async call of P2 driver.
         */
        Mutex mutex;
        mutex.lock();

        p2cbParam_t p2cbPack(this, &mutex);

        params.mpfnCallback = pass2CbFunc;
        params.mpCookie = (void*)&p2cbPack;

        m_pNormalStream->enque(params);

        /* lock again, and wait */
        mutex.lock(); // locked, and wait unlock from pass2CbFunc.
        mutex.unlock(); // unlock.

        mfllLogD("mix dequed");
    }

    return err;
}

enum MfllErr MfllMfb::setSyncPrivateData(void *data, size_t size)
{
    mfllLogD("data=%p, size=%d", data, size);
    if (data == NULL || size <= 0) {
        mfllLogW("set sync private data receieve NULL, ignored");
        return MfllErr_Ok;
    }

    Mutex::Autolock _l(m_mutex);

    /**
     *  *data contains two address, the first one is the address of App IMetadata,
     *  the second one is Hal. Please make sure the caller also send IMetadata
     *  addresses
     */
    long long *pAddr = (long long*)data;
    IMetadata *pAppMeta = (IMetadata*)(void*)(*(pAddr + 0));
    IMetadata *pHalMeta = (IMetadata*)(void*)(*(pAddr + 1));
    MetaSet_T m;
    m.halMeta = *pHalMeta;
    m.appMeta = *pAppMeta;
    m_vMetaSet.push_back(m);
    m_vMetaApp.push_back(pAppMeta);
    m_vMetaHal.push_back(pHalMeta);
    mfllLogD("saves MetaSet_T, size = %d", m_vMetaSet.size());
    return MfllErr_Ok;
}

/******************************************************************************
 * encodeRawToYuv
 *
 * Interface for encoding a RAW buffer to an YUV buffer
 *****************************************************************************/
enum MfllErr MfllMfb::encodeRawToYuv(IMfllImageBuffer *input, IMfllImageBuffer *output, const enum YuvStage &s)
{
    return encodeRawToYuv(input, output, NULL, s);
}

/******************************************************************************
 * encodeRawToYuv
 *
 * Interface for encoding a RAW buffer to two YUV buffers
 *****************************************************************************/
enum MfllErr MfllMfb::encodeRawToYuv(
            IMfllImageBuffer *input,
            IMfllImageBuffer *output,
            IMfllImageBuffer *output_q,
            enum YuvStage s /* = YuvStage_RawToYuy2 */)
{
    enum MfllErr err = MfllErr_Ok;
    IImageBuffer *iimgOut2 = NULL;

    if (input == NULL) {
        mfllLogE("%s: input buffer is NULL", __FUNCTION__);
        err = MfllErr_BadArgument;
    }
    if (output == NULL) {
        mfllLogE("%s: output buffer is NULL", __FUNCTION__);
        err = MfllErr_BadArgument;
    }
    if (err != MfllErr_Ok)
        goto lbExit;

    if (output_q) {
        iimgOut2 = (IImageBuffer*)output_q->getImageBuffer();
    }

    err = encodeRawToYuv(
            (IImageBuffer*)input->getImageBuffer(),
            (IImageBuffer*)output->getImageBuffer(),
            iimgOut2,
            s);

lbExit:
    return err;

}

/******************************************************************************
 * encodeRawToYuv
 *
 * Real implmentation that to control PASS 2 drvier for encoding RAW to YUV
 *****************************************************************************/
enum MfllErr MfllMfb::encodeRawToYuv(
        IImageBuffer *src,
        IImageBuffer *dst,
        IImageBuffer *dst2,
        const enum YuvStage &s)
{
    enum MfllErr err = MfllErr_Ok;
    MBOOL bRet = MTRUE;
    EIspProfile_T profile = EIspProfile_MFB_PostProc_EE_Off;

    mfllAutoLogFunc();
    mfllTraceCall();

    /* If it's encoding base RAW ... */
    bool bBaseYuvStage = (s == YuvStage_BaseYuy2 || s == YuvStage_GoldenYuy2) ? true : false;

    m_mutex.lock();
    int index = m_encodeYuvCount++;
    int sensorId = m_sensorId;
    IImageBuffer *pInput = src;
    IImageBuffer *pOutput = dst;
    IImageBuffer *pOutputQ = dst2;
    int gid = 1;
    /* Do not increase YUV stage if it's encoding base YUV or golden YUV */
    if (bBaseYuvStage) {
        m_encodeYuvCount--;
        index = m_pCore->getIndexByNewIndex(0); // Use the first metadata
    }

    /* check if metadata is ok */
    if ((size_t)index >= m_vMetaSet.size()) {
        mfllLogE("%s: index(%d) is out of metadata set size(%d) ",
                __FUNCTION__,
                index,
                m_vMetaSet.size());
        m_mutex.unlock();
        return MfllErr_UnexpectedError;
    }
    MetaSet_T metaset = m_vMetaSet[index]; // using copy
    IMetadata *pHalMeta = m_vMetaHal[index]; // get address of IMetadata
    m_mutex.unlock();

    /**
     *  Select profile based on Stage:
     *  1) Raw to Yv16
     *  2) Encoding base YUV
     *  3) Encoding golden YUV
     *
     */
    switch(s) {
    case YuvStage_RawToYuy2:
    case YuvStage_RawToYv16:
    case YuvStage_BaseYuy2:
        if (isZsdMode(m_shotMode)) {
            profile = EIspProfile_VSS_MFB_PostProc_EE_Off; // Not related with MNR
        }
        else {
            profile = EIspProfile_MFB_PostProc_EE_Off;
        }
        break;
    case YuvStage_GoldenYuy2:
        if (isZsdMode(m_shotMode)) {
            profile = (m_nrType == NoiseReductionType_SWNR ? EIspProfile_VSS_MFB_PostProc_ANR_EE_SWNR : EIspProfile_VSS_MFB_PostProc_ANR_EE);
        }
        else {
            profile = (m_nrType == NoiseReductionType_SWNR ? EIspProfile_MFB_PostProc_ANR_EE_SWNR : EIspProfile_MFB_PostProc_ANR_EE);
        }
        break;
    } // switch

    /**
     *  Ok, we're going to configure P2 driver
     */
    QParams params;

    /* If using ZSD mode, to use Vss profile can improve performance */
    params.mvStreamTag.push_back(isZsdMode(m_shotMode)
            ? NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Vss
            : NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal
            );

    /* input: source frame [IMGI port] */
    {
        Input p;
        p.mBuffer          = pInput;
        p.mPortID          = RAW2YUV_PORT_IN;
        p.mPortID.group    = 0; // always be 0
        params.mvIn.push_back(p);

        /* cropping info */
        MCrpRsInfo crop;
        crop.mGroupID       = RAW2YUV_GID_IN;
        crop.mCropRect.p_integral.x = 0; // position pixel, in integer
        crop.mCropRect.p_integral.y = 0;
        crop.mCropRect.p_fractional.x = 0; // always be 0
        crop.mCropRect.p_fractional.y = 0;
        crop.mCropRect.s.w  = pInput->getImgSize().w;
        crop.mCropRect.s.h  = pInput->getImgSize().h;
        crop.mResizeDst.w   = pInput->getImgSize().w;
        crop.mResizeDst.h   = pInput->getImgSize().h;
        crop.mFrameGroup    = 0;
        params.mvCropRsInfo.push_back(crop);
        mfllLogD("output: srcCrop (x,y,w,h)=(%d,%d,%d,%d)",
                crop.mCropRect.p_integral.x, crop.mCropRect.p_integral.y,
                crop.mCropRect.s.w, crop.mCropRect.s.h);
        mfllLogD("output: dstSize (w,h)=(%d,%d",
                crop.mResizeDst.w, crop.mResizeDst.h);
    }

    /* output: destination frame [WDMAO port] */
    {
        Output p;
        p.mBuffer          = pOutput;
        p.mPortID          = RAW2YUV_PORT_OUT;
        p.mPortID.group    = 0; // always be 0
        params.mvOut.push_back(p);

        /**
         *  Check resolution between input and output, if the ratio is different,
         *  a cropping window should be applied.
         */
        MRect srcCrop = calCrop(
                MRect(MPoint(0,0), pInput->getImgSize()),
                MRect(MPoint(0,0), pOutput->getImgSize()),
                100);

        /**
         *  Cropping info, only works with input port is IMGI port.
         *  mGroupID should be the index of the MCrpRsInfo.
         */
        MCrpRsInfo crop;
        crop.mGroupID       = RAW2YUV_GID_OUT;
        crop.mCropRect.p_integral.x = srcCrop.p.x; // position pixel, in integer
        crop.mCropRect.p_integral.y = srcCrop.p.y;
        crop.mCropRect.p_fractional.x = 0; // always be 0
        crop.mCropRect.p_fractional.y = 0;
        crop.mCropRect.s.w  = srcCrop.s.w;
        crop.mCropRect.s.h  = srcCrop.s.h;
        crop.mResizeDst.w   = pOutput->getImgSize().w;
        crop.mResizeDst.h   = pOutput->getImgSize().h;
        crop.mFrameGroup    = 0;
        params.mvCropRsInfo.push_back(crop);
        mfllLogD("output: srcCrop (x,y,w,h)=(%d,%d,%d,%d)",
                crop.mCropRect.p_integral.x, crop.mCropRect.p_integral.y,
                crop.mCropRect.s.w, crop.mCropRect.s.h);
        mfllLogD("output: dstSize (w,h)=(%d,%d",
                crop.mResizeDst.w, crop.mResizeDst.h);
    }

    /* output2: destination frame [WROTO prot] */
    if (pOutputQ) {
        Output p;
        p.mBuffer          = pOutputQ;
        p.mPortID          = RAW2YUV_PORT_OUT2;
        p.mPortID.group    = 0; // always be 0
        params.mvOut.push_back(p);

        /**
         *  Check resolution between input and output, if the ratio is different,
         *  a cropping window should be applied.
         */
        MRect srcCrop = calCrop(
                MRect(MPoint(0,0), pInput->getImgSize()),
                MRect(MPoint(0,0), pOutputQ->getImgSize()),
                100);

        /**
         *  Cropping info, only works with input port is IMGI port.
         *  mGroupID should be the index of the MCrpRsInfo.
         */
        MCrpRsInfo crop;
        crop.mGroupID       = RAW2YUV_GID_OUT2;
        crop.mCropRect.p_integral.x = srcCrop.p.x; // position pixel, in integer
        crop.mCropRect.p_integral.y = srcCrop.p.y;
        crop.mCropRect.p_fractional.x = 0; // always be 0
        crop.mCropRect.p_fractional.y = 0;
        crop.mCropRect.s.w  = srcCrop.s.w;
        crop.mCropRect.s.h  = srcCrop.s.h;
        crop.mResizeDst.w   = pOutputQ->getImgSize().w;
        crop.mResizeDst.h   = pOutputQ->getImgSize().h;
        crop.mFrameGroup    = 0;
        params.mvCropRsInfo.push_back(crop);

        mfllLogD("output2: srcCrop (x,y,w,h)=(%d,%d,%d,%d)",
                crop.mCropRect.p_integral.x, crop.mCropRect.p_integral.y,
                crop.mCropRect.s.w, crop.mCropRect.s.h);
        mfllLogD("output2: dstSize (w,h)=(%d,%d",
                crop.mResizeDst.w, crop.mResizeDst.h);
    }

    /* get ready to operate P2 driver, it's a long stroy ... */
    {
        /* pass 2 critical section */
        mfllLogD("%s: wait pass2 critical section", __FUNCTION__);
        Mutex::Autolock _l(gMutexPass2Lock);
        mfllLogD("%s: enter pass2 critical section", __FUNCTION__);

        /* add tuning data is necessary */
        std::unique_ptr<char> tuning_reg(new char[sizeof(dip_x_reg_t)]());
        void *tuning_lsc;

        mfllLogD("%s: create tuning register data chunk with size %d",
                __FUNCTION__, sizeof(dip_x_reg_t));

        {
            TuningParam rTuningParam = { NULL, NULL};
            rTuningParam.pRegBuf = tuning_reg.get();
            updateEntry<MUINT8>(&metaset.halMeta, MTK_3A_PGN_ENABLE, 1);
            updateEntry<MUINT8>(&metaset.halMeta, MTK_3A_ISP_PROFILE, profile);

            MetaSet_T rMetaSet;

            if (m_p3A->setIsp(0, metaset, &rTuningParam, &rMetaSet) == 0) {

                mfllLogD("%s: get tuning data, reg=%p, lsc=%p",
                        __FUNCTION__, rTuningParam.pRegBuf, rTuningParam.pLsc2Buf);

                dip_x_reg_t *tuningDat = (dip_x_reg_t*)tuning_reg.get();

                switch (s) {
                case YuvStage_RawToYuy2:
                case YuvStage_RawToYv16:
                case YuvStage_BaseYuy2:
                    if (m_bExifDumpped[STAGE_BASE_YUV] == 0) {
                        dump_exif_info(m_pCore, tuningDat, STAGE_BASE_YUV);
                        m_bExifDumpped[STAGE_BASE_YUV] = 1;
                    }
                    break;
                case YuvStage_GoldenYuy2:
                    if (m_bExifDumpped[STAGE_GOLDEN_YUV] == 0) {
                        dump_exif_info(m_pCore, tuningDat, STAGE_GOLDEN_YUV);
                        m_bExifDumpped[STAGE_GOLDEN_YUV] = 1;
                    }
                    break;
                }

                /* update metadata within the one given by IHal3A */
                *pHalMeta += rMetaSet.halMeta;
                params.mvTuningData.push_back(tuning_reg.get()); // adding tuning data

                /* LSC tuning data is constructed as an IImageBuffer, and we don't need to release */
                IImageBuffer* pSrc = static_cast<IImageBuffer*>(rTuningParam.pLsc2Buf);
                if (pSrc != NULL) {
                    Input src;
                    src.mPortID         = PORT_DEPI;
                    src.mPortID.group   = 0;
                    src.mBuffer         = pSrc;
                    params.mvIn.push_back(src);
                }
                else {
                    mfllLogE("create LSC image buffer fail");
                    err = MfllErr_UnexpectedError;
                }
            }
            else {
                mfllLogE("%s: set ISP profile failed...", __FUNCTION__);
                err = MfllErr_UnexpectedError;
                goto lbExit;
            }
        }

        /**
         *  Finally, we're going to invoke P2 driver to encode Raw. Notice that,
         *  we must use async function call to trigger P2 driver, which means
         *  that we have to give a callback function to P2 driver.
         *
         *  E.g.:
         *      QParams::mpfnCallback = CALLBACK_FUNCTION
         *      QParams::mpCookie --> argument that CALLBACK_FUNCTION will get
         *
         *  Due to we wanna make the P2 driver as a synchronized flow, here we
         *  simply using Mutex to sync async call of P2 driver.
         */
        Mutex mutex;
        mutex.lock();

        p2cbParam_t p2cbPack(this, &mutex);

        params.mpfnCallback = pass2CbFunc;
        params.mpCookie = (void*)&p2cbPack;

        mfllLogD("%s: enque", __FUNCTION__);

#ifdef __DEBUG
        if (pOutputQ) {
            mfllLogD("%s: VA input=%p,output1=%p,output2=%p", __FUNCTION__,
                    pInput->getBufVA(0), pOutput->getBufVA(0), pOutputQ->getBufVA(0));
            mfllLogD("%s: PA input=%p,output1=%p,output2=%p", __FUNCTION__,
                    pInput->getBufPA(0), pOutput->getBufPA(0), pOutputQ->getBufPA(0));
        }
        else {
            mfllLogD("%s: VA input=%p,output1=%p", __FUNCTION__,
                    pInput->getBufVA(0), pOutput->getBufVA(0));
            mfllLogD("%s: PA input=%p,output1=%p", __FUNCTION__,
                    pInput->getBufPA(0), pOutput->getBufPA(0));
        }
#endif

        m_pNormalStream->enque(params);
        mfllLogD("%s: dequed", __FUNCTION__);

        /* lock again, and wait */
        mutex.lock(); // locked, and wait unlock from pass2CbFunc.
        mutex.unlock(); // unlock.

        mfllLogD("p2 dequed");
    }

lbExit:
    return err;
}
