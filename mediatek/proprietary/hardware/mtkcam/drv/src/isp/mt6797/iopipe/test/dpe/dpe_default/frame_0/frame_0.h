#ifndef DPE_FRAME0_H
#define DPE_FRAME0_H
extern char frame_0_dpe_confo_l_frame_00_01[];
#define frame_0_dpe_confo_l_frame_00_01_size 2304
extern unsigned int frame_0_golden_dpe_confo_l_size;
extern char* frame_0_golden_dpe_confo_l_frame;

extern char frame_0_dpe_confo_r_frame_00_01[];
#define frame_0_dpe_confo_r_frame_00_01_size 2304
extern unsigned int frame_0_golden_dpe_confo_r_size;
extern char* frame_0_golden_dpe_confo_r_frame;

extern char frame_0_dpe_dvi_l_frame_00[];
#define frame_0_dpe_dvi_l_frame_00_size 4608
extern char frame_0_dpe_dvi_r_frame_00[];
#define frame_0_dpe_dvi_r_frame_00_size 4608
extern char frame_0_dpe_dvo_l_frame_00_01[];
#define frame_0_dpe_dvo_l_frame_00_01_size 4608
extern unsigned int frame_0_golden_dpe_dvo_l_size;
extern char* frame_0_golden_dpe_dvo_l_frame;

extern char frame_0_dpe_dvo_r_frame_00_01[];
#define frame_0_dpe_dvo_r_frame_00_01_size 4608
extern unsigned int frame_0_golden_dpe_dvo_r_size;
extern char* frame_0_golden_dpe_dvo_r_frame;

extern char frame_0_dpe_imgi_l_frame_00[];
#define frame_0_dpe_imgi_l_frame_00_size 2304
extern char frame_0_dpe_imgi_r_frame_00[];
#define frame_0_dpe_imgi_r_frame_00_size 2304
extern char frame_0_dpe_maski_l_frame_00[];
#define frame_0_dpe_maski_l_frame_00_size 2304
extern char frame_0_dpe_maski_r_frame_00[];
#define frame_0_dpe_maski_r_frame_00_size 2304
extern char frame_0_dpe_respo_l_frame_00_01[];
#define frame_0_dpe_respo_l_frame_00_01_size 2304
extern unsigned int frame_0_golden_dpe_respo_l_size;
extern char* frame_0_golden_dpe_respo_l_frame;

extern char frame_0_dpe_respo_r_frame_00_01[];
#define frame_0_dpe_respo_r_frame_00_01_size 2304
extern unsigned int frame_0_golden_dpe_respo_r_size;
extern char* frame_0_golden_dpe_respo_r_frame;

extern char frame_0_dpe_wmf_dpi_frame_00_00_0[];
#define frame_0_dpe_wmf_dpi_frame_00_00_0_size 2304
extern char* frame_0_in_dpe_wmf_dpi_frame_0;

extern char frame_0_dpe_wmf_dpi_frame_00_00_1[];
#define frame_0_dpe_wmf_dpi_frame_00_00_1_size 18432
extern char* frame_0_in_dpe_wmf_dpi_frame_1;

extern char frame_0_dpe_wmf_dpi_frame_00_00_2[];
#define frame_0_dpe_wmf_dpi_frame_00_00_2_size 2304
extern char* frame_0_in_dpe_wmf_dpi_frame_2;

extern char frame_0_dpe_wmf_dpo_frame_00_00_0[];
#define frame_0_dpe_wmf_dpo_frame_00_00_0_size 2304
extern unsigned int frame_0_golden_dpe_wmf_dpo_0_size;
extern char* frame_0_golden_dpe_wmf_dpo_frame_0;

extern char frame_0_dpe_wmf_dpo_frame_00_00_1[];
#define frame_0_dpe_wmf_dpo_frame_00_00_1_size 9216
extern unsigned int frame_0_golden_dpe_wmf_dpo_1_size;
extern char* frame_0_golden_dpe_wmf_dpo_frame_1;

extern char frame_0_dpe_wmf_dpo_frame_00_00_2[];
#define frame_0_dpe_wmf_dpo_frame_00_00_2_size 9216
extern unsigned int frame_0_golden_dpe_wmf_dpo_2_size;
extern char* frame_0_golden_dpe_wmf_dpo_frame_2;

extern char frame_0_dpe_wmf_imgi_frame_00_00_0[];
#define frame_0_dpe_wmf_imgi_frame_00_00_0_size 2304
extern char* frame_0_in_dpe_wmf_imgi_frame_0;

extern char frame_0_dpe_wmf_imgi_frame_00_00_1[];
#define frame_0_dpe_wmf_imgi_frame_00_00_1_size 18432
extern char* frame_0_in_dpe_wmf_imgi_frame_1;

extern char frame_0_dpe_wmf_imgi_frame_00_00_2[];
#define frame_0_dpe_wmf_imgi_frame_00_00_2_size 9216
extern char* frame_0_in_dpe_wmf_imgi_frame_2;

extern char frame_0_dpe_wmf_tbli_frame_00_00_0[];
#define frame_0_dpe_wmf_tbli_frame_00_00_0_size 256
extern char* frame_0_in_dpe_wmf_tbli_frame_0;

extern char frame_0_dpe_wmf_tbli_frame_00_00_1[];
#define frame_0_dpe_wmf_tbli_frame_00_00_1_size 256
extern char* frame_0_in_dpe_wmf_tbli_frame_1;

extern char frame_0_dpe_wmf_tbli_frame_00_00_2[];
#define frame_0_dpe_wmf_tbli_frame_00_00_2_size 256
extern char* frame_0_in_dpe_wmf_tbli_frame_2;



#if 0
extern void getframe_0GoldPointer(
	unsigned long* golden_dpe_dvo_l_frame,
	unsigned long* golden_dpe_dvo_r_frame,
	unsigned long* golden_dpe_confo_l_frame,
	unsigned long* golden_dpe_confo_r_frame,
	unsigned long* golden_dpe_respo_l_frame,
	unsigned long* golden_dpe_respo_r_frame,
	unsigned long* golden_dpe_wmf_dpo_frame_0,
	unsigned long* golden_dpe_wmf_dpo_frame_1,
	unsigned long* golden_dpe_wmf_dpo_frame_2
);
#endif
#endif