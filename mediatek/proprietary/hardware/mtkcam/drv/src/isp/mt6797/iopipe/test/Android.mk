#
# iopipeFrmBtest
#
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
#-----------------------------------------------------------
LOCAL_SRC_FILES += main.cpp main_camio.cpp main_iopipe.cpp main_dpestream.cpp

LOCAL_SRC_FILES += dpe/dpe_testcommon.cpp
LOCAL_SRC_FILES += dpe/dpe_test_case_00/MultiEnque_dpe_test_case_00_frame_0_dpe_setting_00.cpp
LOCAL_SRC_FILES += dpe/dpe_test_case_00/MultiEnque_dpe_test_case_00_frame_2_dpe_setting_00.cpp

LOCAL_SRC_FILES +=  \
  dpe/dpe_default/frame_0/frame_0.c \
  dpe/dpe_default/frame_0/frame_0_dpe_confo_l_frame_00_01verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_confo_r_frame_00_01verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_dvi_l_frame_00verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_dvi_r_frame_00verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_dvo_l_frame_00_01verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_dvo_r_frame_00_01verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_imgi_l_frame_00verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_imgi_r_frame_00verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_maski_l_frame_00verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_maski_r_frame_00verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_respo_l_frame_00_01verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_respo_r_frame_00_01verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_0verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_1verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpi_frame_00_00_2verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_0verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_1verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_wmf_dpo_frame_00_00_2verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_0verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_1verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_wmf_imgi_frame_00_00_2verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_0verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_1verif.c \
  dpe/dpe_default/frame_0/frame_0_dpe_wmf_tbli_frame_00_00_2verif.c \

LOCAL_SRC_FILES +=  \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0.cpp \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_confo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_confo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_dvi_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_dvi_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_dvo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_dvo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_imgi_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_imgi_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_maski_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_maski_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_respo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_respo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_dpi_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_dpi_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_dpi_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_dpo_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_dpo_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_dpo_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_imgi_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_imgi_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_imgi_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_tbli_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_tbli_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_wmf_tbli_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_0/dpe_test_case_00_frame_0_dpe_setting_00.cpp \

LOCAL_SRC_FILES +=  \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1.cpp \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_confo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_confo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_dvi_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_dvi_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_dvo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_dvo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_imgi_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_imgi_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_maski_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_maski_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_respo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_respo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_dpi_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_dpi_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_dpi_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_dpo_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_dpo_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_dpo_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_imgi_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_imgi_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_imgi_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_tbli_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_tbli_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_wmf_tbli_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_1/dpe_test_case_00_frame_1_dpe_setting_00.cpp \

LOCAL_SRC_FILES +=  \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2.cpp \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_confo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_confo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_dvi_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_dvi_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_dvo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_dvo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_imgi_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_imgi_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_maski_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_maski_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_respo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_respo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_dpi_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_dpi_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_dpi_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_dpo_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_dpo_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_dpo_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_imgi_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_imgi_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_imgi_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_tbli_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_tbli_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_wmf_tbli_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_2/dpe_test_case_00_frame_2_dpe_setting_00.cpp \

LOCAL_SRC_FILES +=  \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3.cpp \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_confo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_confo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_dvi_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_dvi_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_dvo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_dvo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_imgi_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_imgi_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_maski_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_maski_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_respo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_respo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_dpi_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_dpi_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_dpi_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_dpo_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_dpo_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_dpo_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_imgi_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_imgi_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_imgi_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_tbli_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_tbli_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_wmf_tbli_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_3/dpe_test_case_00_frame_3_dpe_setting_00.cpp \

LOCAL_SRC_FILES +=  \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4.cpp \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_confo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_confo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_dvi_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_dvi_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_dvo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_dvo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_imgi_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_imgi_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_maski_l_frame_00verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_maski_r_frame_00verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_respo_l_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_respo_r_frame_00_01verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_dpi_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_dpi_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_dpi_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_dpo_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_dpo_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_dpo_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_imgi_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_imgi_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_imgi_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_tbli_frame_00_00_0verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_tbli_frame_00_00_1verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_wmf_tbli_frame_00_00_2verif.c \
  dpe/dpe_test_case_00/frame_4/dpe_test_case_00_frame_4_dpe_setting_00.cpp \

LOCAL_SRC_FILES +=  \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0.cpp \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_confo_l_frame_00_05verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_confo_r_frame_00_05verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_dvi_l_frame_00verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_dvi_r_frame_00verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_dvo_l_frame_00_05verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_dvo_r_frame_00_05verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_imgi_l_frame_00verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_imgi_r_frame_00verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_maski_l_frame_00verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_maski_r_frame_00verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_respo_l_frame_00_05verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_respo_r_frame_00_05verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_setting_00.cpp \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_wmf_dpi_frame_00_02_0verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_wmf_dpi_frame_00_02_1verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_wmf_dpi_frame_00_02_2verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_wmf_dpo_frame_00_02_0verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_wmf_dpo_frame_00_02_1verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_wmf_dpo_frame_00_02_2verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_wmf_imgi_frame_00_02_0verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_wmf_imgi_frame_00_02_1verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_wmf_imgi_frame_00_02_2verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_wmf_tbli_frame_00_02_0verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_wmf_tbli_frame_00_02_1verif.c \
  dpe/dpe_test_case_01/frame_0/dpe_test_case_01_frame_0_dpe_wmf_tbli_frame_00_02_2verif.c \

LOCAL_SRC_FILES +=  \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0.cpp \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_confo_l_frame_00_14verif.c \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_confo_r_frame_00_14verif.c \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_dvi_l_frame_00verif.c \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_dvi_r_frame_00verif.c \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_dvo_l_frame_00_14verif.c \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_dvo_r_frame_00_14verif.c \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_imgi_l_frame_00verif.c \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_imgi_r_frame_00verif.c \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_maski_l_frame_00verif.c \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_maski_r_frame_00verif.c \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_respo_l_frame_00_14verif.c \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_respo_r_frame_00_14verif.c \
  dpe/dpe_test_case_02/frame_0/dpe_test_case_02_frame_0_dpe_setting_00.cpp \

#
# Note: "/bionic" and "/external/stlport/stlport" is for stlport.
#LOCAL_C_INCLUDES += $(TOP)/bionic
#LOCAL_C_INCLUDES += $(TOP)/external/stlport/stlport
#
#Thread Priority
LOCAL_C_INCLUDES += $(TOP)/system/core/include
#utility

#
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_COMMON)/kernel/imgsensor/inc
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_CUSTOM)/kernel/sensor/inc

#for camera_vendor_tags.h:
LOCAL_C_INCLUDES += $(TOP)/system/media/camera/include

#
LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libutils \
    libcam_utils \
    libcam.iopipe \
    libcam.halsensor \
    libcam.metadata

LOCAL_SHARED_LIBRARIES += libstdc++

LOCAL_SHARED_LIBRARIES +=
LOCAL_SHARED_LIBRARIES  += libcamdrv_isp
LOCAL_SHARED_LIBRARIES  += libcamdrv_imem
LOCAL_SHARED_LIBRARIES  += libcamdrv_tuning_mgr

#
LOCAL_STATIC_LIBRARIES := \
#    libcam.iopipe.camio

#
LOCAL_WHOLE_STATIC_LIBRARIES := \

#
LOCAL_MODULE := iopipeTest

#
LOCAL_MODULE_TAGS := eng

#
LOCAL_PRELINK_MODULE := false

#
# Start of common part ------------------------------------
-include $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

MTKCAM_CFLAGS += -DL1_CACHE_BYTES=32
#-----------------------------------------------------------
LOCAL_CFLAGS += $(MTKCAM_CFLAGS)

#-----------------------------------------------------------
LOCAL_C_INCLUDES += $(MTKCAM_C_INCLUDES)
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/middleware/common/include
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/gralloc_extra/include
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/ext/include

LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_VERSION)
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_VERSION)/drv
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_VERSION)/imageio
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_VERSION)/iopipe
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/include/$(MTKCAM_DRV_VERSION)/Sen
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/include/$(MTKCAM_DRV_VERSION)
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/common/include
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/middleware/v1.4/include
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/include/$(MTKCAM_DRV_VERSION)/iopipe/camio
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/drv/src/isp/$(MTKCAM_DRV_VERSION)/inc

ifeq ($(MTK_CAM_STEREO_DENOISE_SUPPORT),yes)
    LOCAL_CFLAGS += -DSTEREO_DENOISE_SUPPORTED=1
endif

LOCAL_MULTILIB := 32
#-----------------------------------------------------------
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/hardware/include
#-----------------------------------------------------------
ifeq ($(TARGET_BUILD_VARIANT), user)
MTKCAM_LOGENABLE_DEFAULT   := 0
else
MTKCAM_LOGENABLE_DEFAULT   := 1
endif

#-----------------------------------------------------------
LOCAL_CFLAGS += -DMTKCAM_LOGENABLE_DEFAULT=$(MTKCAM_LOGENABLE_DEFAULT)
# End of common part ---------------------------------------
#
include $(BUILD_EXECUTABLE)


#
include $(call all-makefiles-under,$(LOCAL_PATH))
