/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/mount.h>
#include <sys/statfs.h>
#include <dirent.h>
#include <linux/input.h>
#include <math.h>

#include "common.h"
#include "miniui.h"
#include "ftm.h"
#include "cust.h"

#include <linux/sensors_io.h>

extern sp_ata_data return_data;

/******************************************************************************
 * MACRO
 *****************************************************************************/
#define TAG "[MODEM] "
#define mod_to_modem_data(p) (struct modem_data*)((char*)(p) + sizeof(struct ftm_module))

bool modem_thread_exit = false;

pthread_mutex_t modem_mutex = PTHREAD_MUTEX_INITIALIZER;

/******************************************************************************
 * Structure
 *****************************************************************************/
enum {
    ITEM_PASS,
    ITEM_FAIL,
};

struct modem_data
{

    /*common for each factory mode*/
    char  info[1024];
    //bool  avail;
    bool  exit_thd;

    text_t    title;
    text_t    text;
    text_t    left_btn;
    text_t    center_btn;
    text_t    right_btn;
    
    pthread_t update_thd;
    struct ftm_module *mod;
    //struct textview tv;
    struct itemview *iv;
};

extern char barcode[128]; 
extern int create_verinfo(char *info, int size);


/*---------------------------------------------------------------------------*/
static item_t modem_items[] = {
#ifndef FEATURE_FTM_TOUCH_MODE
    item(ITEM_PASS,   uistr_pass),
    item(ITEM_FAIL,   uistr_fail),
#endif
    item(-1, NULL),
};
static item_t modem_items_fail[] = {
#ifndef FEATURE_FTM_TOUCH_MODE 
    item(ITEM_FAIL,   uistr_fail),
#endif
    item(-1, NULL),
};

/*---------------------------------------------------------------------------*/
static void *modem_update_iv_thread(void *priv)
{
    struct modem_data *dat = (struct modem_data *)priv; 
    struct itemview *iv = dat->iv;    
    int err = 0, len = 0;
    char *status;
	//char barcode[128] = "unknown";   
	
    //get_barcode_from_nvram(barcode);
	while(1){
		if (dat->exit_thd)
            break;

		len = 0;
	    len += snprintf(dat->info+len, sizeof(dat->info)-len, "barcode: %s  \n", barcode);  

		iv->set_text(iv, &dat->text);
        iv->redraw(iv);
	}
	pthread_exit(NULL);
    return NULL;
}
/*---------------------------------------------------------------------------*/
int modem_entry(struct ftm_param *param, void *priv)
{
    char *ptr;
    int chosen;
    struct modem_data *dat = (struct modem_data *)priv;
	//struct ftm_module *mod = (struct ftm_module *)priv;
    struct textview *tv;
    struct itemview *iv;
    struct statfs stat;
    int err;
    int len = 0;
    LOGD(TAG "%s\n", __FUNCTION__);
	create_verinfo(ptr,&len);
	//LOGD(TAG "%s %c %c %c\n", __FUNCTION__,barcode[60],barcode[61],barcode[62]);

    init_text(&dat->title, param->name, COLOR_YELLOW);
    init_text(&dat->text, &dat->info[0], COLOR_YELLOW);
    init_text(&dat->left_btn, uistr_info_sensor_fail, COLOR_YELLOW);
    init_text(&dat->center_btn, uistr_info_sensor_pass, COLOR_YELLOW);
    init_text(&dat->right_btn, uistr_info_sensor_back, COLOR_YELLOW);
	
    dat->exit_thd = false;  
    modem_thread_exit = false;
 

    if (!dat->iv) {
        iv = ui_new_itemview();
        if (!iv) {
            LOGD(TAG "No memory");
            return -1;
        }
        dat->iv = iv;
    }
    iv = dat->iv;
    iv->set_title(iv, &dat->title);
    if((barcode[60] == '1')&&(barcode[61] == '0')&&(barcode[62] == 'P'))
		iv->set_items(iv, modem_items, 0);
	else
		iv->set_items(iv, modem_items_fail, 0);
    iv->set_text(iv, &dat->text);
    //sp_ata_status = param->test_type;
    pthread_create(&dat->update_thd, NULL, modem_update_iv_thread, priv);
    do {
        /*if(sp_ata_status == FTM_MANUAL_ITEM)*/{

        chosen = iv->run(iv, &modem_thread_exit);
        switch (chosen) {
        case ITEM_PASS:
        case ITEM_FAIL:
            if (chosen == ITEM_PASS) {
                dat->mod->test_result = FTM_TEST_PASS;
            } else if (chosen == ITEM_FAIL) {
                dat->mod->test_result = FTM_TEST_FAIL;
            }      
            modem_thread_exit = true;            
            break;
        }
        }
        iv->redraw(iv);
        pthread_mutex_lock (&modem_mutex);
        if (modem_thread_exit) {
            dat->exit_thd = true;
            pthread_mutex_unlock (&modem_mutex);
            break;
        }else{
            pthread_mutex_unlock (&modem_mutex);
            usleep(50000);
        }        
    } while (1);
    pthread_join(dat->update_thd, NULL);

    return 0;
}
/*---------------------------------------------------------------------------*/
int modem_init(void)
{
    int ret = 0;
    struct ftm_module *mod;
	struct modem_data *dat;

    LOGD(TAG "%s \n", __FUNCTION__);
    
    mod = ftm_alloc(ITEM_MODEM, sizeof(struct modem_data));
    dat = mod_to_modem_data(mod);

	dat->mod = mod;
	
    if (!mod)
        return -ENOMEM;

    ret = ftm_register(mod, modem_entry, (void*)dat);

    return ret;
}

